/**
 * Element Short Code js v1.1.0
 * Copyright 2017-2020
 * Licensed under  ()
 */

"use strict";

jQuery(document).ready(function () {

    /* Slider Blog */
    var $tz_slider_blog = jQuery( '.tz_slider_blog' );

    if ( $tz_slider_blog.length ) {
        var $tz_pagination      = $tz_slider_blog.data('pagination'),
            $tz_pagination_val  =   '',
            $tz_auto            = $tz_slider_blog.data('auto'),
            $tz_auto_val        =  '';

        if ( $tz_pagination === 0 ) {
            $tz_pagination_val = false;
        }else if( $tz_pagination === 1 ) {
            $tz_pagination_val = true;
        }

        if ( $tz_auto === 0 ) {
            $tz_auto_val = false;
        }else if( $tz_auto === 1 ) {
            $tz_auto_val = 6000;
        }

        var $tz_superslides =   jQuery('#slides');

        $tz_superslides.superslides({
            play: $tz_auto_val,
            slide_easing: 'easeInOutCubic',
            pagination: $tz_pagination_val,
            scrollable: true
        });


        $tz_superslides.on('animated.slides || init.slides', function() {
            // get current slide using superslides API current
            var currentSlideIndex = jQuery(this).superslides('current'),
                currentSlide = jQuery('.slides-container > .tz_slides_post')[currentSlideIndex];


            // pause all videos
            jQuery(".video_background").each(function () {
                this.pause();
            });

            // if there is a video on current slide, play it
            var currentVid = jQuery(currentSlide).find("video")[0];

            if (jQuery(currentVid).length) {
                jQuery(currentVid)[0].oncanplaythrough = jQuery(currentVid)[0].play();
            }

        });

        var tz_btn_play_video_bk    =   jQuery( '.tz_btn_play_video_bk' );

        if ( tz_btn_play_video_bk.length ) {

            tz_btn_play_video_bk.click(function () {

                var tz_video_background =   jQuery(this).parents( '.tz_slides_post' ).find( '.video_background' );

                if ( tz_video_background.get(0).paused ) {
                    tz_video_background.get(0).play();
                    jQuery(this).addClass( 'tz_btn_play_video_bk_pause' );
                } else {
                    tz_video_background.get(0).pause();
                    jQuery(this).removeClass( 'tz_btn_play_video_bk_pause' );
                }

            });

        }

    }
    /* End Slider Blog */

    /* Reservation */
    var $tz_reservation  =   jQuery( '.tz_reservation' );
    if ( $tz_reservation.length ) {

        jQuery( '.tz_title_book' ).on( "click", function() {
            jQuery(this).parent().find('.tz_reservation').fadeIn(350);
        });

        jQuery( '.tz_reservation_close i.fa' ).on( "click", function() {
            jQuery(this).parents('.tz_reservation').fadeOut(350);
        });

        jQuery(function(){
            jQuery( ".tz_date_contact input" ).datepicker();
        });

    }
    /* End Reservation */

    /* Slider Images */
    var $tz_slider_images = jQuery( '.tz_slider_images' );

    if ( $tz_slider_images.length ) {

        var $tz_slider_images_items = jQuery('.tz_slider_images_items');

        $tz_slider_images_items.each( function() {

            var $tz_data_color      =   jQuery(this).parents('.tz_slider_images').data('color'),
                $tz_data_auto       =   jQuery(this).parents('.tz_slider_images').data('auto'),
                $tz_data_loop       =   jQuery(this).parents('.tz_slider_images').data('loop'),
                $tz_data_rtl        =   jQuery(this).parents('.tz_slider_images').data('rtl'),
                $tz_data_auto_val   =   '',
                $tz_data_loop_val   =   '',
                $tz_data_rtl_val    =   '';

            if ( $tz_data_color !== undefined ) {
                jQuery(this).find('.tz_title_image').css("color", $tz_data_color);
            }

            if ( $tz_data_auto === 0 ) {
                $tz_data_auto_val = false;
            }else if ( $tz_data_auto === 1 )  {
                $tz_data_auto_val = true;
            }

            if ( $tz_data_loop === 0 ) {
                $tz_data_loop_val = false;
            }else if ( $tz_data_loop === 1 )  {
                $tz_data_loop_val = true;
            }

            if ( $tz_data_rtl === 0 ) {
                $tz_data_rtl_val = false;
            }else if ( $tz_data_rtl === 1 )  {
                $tz_data_rtl_val = true;
            }

            jQuery(this).owlCarousel({
                items : 1,
                slideSpeed:800,
                paginationSpeed:800,
                smartSpeed: 700,
                nav:true,
                dots: false,
                navText: [ '<i class="lnr lnr-arrow-left"></i>', '<i class="lnr lnr-arrow-right"></i>' ],
                autoplayTimeout: 5000,
                autoplay: $tz_data_auto_val,
                loop: $tz_data_loop_val,
                rtl:$tz_data_rtl_val,
                autoHeight:false
            });

        });

    }
    /* End Slider Images */

    /* Element Services */
    var  $tz_night_club_services_item = jQuery(".tz_night_club_services_item");

    if ( $tz_night_club_services_item.length ) {

        fitui();

        jQuery('.third').each(function(){
            var $thisThird = jQuery(this);
            $thisThird.mouseenter(function(){
                $thisThird.siblings().removeClass('inactive');
                $thisThird.siblings().removeClass('active');

                $thisThird.removeClass('inactive');
                $thisThird.removeClass('active');

                $thisThird.siblings().addClass('inactive');
                $thisThird.addClass('active');

            });
        });

        // tz_element_services();

        $tz_night_club_services_item.each(function(){

            var $tz_night_club_services_item_type_video =   jQuery(this).data( 'type-video' );

            if ( $tz_night_club_services_item_type_video === 1 ) {

                jQuery(this).mouseover(function() {
                    jQuery('video', this).get(0).play();
                });

                jQuery(this).mouseleave(function() {
                    jQuery('video', this).get(0).pause();
                });

            }

        })

    }
    /* End Element Services */

    /* Element Event */
    var $tz_night_club_event = jQuery( '.tz_night_club_event' );

    if ( $tz_night_club_event.length ) {

        var $tz_night_club_event_item           =   jQuery( '.tz_night_club_event_item.tz_night_club_event_item_fist').data('image'),
            $tz_night_club_event_item_click     =   jQuery( '.tz_night_club_event_item'),
            $tz_night_club_event_item_bk_hover  =   $tz_night_club_event_item_click.data('bk-hover');

        $tz_night_club_event_item_click.each(function(){

            jQuery(this).on("click",function(){

                jQuery(this).siblings().removeClass('active');
                jQuery(this).addClass('active');

            })

        });

        $tz_night_club_event_item_click.hover(function(){

            var $tz_night_club_event_item_attr  =   jQuery(this).data('image');

            jQuery('.tz_upcoming_event_image img').fadeIn('500').attr('src', $tz_night_club_event_item_attr);

        });

        $tz_night_club_event.each( function() {
            jQuery(this).tinyscrollbar(
                {
                    thumbSize: 100
                }
            );
        })

    }
    /* End Element Event */

    /* Element Video */
    var $tz_blog_more_video =   jQuery( '.tz_blog_more_video' );

    if ( $tz_blog_more_video.length ) {
        var $tz_title_more_video    = jQuery( '.tz_title_more_video' );

        $tz_title_more_video.each(function(){

            jQuery(this).on("click", function(){
                var $tz_date_more_video     =   jQuery(this).data('date'),
                    $href_more_post_video   =   jQuery(this).data('href-title'),
                    $title_more_video       =   jQuery(this).data('title'),
                    $content_more_video     =   jQuery(this).data('content');

                jQuery('.tz_blog_video_content_box').fadeOut("1600",function(){
                    jQuery('.tz_blog_video_content_box').fadeIn('1600');
                    jQuery('.tz_blog_video_content .tz_date_post_video').html($tz_date_more_video);
                    jQuery('.tz_blog_video_content .tz_title_blog_video a').attr('href', $href_more_post_video).html($title_more_video);
                    jQuery('.tz_blog_video_content .tz_content_post_video').html($content_more_video);
                });

            })

        })
    }
    /* End Element Video */

    /* Element Gallery */
    var $tz_1040nightclub_gallery   =   jQuery( '.tz_nightclub_gallery' );

    if ( $tz_1040nightclub_gallery.length ) {
        var $tz_1040nightclub_gallery_slider    =   jQuery( '.tz_nightclub_gallery_slider');

        tz_height_gallery();
        /* ResizeImage blog list */
        tz_1040nightclub_ResizeImage( jQuery('.tz_img_item_gallery') );

        $tz_1040nightclub_gallery_slider.each( function() {
            var owl = $tz_1040nightclub_gallery_slider;

            var $data_auto_gallery  =   jQuery(this).data( 'auto-gallery'),
                $tz_gallery_auto    =   '',
                $data_loop_gallery  =   jQuery(this).data( 'loop-gallery'),
                $tz_gallery_loop    =   '';

            if ( $data_auto_gallery === 0 ) {
                $tz_gallery_auto = false;
            }else if ( $data_auto_gallery === 1 )  {
                $tz_gallery_auto = true;
            }

            if ( $data_loop_gallery === 0 ) {
                $tz_gallery_loop = false;
            }else if ( $data_loop_gallery === 1 )  {
                $tz_gallery_loop = true;
            }

            owl.owlCarousel({
                items : 1,
                slideSpeed:800,
                paginationSpeed:800,
                smartSpeed: 700,
                autoplayTimeout: 5000,
                autoplay: $tz_gallery_auto,
                loop: $tz_gallery_loop,
                rtl:false,
                autoHeight:false
            });

            var total = jQuery('.owl-dot').length;

            jQuery('.tz_next_gallery').click(function() {
                owl.trigger('next.owl.carousel');
            });
            jQuery('.tz_pre_gallery').click(function() {
                owl.trigger('prev.owl.carousel', [300]);
            });

            owl.on('changed.owl.carousel', function() {

                jQuery('.owl-dot').each(function(index){
                    var active =jQuery(this).hasClass('active');
                    if(active){
                        var active_next = index + 2;
                        var active_pre = index + 1;

                        if ( $tz_gallery_loop === 0 ) {

                            if( active_next > total ){
                                active_next = total;
                                jQuery('.tz_next_gallery').addClass('tz_next_gallery_end');
                            }else {
                                jQuery('.tz_next_gallery').removeClass('tz_next_gallery_end');
                            }

                        }else {

                            if( active_next > total ){

                                active_next = total;

                            }

                        }

                        jQuery('.tz_next_gallery span').html(twoDigit(active_next) + ' ' +  '/' + ' ' + twoDigit(total) + '<i class="lnr lnr-arrow-right"></i>');
                        jQuery('.tz_pre_gallery span').html('<i class="lnr lnr-arrow-left"></i>' + twoDigit(active_pre) + ' ' + '/' + ' ' + twoDigit(total));
                    }

                })

            });

        });

        jQuery(".fancybox").fancybox({
            padding: 0,
            openSpeed : 500,
            direction : {
                next : 'left',
                prev : 'right'
            }
        });

    }
    /* End Element Gallery */

    /* Contac info - hour */
    var $tz_night_club_info_hour = jQuery( '.tz_night_club_info_hour' );

    if ( $tz_night_club_info_hour.length ) {

        $tz_night_club_info_hour.each( function() {

            var $data_color_text_1 = jQuery(this).data( 'color-text-1'),
                $data_color_text_2 = jQuery(this).data( 'color-text-2' );

            if ( $data_color_text_1 !== undefined ) {
                jQuery(this).find('.tz_night_club_info_hour_text_1').css("color", $data_color_text_1);
            }
            if ( $data_color_text_2 !== undefined ) {
                jQuery(this).find('.tz_night_club_info_hour_text_2').css("color", $data_color_text_2);
            }

        })

    }
    /* End Contac info - hour */

});

jQuery(window).on("load",function() {

    /* Element Services */
    jQuery( '.tz_night_club_services').addClass('tz_night_club_services_active');
    tz_element_services();
    /* End Element Services */

});


var tz_timer,
    winwid = 0;


jQuery(window).on("resize",function(){

    fitui();

    if ( tz_timer ) clearTimeout(tz_timer);

    tz_timer = setTimeout( function() {

        /* Element Services */
        tz_element_services();
        /* End Element Services */

        /* Element Gallery */
        tz_height_gallery();
        tz_1040nightclub_ResizeImage( jQuery('.tz_img_item_gallery') );
        /* End Element Gallery */

    }, 200);

});

function tz_1040nightclub_ResizeImage(obj){
    var widthStage;
    var heightStage ;
    var widthImage;
    var heightImage;
    obj.each(function (i,el){

        heightStage = jQuery(this).height();

        widthStage = jQuery (this).width();

        var img_url = jQuery(this).find('img').attr('src');

        var image = new Image();
        image.src = img_url;

        widthImage = image.naturalWidth;
        heightImage = image.naturalHeight;

        var tzimg	=	new resizeImage(widthImage, heightImage, widthStage, heightStage);
        jQuery(this).find('img').css ({ top: tzimg.top, left: tzimg.left, width: tzimg.width, height: tzimg.height });


    });

}

/* Function Element Services */
function fitui() {

    var winhei      = jQuery(window).height(),
        headerhei   = jQuery('#header').outerHeight(),
        footerhei   = jQuery('#footer').outerHeight(),
        twinhei     = winhei - ( headerhei + footerhei );
    jQuery('.third').height( twinhei );

}

function tz_element_services() {

    var winwid                  =   jQuery(window).width(),
        $tz_night_club_services =   jQuery( '.tz_night_club_services'),
        $tz_winwid              =   $tz_night_club_services.width(),
        $tz_data_hover_width    =   $tz_night_club_services.data('hover-width');

    if ( $tz_night_club_services.length ) {

        var $tz_night_club_services_item        =   jQuery('.tz_night_club_services_item'),
            $count_tz_night_club_services_item  =   $tz_night_club_services_item.length,
            $width_services_item                =   winwid/$count_tz_night_club_services_item,
            $width_services_item_active         =   $width_services_item*($count_tz_night_club_services_item - 1);

        if( winwid > 1200 ){

            $tz_night_club_services_item.css( "width",$width_services_item );

            if ( $tz_data_hover_width === 1 ) {

                $tz_night_club_services_item.mouseover( function(){

                    jQuery(this).css("width", $width_services_item_active);

                    var $item   =   ( $tz_winwid - $width_services_item_active )/( $count_tz_night_club_services_item - 1 );
                    jQuery(this).siblings().css("width", $item);

                });

                $tz_night_club_services_item.mouseleave( function(){

                    jQuery(this).css("width", $width_services_item);
                    jQuery(this).siblings().css("width", $width_services_item);

                });

            }

        } else {

            var $tz_height_service      =   jQuery( window ).height(),
                $height_services_item   =   $tz_height_service/$count_tz_night_club_services_item;

            $tz_night_club_services_item.unbind( "mouseover" );
            $tz_night_club_services_item.unbind( "mouseleave" );
            $tz_night_club_services_item.css( { "width":$tz_winwid,"height":$height_services_item } );

        }

    }
}
/* End Function Element Services */

/* Function Element Gallery */
function tz_height_gallery() {

    var $tz_1040nightclub_gallery_slider        =   jQuery( '.tz_nightclub_gallery_slider'),
        $tz_1040nightclub_gallery_slider_width  =   jQuery( window ).width(),
        $tz_1040nightclub_gallery_slider_height =   jQuery( window ).height();

    $tz_1040nightclub_gallery_slider.each(function(){

        var $data_number_column  =   jQuery(this).attr('data-number-column'),
            $data_number_row     =   jQuery(this).attr('data-number-row');

        if ( $tz_1040nightclub_gallery_slider_width > 1024 ) {

            jQuery( '.tz_img_item_gallery').css({"width": $tz_1040nightclub_gallery_slider_width/$data_number_column,"height": $tz_1040nightclub_gallery_slider_height/$data_number_row});

        }else if ( $tz_1040nightclub_gallery_slider_width <= 1024 ) {

            jQuery( '.tz_img_item_gallery').css({"width": $tz_1040nightclub_gallery_slider_width/2,"height": $tz_1040nightclub_gallery_slider_height/$data_number_row});

        }

    })

}
/* End Function Element Gallery */

function twoDigit(number) {
    var two_digit = number >= 10 ? number : "0"+number.toString();
    return two_digit;
}
/* End Function Element Gallery */