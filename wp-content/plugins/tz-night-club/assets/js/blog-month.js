jQuery(document).ready(function(){

    "use strict";

    /* Element Blog Carousel */
    var $tz_blog_carousel =   jQuery( '.tz_blog_carousel' );

    if ( $tz_blog_carousel.length ) {

        var $tz_blog_carousel_items =   jQuery( '.tz_blog_carousel_items' );


        /* Filters Post */
        var $tz_filters_post_carousel = jQuery( '.tz_filters_post_carousel' );

        $tz_filters_post_carousel.each(function(){

            var $tz_monthly_archives = jQuery(this).find( '.tz_monthly_archives').length;

            if ( $tz_monthly_archives > 6 ) {
                jQuery(this).parent().find('.tz_next_pre_month').addClass('tz_next_pre_month_active');
                jQuery(this).addClass('tz_filters_post_carousel_active');
            }

            jQuery(this).owlCarousel({
                responsiveClass:true,
                responsive:{
                    0:{
                        items:2
                    },
                    1024:{
                        items:3
                    },
                    1500:{
                        items : 6
                    }
                },
                slideSpeed:800,
                paginationSpeed:800,
                smartSpeed: 700,
                dots:false,
                autoplayTimeout: 5000,
                autoplay: false,
                loop: false,
                autoHeight:false
            });

            jQuery('.tz_next_month').click(function(){
                jQuery(this).parents('.tz_filters_post').find('.tz_filters_post_carousel').trigger('prev.owl.carousel');
            });
            jQuery('.tz_pre_month').click(function(){
                jQuery(this).parents('.tz_filters_post').find('.tz_filters_post_carousel').trigger('next.owl.carousel');
            });


        });

        var $tz_filters_post_click = jQuery( '.tz_filters_post_carousel h4 a' );

        $tz_filters_post_click.each(function(){

            jQuery(this).click(function(event){

                event.preventDefault();
                jQuery(this).parent().addClass('tz_filters_active');
                jQuery(this).parents('.owl-item').siblings().find('h4').removeClass('tz_filters_active');

                var $data_limit_post    =   jQuery(this).parents('.tz_blog_carousel').attr('data-limit-post'),
                    $data_order_post    =   jQuery(this).parents('.tz_blog_carousel').attr('data-order-post'),
                    $data_date_post     =   jQuery(this).text();

                $tz_blog_carousel_items.css('opacity', 0);

                jQuery.ajax({
                    url: tz_1040nightclub_blog_month_var.url,
                    type: "post",
                    data:({
                        action: 'tz_1040nightclub_blog_month',
                        limit: $data_limit_post,
                        order: $data_order_post,
                        date: $data_date_post
                    }),
                    success: function(data) {

                        var $tz_filters_post_carousel   =   jQuery('.tz_blog_carousel_items');

                        $tz_filters_post_carousel.css('opacity', 1);

                        jQuery(this).parents('.tz_blog_carousel').find('.tz_blog_carousel_items').removeClass('tz_blog_carousel_items_hidden');
                        $tz_filters_post_carousel.trigger('replace.owl.carousel', data).trigger('refresh.owl.carousel');

                        jQuery('.tz_blog_carousel_items .owl-height').css('height', 'auto');

                    }
                })

            });

        });

    }
    /* End Element Blog Carousel */

});

jQuery(window).on("load",function() {

    "use strict";

    /* Element Blog Carousel */
    var $tz_blog_carousel =   jQuery( '.tz_blog_carousel' );

    if ( $tz_blog_carousel.length ) {

        var $tz_blog_carousel_items = jQuery('.tz_blog_carousel_items');

        $tz_blog_carousel_items.each(function () {

            jQuery(this).owlCarousel({
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    767: {
                        items: 2
                    },
                    1024: {
                        items: 3
                    },
                    1500: {
                        items: 4
                    }
                },
                slideSpeed: 800,
                paginationSpeed: 800,
                smartSpeed: 700,
                dots: false,
                autoplayTimeout: 5000,
                autoplay: false,
                loop: false,
                rtl: false,
                autoHeight: true
            });

        });

        jQuery('.tz_partner_prev').click(function () {
            jQuery(this).parents('.tz_blog_carousel').find('.tz_blog_carousel_items').trigger('prev.owl.carousel');
        });
        jQuery('.tz_partner_next').click(function () {
            jQuery(this).parents('.tz_blog_carousel').find('.tz_blog_carousel_items').trigger('next.owl.carousel');
        });
    }

});
