<?php
function tz_nightclub_coming_soon( $atts, $content = null ) {

    $bk_image = $title_coming_soon = $text_date = $date =  $month = $year = $time = $social_network = '';
    $color_title = $color_text_date = $color_count_down = $color_social_network = $color_social_network_hover = '';
    $text_day = $text_hours = $text_minutes = $text_seconds = $bk_color = '';
    
    extract(shortcode_atts(array(

        'bk_image'                      =>  '',
        'bk_color'                      =>  '',
        'title_coming_soon'             =>  'COMING SOON',
        'text_date'                     =>  '25.07.2016',
        'date'                          =>  1,
        'month'                         =>  1,
        'year'                          =>  '2016',
        'time'                          =>  '23:23:23',
        'text_day'                      =>  'Days',
        'text_hours'                    =>  'Hours',
        'text_minutes'                  =>  'Minutes',
        'text_seconds'                  =>  'Seconds',
        'social_network'                =>  1,
        'color_title'                   =>  '',
        'color_text_date'               =>  '',
        'color_count_down'              =>  '',
        'color_social_network'          =>  '',
        'color_social_network_hover'    =>  '',

    ), $atts));
    
    ob_start();

    $tz_nightclub_bk_img    =   '';
    $tz_nightclub_bk_color  =   '';
    if ( $bk_image != '' || $bk_color !='' ) :

        if ( $bk_image != '' ) {
            $tz_nightclub_bk_img = 'background-image:url(' . esc_url(wp_get_attachment_url($bk_image)) . ');';
        }
        if ( $bk_color !='' ) {
            $tz_nightclub_bk_color = 'background-color:' .$bk_color. ';';
        }

    endif;

?>

    <div class="tz_nightclub_coming_soon" <?php echo ( $bk_image != '' || $bk_color !='' ? 'style="'. $tz_nightclub_bk_img . $tz_nightclub_bk_color .'"' : '' ); ?>>
        <div class="ds-table-cell">
            <div class="tz_coming_soon_title">
                <h3 <?php echo( $color_title != '' ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>
                    <?php echo balanceTags( $title_coming_soon ); ?>
                </h3>
                <h2 class="tz_text_date" <?php echo( $color_text_date != '' ? 'style="color:' . esc_attr( $color_text_date ) . '"' : '' ); ?>>
                    <?php echo balanceTags( $text_date ); ?>
                </h2>
            </div>
            <div class="tz_nightclub_countdown" data-countdown="<?php echo esc_attr( $year ).'/'.esc_attr( $month ).'/'.esc_attr( $date ).' '.esc_attr( $time ); ?>" data-text-day="<?php echo esc_attr( $text_day ); ?>" data-text-hours="<?php echo esc_attr( $text_hours ); ?>" data-text-minutes="<?php echo esc_attr( $text_minutes ); ?>" data-text-seconds="<?php echo esc_attr( $text_seconds ); ?>" <?php echo( $color_count_down != '' ? 'style="color:' . esc_attr( $color_count_down ) . '"' : '' ); ?>>
                <div id="clock"></div>
            </div>

            <?php if ( $content != '' ) : ?>

                <div class="tz_newsletter_comingsoon">
                    <?php echo do_shortcode( $content ); ?>
                </div>

            <?php endif; ?>

            <?php if ( $social_network == 1 && function_exists('tz_1040nightclub_social_network') ) : ?>

                <div class="tz_coming_soon_social_network" <?php echo( $color_social_network != '' ? 'data-color-social="' . esc_attr( $color_social_network ) . '"' : '' ); ?> <?php echo( $color_social_network_hover != '' ? 'data-color-social-hover="' . esc_attr( $color_social_network_hover ) . '"' : '' ); ?>>
                    <?php tz_1040nightclub_social_network(); ?>
                </div>

            <?php endif; ?>

        </div>
    </div>

<?php

    $tz_nightclub  =   ob_get_contents();
    ob_end_clean();
    return $tz_nightclub;

}
add_shortcode( 'tz_coming_soon','tz_nightclub_coming_soon' );

?>