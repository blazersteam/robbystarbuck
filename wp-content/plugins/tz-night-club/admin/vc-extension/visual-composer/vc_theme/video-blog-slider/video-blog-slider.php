<?php
function tz_nightclub_video_blog_slider( $atts ) {

    $title_slider = $sub_title_slider = $category_blog_arr = $posts_per_page = $orderby = $order = $limit_words_content = '';
    $color_title = $color_sub_title = $color_date_post = $color_title_post = $color_content_post = $color_btn_play = '';
    $title_more_videos = $color_title_more = $color_icon_more = $color_title_post_more = '';
    $mouse_move = '';

    extract(shortcode_atts(array(

        'title_slider'          =>  'Live Stream in 1040 club',
        'sub_title_slider'      =>  'The Video',
        'category_blog_arr'     =>  '',
        'posts_per_page'        =>  3,
        'orderby'               =>  'date',
        'order'                 =>  'desc',
        'limit_words_content'   =>  30,
        'color_title'           =>  '',
        'color_sub_title'       =>  '',
        'color_date_post'       =>  '',
        'color_title_post'      =>  '',
        'color_content_post'    =>  '',
        'color_btn_play'        =>  '',
        'title_more_videos'     =>  'More Videos',
        'color_title_more'      =>  '',
        'color_icon_more'       =>  '',
        'color_title_post_more' =>  '',
        'mouse_move'            =>  0,

    ), $atts));

    ob_start();

    global $post;
    if ( isset($category_blog_arr) && !empty($category_blog_arr) ):
        $args = array(
            'post_type'         =>  'post',
            'cat'               =>  $category_blog_arr,
            'posts_per_page'    =>  $posts_per_page,
            'orderby'           =>  $orderby,
            'order'             =>  $order,
        );
    else:
        $args = array(
            'post_type'         =>  'post',
            'posts_per_page'    =>  $posts_per_page,
            'orderby'           =>  $orderby,
            'order'             =>  $order,
        );
    endif;

    $recent_news    =   new WP_Query( $args );

    ?>

    <div class="tz_blog_slider_video">
        <div class="tz_blog_slider_video_bk"></div>
        <div class="tz_blog_video_content">
            <div class="ds-table">
                <div class="ds-table-cell-left-center">

                    <?php if ( $title_slider !='' || $sub_title_slider !='' ) : ?>

                        <div class="tz_element_title tz_element_title_video">

                            <?php if ( $title_slider !='' ) : ?>

                                <h3 class="tz_title" <?php echo( $color_title != '' ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>
                                    <?php echo balanceTags( $title_slider ); ?>
                                </h3>

                            <?php endif; ?>

                            <?php if ( $sub_title_slider !='' ) : ?>

                                <h2 class="tz_sub_title" <?php echo( $color_sub_title != '' ? 'style="color:' . esc_attr( $color_sub_title ) . '"' : '' ); ?>>
                                    <?php echo balanceTags( $sub_title_slider ); ?>
                                </h2>

                            <?php endif; ?>

                        </div>

                    <?php endif; ?>

                    <?php
                    if ( $recent_news->have_posts() ) :
                        while ( $recent_news->have_posts() ) :
                            $recent_news->the_post();

                            $tz_nightclub_video_mp4     =   get_post_meta( $post -> ID, 'nightclub_portfolio_video_mp4', true );

                            if ( $recent_news->current_post == 0 ) :
                    ?>

                                <div id="tz_blog_video_<?php the_ID(); ?>" class="tz_tz_blog_video_item">
                                    <div class="tz_blog_video_content_box">
                                        <span class="tz_date_post_video" <?php echo( $color_date_post != '' ? 'style="color:' . esc_attr( $color_date_post ) . '"' : '' ); ?>>
                                            <?php echo get_the_date(); ?>
                                        </span>
                                        <h2 class="tz_title_blog_video">
                                            <a href="<?php the_permalink() ?>" <?php echo( $color_title_post != '' ? 'style="color:' . esc_attr( $color_title_post ) . '"' : '' ); ?>>
                                                <?php the_title(); ?>
                                            </a>
                                        </h2>
                                        <p class="tz_content_post_video" <?php echo( $color_content_post != '' ? 'style="color:' . esc_attr( $color_content_post ) . '"' : '' ); ?>>
                                            <?php echo wp_trim_words( get_the_content(), $limit_words_content ); ?>
                                        </p>

                                    </div>
                                </div>

                    <?php
                            endif;
                        endwhile;
                    endif;
                    wp_reset_postdata();
                    ?>

                    <div class="tz_box_play" id="nav">
                        <div class="toggle-btn">
                            <span class="tz_btn_video_play_pause" <?php echo( $color_btn_play != '' ? 'style="color:' . esc_attr( $color_btn_play ) . '"' : '' ); ?>>
                                <i class="icon-music-play-button"></i>
                            </span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="tz_blog_more_video">

            <?php
            if ( $recent_news->have_posts() ) :
            ?>

                <h3 class="tz_header_more_video" <?php echo( $color_title_more != '' ? 'style="color:' . esc_attr( $color_title_more ) . '"' : '' ); ?>>
                    <?php echo balanceTags( $title_more_videos ); ?>
                </h3>

            <?php
                while ( $recent_news->have_posts() ) :
                    $recent_news->the_post();

                        $tz_nightclub_video_mp4     =   get_post_meta( $post -> ID, 'nightclub_portfolio_video_mp4', true );
            ?>
                        <h5 class="tz_title_more_video" data-date="<?php echo get_the_date(); ?>" data-href-title="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" data-content="<?php echo wp_trim_words( get_the_content(), $limit_words_content ); ?>">
                            <a href="<?php echo esc_url( $tz_nightclub_video_mp4 ); ?>" class="playlist-btn" <?php echo( $color_title_post_more != '' ? 'style="color:' . esc_attr( $color_title_post_more ) . '"' : '' ); ?>>
                                <i class="lnr lnr-film-play" <?php echo( $color_icon_more != '' ? 'style="color:' . esc_attr( $color_icon_more ) . '"' : '' ); ?>></i>
                                <?php the_title(); ?>
                            </a>
                        </h5>
            <?php

                endwhile;
            endif;
            wp_reset_postdata();
            ?>

        </div>
    </div>

    <script>
        var BV;
        jQuery(function() {

            // initialize BigVideo
            var BV = new jQuery.BigVideo({
                useFlashForFirefox:false,
                doLoop:true
            });
            BV.init();
            BV.show([
                <?php
                if ( $recent_news->have_posts() ) :
                while ( $recent_news->have_posts() ) :
                $recent_news->the_post();
                $tz_nightclub_video_mp4     =   get_post_meta( $post -> ID, 'nightclub_portfolio_video_mp4', true );
                $tz_nightclub_video_ogv     =   get_post_meta( $post -> ID, 'nightclub_portfolio_video_ogv', true );
                $tz_nightclub_video_webm    =   get_post_meta( $post -> ID, 'nightclub_portfolio_video_webm', true );

                ?>

                { type: "video/mp4",  src: "<?php echo esc_url( $tz_nightclub_video_mp4 ); ?>" },
                { type: "video/webm", src: "<?php echo esc_url( $tz_nightclub_video_ogv ); ?>" },
                { type: "video/ogg",  src: "<?php echo esc_url( $tz_nightclub_video_webm ); ?>" },

                <?php
                endwhile;
                endif;
                wp_reset_postdata();
                ?>

            ]);

            var isPlaying = false,
                player = BV.getPlayer(),
                $tz_btn_video_play_pause = jQuery('.tz_btn_video_play_pause');

            // Playlist button click starts video
            jQuery('.playlist-btn').on('click', function(e) {
                e.preventDefault();

                jQuery('#big-video-wrap').fadeOut("1600",function(){
                    jQuery('#big-video-wrap').fadeIn('1600');
                });
                BV.show(jQuery(this).attr('href'));
                player.play();
                isPlaying = true;
                jQuery('.tz_blog_video_content, .tz_blog_more_video, .tz_navigation_pagination, .tz_footer').addClass('tz_video_blog_active').removeClass('tz_active_important');
                jQuery('.tz_blog_slider_video_bk').addClass('tz_blog_slider_video_bk_action').removeClass('tz_active_bk_important');
                jQuery('.tz_btn_video_play_pause').html('<i class="icon-music-pause-button"></i>');

                mouse_move();

            });

            /* Video Play/Pause toggle */

            <?php if (  $mouse_move == 1 ) : ?>

                $tz_btn_video_play_pause.on('click', function(e) {

                    if (isPlaying) {

                        player.pause();
                        isPlaying = false;
                        jQuery('.tz_blog_video_content, .tz_blog_more_video, .tz_navigation_pagination, .tz_footer').removeClass('tz_video_blog_active');
                        jQuery('.tz_blog_slider_video_bk').removeClass('tz_blog_slider_video_bk_action');
                        jQuery('.tz_btn_video_play_pause').html('<i class="icon-music-play-button"></i>');

                    } else {

                        player.play();
                        isPlaying = true;
                        jQuery('.tz_blog_video_content, .tz_blog_more_video, .tz_navigation_pagination, .tz_footer').addClass('tz_video_blog_active');
                        jQuery('.tz_blog_slider_video_bk').addClass('tz_blog_slider_video_bk_action');
                        jQuery('.tz_btn_video_play_pause').html('<i class="icon-music-pause-button"></i>');

                    }

                });

            <?php else: ?>

                $tz_btn_video_play_pause.on('click', function(e) {

                    if (isPlaying) {

                        player.pause();
                        isPlaying = false;
                        jQuery('.tz_blog_video_content, .tz_blog_more_video, .tz_navigation_pagination, .tz_footer').removeClass('tz_video_blog_active').addClass('tz_active_important');
                        jQuery('.tz_blog_slider_video_bk').removeClass('tz_blog_slider_video_bk_action').addClass('tz_active_bk_important');
                        jQuery('.tz_btn_video_play_pause').html('<i class="icon-music-play-button"></i>');
                        jQuery(document).unbind("mousemove");


                    } else {

                        player.play();
                        isPlaying = true;
                        jQuery('.tz_blog_video_content, .tz_blog_more_video, .tz_navigation_pagination, .tz_footer').addClass('tz_video_blog_active').removeClass('tz_active_important');
                        jQuery('.tz_blog_slider_video_bk').addClass('tz_blog_slider_video_bk_action').removeClass('tz_active_bk_important');
                        jQuery('.tz_btn_video_play_pause').html('<i class="icon-music-pause-button"></i>');

                        mouse_move();


                    }

                });

            <?php endif; ?>
            
            /* End Video Play/Pause toggle */

            /* mousemove video */
            function mouse_move() {

                var timeout = null;
                jQuery(document).on('mousemove', function() {

                    if (timeout !== null) {

                        jQuery('.tz_blog_video_content, .tz_blog_more_video, .tz_navigation_pagination, .tz_footer').removeClass('tz_video_blog_active');
                        jQuery('.tz_blog_slider_video_bk').removeClass('tz_blog_slider_video_bk_action');
                        clearTimeout(timeout);

                    }

                    timeout = setTimeout(function() {

                        jQuery('.tz_blog_video_content, .tz_blog_more_video, .tz_navigation_pagination, .tz_footer').addClass('tz_video_blog_active');
                        jQuery('.tz_blog_slider_video_bk').addClass('tz_blog_slider_video_bk_action');

                    }, 2000);
                });
            }
            
            function mouse_move_2() {

                var timeout = null;
                jQuery(document).on('mousemove', function() {

                    if (timeout !== null) {

                        jQuery('.tz_blog_video_content, .tz_blog_more_video, .tz_navigation_pagination, .tz_footer').removeClass('tz_video_blog_active');
                        jQuery('.tz_blog_slider_video_bk').removeClass('tz_blog_slider_video_bk_action');
                        clearTimeout(timeout);
                    }

                    timeout = setTimeout(function() {
                        player.play();
                        isPlaying = true;
                        jQuery('.tz_blog_video_content, .tz_blog_more_video, .tz_navigation_pagination, .tz_footer').addClass('tz_video_blog_active');
                        jQuery('.tz_blog_slider_video_bk').addClass('tz_blog_slider_video_bk_action');
                        jQuery('.tz_btn_video_play_pause').html('<i class="icon-music-pause-button"></i>');

                    }, 2000);
                });

            }
            
            <?php if ( $mouse_move == 1 ) : ?>

            mouse_move_2();

            <?php endif; ?>
            /* End mousemove video */

            // set up navigation
            jQuery('.nav-link').on('click',function(e){
                e.preventDefault();
                scrollToSection(jQuery(this).attr('href'));
            });

            function scrollToSection(id) {
                jQuery('body,html').animate({scrollTop: jQuery(id).offset().top - 50}, 400);
            }
        });
    </script>

    <?php

    $tz_nightclub  =   ob_get_contents();
    ob_end_clean();
    return $tz_nightclub;

}
add_shortcode( 'tz_video_blog_slider','tz_nightclub_video_blog_slider' );

?>