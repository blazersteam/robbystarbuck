<?php

$tz_nightclub_cat_blog      = array();
$tz_nightclub_categories_b  = get_categories();

if ( isset( $tz_nightclub_categories_b ) && !empty($tz_nightclub_categories_b) ):
    foreach( $tz_nightclub_categories_b as $tz_nightclub_cate_blog ){
        $tz_nightclub_cat_blog[$tz_nightclub_cate_blog->name] = $tz_nightclub_cate_blog->term_id;
    }
endif;

vc_map( array(
    'base'      =>  'tz_video_blog_slider',
    'name'      =>  'Video Blog Slider',
    'icon'      =>  'tz_vc_icon_video_blog_slider',
    'category'  =>  'Night club',
    'params'    =>  array(

        array(
            'type'          =>  'textfield',
            'admin_label' 	=>  true,
            'heading'       =>  'Title',
            'param_name'    =>  'title_slider',
            'value'         =>  'Live Stream in 1040 club',
            'group'         =>  'Post',
        ),
        array(
            'type'          =>  'textfield',
            'admin_label' 	=>  true,
            'heading'       =>  'Sub Title',
            'param_name'    =>  'sub_title_slider',
            'value'         =>  'The Video',
            'group'         =>  'Post',
        ),
        array(
            'type'          =>  'checkbox',
            'heading'       =>  'Category Blog',
            'param_name'    =>  'category_blog_arr',
            'class'         =>  'tz_slider_blog',
            'value'         =>  $tz_nightclub_cat_blog,
            'description'   =>  'Choose category blog.',
            'group'         =>  'Post',
        ),
        array(
            'type'          =>  'textfield',
            'admin_label' 	=>  true,
            'heading'       =>  'Limit Blog',
            'param_name'    =>  'posts_per_page',
            'value'         =>  3,
            'group'         =>  'Post',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Order By',
            'param_name'    =>  'orderby',
            'value'         =>  array(
                'Date'      =>  'date',
                'ID'        =>  'id',
                'Title'     =>  'title',
            ),
            'group'         =>  'Post',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Order',
            'param_name'    =>  'order',
            'value'         =>  array(
                'Z --> A'   =>  'desc',
                'A --> Z'   =>  'asc',
            ),
            'group'         =>  'Post',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Limit Words Content',
            'param_name'    =>  'limit_words_content',
            'value'         =>  30,
            'group'         =>  'Post',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title',
            'param_name'    =>  'color_title',
            'value'         =>  '',
            'group'         =>  'Color Post',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Sub Title',
            'param_name'    =>  'color_sub_title',
            'value'         =>  '',
            'group'         =>  'Color Post',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Date Post',
            'param_name'    =>  'color_date_post',
            'value'         =>  '',
            'group'         =>  'Color Post',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title Post',
            'param_name'    =>  'color_title_post',
            'value'         =>  '',
            'group'         =>  'Color Post',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Content Post',
            'param_name'    =>  'color_content_post',
            'value'         =>  '',
            'group'         =>  'Color Post',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Button Play',
            'param_name'    =>  'color_btn_play',
            'value'         =>  '',
            'group'         =>  'Color Post',
        ),
        array(
            'type'          =>  'textfield',
            'admin_label' 	=>  true,
            'heading'       =>  'Title More Videos',
            'param_name'    =>  'title_more_videos',
            'value'         =>  'More Videos',
            'group'         =>  'More Videos',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title More Videos',
            'param_name'    =>  'color_title_more',
            'value'         =>  '',
            'group'         =>  'More Videos',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Icon More Videos',
            'param_name'    =>  'color_icon_more',
            'value'         =>  '',
            'group'         =>  'More Videos',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title Post More Videos',
            'param_name'    =>  'color_title_post_more',
            'value'         =>  '',
            'group'         =>  'More Videos',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Mouse Move',
            'param_name'    =>  'mouse_move',
            'value'         =>  array(
                'No'    =>  0,
                'Yes'   =>  1,
            ),
            'group'         =>  'Setting video',
        ),

    )
) );