<?php

$tzcf7 = get_posts('post_type="wpcf7_contact_form"&numberposts=-1');

$contact_forms = array();

if ( $tzcf7 ) {
    foreach ($tzcf7 as $tzcform) {
        $contact_forms[$tzcform->post_title] = $tzcform->ID;
    }
} else {
    $contact_forms[__('No contact forms found', 'js_composer')] = 0;
}

vc_map( array(
    'base'      =>  'tz_book_table',
    'name'      =>  'Book The Table',
    'icon'      =>  'tz_icon_table',
    'category'  =>  'Night club',
    'params'    =>  array(

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Title Book The Table',
            'param_name'    =>  'title_book_table',
            'value'         =>  'BOOK THE TABLE',
        ),

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Title Contact Form',
            'param_name'    =>  'title_contact_form',
            'value'         =>  'RESERVATION',
        ),

        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Type Book Table',
            'param_name'    =>  'type_book',
            'value'         =>  array(
                'Use Contact Form'  =>  1,
                'Use Custom Link'   =>  2,
            ),
        ),

        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Select Contact Form',
            'param_name'    =>  'contact_form_id',
            'value'         =>  $contact_forms,
            'description'   =>  'Choose previously created contact form from the drop down list.',
            'dependency'    =>  array( 'element' => "type_book", 'value' => array( '1' ) ),
        ),

        array(
            'type'          =>  'vc_link',
            'heading'       =>  'URL (Link)',
            'param_name'    =>  'custom_link',
            'description'   =>  'Add custom link.',
            'dependency'    =>  array( 'element' => "type_book", 'value' => array( '2' ) ),
        ),

        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title',
            'param_name'    =>  'color_title',
            'value'         =>  '',
            'group'         =>  'Setting Color'
        ),

        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title Contact Form',
            'param_name'    =>  'color_title_contact',
            'value'         =>  '',
            'dependency'    =>  array( 'element' => "type_book", 'value' => array( '1' ) ),
            'group'         =>  'Setting Color'
        ),

    )
) );