<?php
function tz_nightclub_book_table( $atts ) {

    $title_book_table = $color_title = $title_contact_form = $color_title_contact = $contact_form_id = $type_book = $custom_link = '';

    extract(shortcode_atts(array(

        'title_book_table'      =>  'BOOK THE TABLE',
        'color_title'           =>  '',
        'title_contact_form'    =>  'RESERVATION',
        'color_title_contact'   =>  '',
        'type_book'             =>  1,
        'contact_form_id'       =>  '',
        'custom_link'           =>  '',

    ), $atts));

    ob_start();

    $vc_custom_link = '';

    if ( $type_book == 2 ) :
        $vc_custom_link = vc_build_link( $custom_link );
    endif;

?>

    <div class="tz_book_table">
        <h4 class="tz_title_book" <?php echo( $color_title != '' && $type_book == 1 ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>
            <?php

            if ( $type_book == 1 ) :
                echo balanceTags( $title_book_table );
            else:

            ?>

                <a <?php echo ( $vc_custom_link['target'] != '' ? 'target="' . esc_attr( $vc_custom_link['target'] ) . '"' : '') .' '. ( $vc_custom_link['url'] != '' ? 'href="' . esc_attr( $vc_custom_link['url'] ) . '"' : '' ) . ' '. ( $vc_custom_link['title'] != '' ? 'title="' . esc_attr( $vc_custom_link['title'] ) . '"' : '' )  ; ?> <?php echo( $color_title != '' ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>
                    <?php echo balanceTags( $title_book_table ); ?>
                </a>

            <?php endif; ?>

        </h4>

        <?php if ( $type_book == 1 ) : ?>

            <div class="tz_reservation">
                <div class="tz_reservation_contact_box">
                    <div class="tz_reservation_contact_form">
                        <span class="tz_reservation_close">
                            <i class="fa fa-close"></i>
                        </span>
                        <h3 class="tz_title_contact_form" <?php echo( $color_title_contact != '' ? 'style="color:' . esc_attr( $color_title_contact ) . '"' : '' ); ?>>
                            <?php echo balanceTags( $title_contact_form ); ?>
                        </h3>
                        <?php echo do_shortcode( '[contact-form-7 id="'.esc_attr( $contact_form_id ).'"]' ); ?>
                    </div>
                </div>

            </div>

        <?php endif; ?>

    </div>


<?php

    $tz_nightclub  =   ob_get_contents();
    ob_end_clean();
    return $tz_nightclub;

}
add_shortcode( 'tz_book_table','tz_nightclub_book_table' );

?>
