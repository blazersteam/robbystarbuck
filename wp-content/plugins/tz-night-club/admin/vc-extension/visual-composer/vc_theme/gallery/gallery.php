<?php

function tz_nightclub_gallery( $atts ) {

    $title_gallery = $sub_title_gallery = $images_gallery = $number_images_column = $number_images_row = '';
    $show_hide_title = $color_title = $color_sub_title = $show_hide_pre_next = $auto_gallery = $loop_gallery = '';

    extract(shortcode_atts(array(

        'title_gallery'         =>  'Gallery club',
        'sub_title_gallery'     =>  'The gallery',
        'images_gallery'        =>  '',
        'number_images_column'  =>  4,
        'number_images_row'     =>  3,
        'show_hide_title'       =>  1,
        'color_title'           =>  '',
        'color_sub_title'       =>  '',
        'show_hide_pre_next'    =>  1,
        'auto_gallery'          =>  0,
        'loop_gallery'          =>  0

    ), $atts));

    ob_start();

    $images_gallery_arr = array();

    if ( isset( $images_gallery ) && !empty( $images_gallery ) ):

        $images_gallery_arr    = explode( ',', $images_gallery );

    endif;

    $page = ceil(count($images_gallery_arr)/($number_images_column*$number_images_row));

    if ( $page < 10 ) :
        $number =   '0'.$page;
    else:
        $number =   $page;
    endif;


?>

    <div class="tz_nightclub_gallery">

        <?php if ( ( ( $number_images_column*$number_images_row ) != count($images_gallery_arr) ) && $show_hide_pre_next == 1 ) : ?>

            <div class="tz_pre_gallery tz_next_pre_gallery">
                <span>
                    <i class="lnr lnr-arrow-left"></i>
                    <?php echo "01 / ".$number; ?>
                </span>
            </div>
            <div class="tz_next_gallery tz_next_pre_gallery">
                <span>
                    <?php echo "02 / ".$number; ?>
                    <i class="lnr lnr-arrow-right"></i>
                </span>
            </div>

        <?php endif; ?>

        <?php if ( ($title_gallery !='' || $sub_title_gallery !='') && $show_hide_title == 1 ) : ?>

            <div class="tz_nightclub_gallery_box">
                <h5>
                    <span class="tz_nightclub_gallery_title" <?php echo( $color_title != '' ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>
                        <?php echo esc_attr( $title_gallery ); ?>
                    </span>
                    <span class="tz_nightclub_gallery_sub_title" <?php echo( $color_sub_title != '' ? 'style="color:' . esc_attr( $color_sub_title ) . '"' : '' ); ?>>
                        <?php echo esc_attr( $sub_title_gallery ); ?>
                    </span>
                </h5>
            </div>

        <?php endif; ?>

        <div class="tz_nightclub_gallery_slider owl-carousel owl-theme " data-number-column="<?php echo esc_attr( $number_images_column ); ?>" data-number-row="<?php echo esc_attr( $number_images_row ); ?>" data-auto-gallery="<?php echo esc_attr( $auto_gallery ); ?>" data-loop-gallery="<?php echo esc_attr( $loop_gallery ); ?>">

            <?php

            $nightclub_images_column = $number_images_column;
            $nightclub_images_row = $number_images_row;

            $nightclub_total_image = count( $images_gallery_arr );

            $nightclub_gallery_item_number = ceil($nightclub_total_image/($nightclub_images_column*$nightclub_images_row));

            for( $j = 1; $j <= $nightclub_gallery_item_number; $j++ ) :

            ?>
                <div class="tz_nightclub_gallery_item">

                    <?php

                    $nightclub_count = $j*$nightclub_images_row*$nightclub_images_column;

                    if( $nightclub_count <= $nightclub_total_image ) {

                        if( $j==1 ){

                            for( $i = 0; $i < $nightclub_count; $i++ ):

                                $d = ceil( $i%$nightclub_images_column );

                                if( $d==0 ){
                    ?>
                                    <div class="tz_gallery_item">

                                <?php }

                                echo '<div class="tz_img_item_gallery"><div class="tz_img_item_gallery_bk"></div><a class="fancybox" rel="alternate" href="'.wp_get_attachment_url( $images_gallery_arr[$i] ).'">' .wp_get_attachment_image( $images_gallery_arr[$i], 'full' ). '</a></div>';

                                if( $d==($nightclub_images_column-1) ){ ?>
                                    </div>
                                <?php } ?>

                                <?php

                            endfor;

                        }else{

                            for( $i = ($j-1)*$nightclub_images_column*$nightclub_images_row; $i < $nightclub_count; $i++ ):
                                $d = ceil( $i%$nightclub_images_column );

                                if( $d==0 ){
                            ?>
                                    <div class="tz_gallery_item">

                            <?php }

                                echo '<div class="tz_img_item_gallery"><div class="tz_img_item_gallery_bk"></div><a class="fancybox" rel="alternate" href="'.wp_get_attachment_url( $images_gallery_arr[$i] ).'">'.wp_get_attachment_image( $images_gallery_arr[$i], 'full' ).'</a></div>';

                                if( $d==($nightclub_images_column-1) ){ ?>

                                    </div>

                                <?php } ?>

                                <?php

                            endfor;
                        }
                    }else{

                        for( $i = ($j-1)*$nightclub_images_column*$nightclub_images_row; $i < $nightclub_total_image; $i++ ):
                            $d = ceil($i%$nightclub_images_column);

                            if( $d==0 ){
                        ?>

                                <div class="tz_gallery_item">

                        <?php }

                            echo '<div class="tz_img_item_gallery"><div class="tz_img_item_gallery_bk"></div><a class="fancybox" rel="alternate" href="'.wp_get_attachment_url( $images_gallery_arr[$i] ).'">'.wp_get_attachment_image( $images_gallery_arr[$i], 'full' ).'</a></div>';

                            if( $d==($nightclub_images_column-1) ){ ?>

                                </div>

                            <?php } ?>

                        <?php  endfor;

                    }

                    ?>
                </div>

            <?php endfor; ?>

        </div>
    </div>

<?php

    $tz_nightclub  =   ob_get_contents();
    ob_end_clean();
    return $tz_nightclub;

}
add_shortcode( 'tz_gallery','tz_nightclub_gallery' );

?>