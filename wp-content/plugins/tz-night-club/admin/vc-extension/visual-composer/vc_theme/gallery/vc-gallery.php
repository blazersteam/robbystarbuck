<?php

vc_map( array(
    'base'      =>  'tz_gallery',
    'name'      =>  'Gallery',
    'icon'      =>  'tz_vc_icon_gallery',
    'category'  =>  'Night club',
    'params'    =>  array(

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Title Gallery',
            'param_name'    =>  'title_gallery',
            'admin_label'   =>  true,
            'value'         =>  'Gallery club',
            'group'         =>  'Gallery'
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Sub Title Gallery',
            'param_name'    =>  'sub_title_gallery',
            'value'         =>  'The gallery',
            'group'         =>  'Gallery'
        ),
        array(
            'type'          =>  'attach_images',
            'heading'       =>  'Upload Images',
            'param_name'    =>  'images_gallery',
            'value'         =>  '',
            'group'         =>  'Gallery'
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Number of images in a column',
            'param_name'    =>  'number_images_column',
            'admin_label'   =>  true,
            'value'         =>  4,
            'group'         =>  'Gallery'
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Number of images in a row',
            'param_name'    =>  'number_images_row',
            'admin_label'   =>  true,
            'value'         =>  3,
            'group'         =>  'Gallery'
        ),

        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Show Or Hide Title & Sub Title',
            'param_name'    =>  'show_hide_title',
            'value'         =>  array(
                'Show'  => 1,
                'Hide'  => 0,
            ),
            'group'         =>  'Option Gallery'
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title',
            'param_name'    =>  'color_title',
            'value'         =>  '',
            'dependency'    =>  Array('element' => "show_hide_title", 'value' => array('1')),
            'group'         =>  'Option Gallery',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Sub Title',
            'param_name'    =>  'color_sub_title',
            'value'         =>  '',
            'dependency'    =>  Array('element' => "show_hide_title", 'value' => array('1')),
            'group'         =>  'Option Gallery',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Show Or Hide Pre & Next',
            'param_name'    =>  'show_hide_pre_next',
            'value'         =>  array(
                'Show'  => 1,
                'Hide'  => 0,
            ),
            'group'         =>  'Option Gallery'
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Auto Play',
            'param_name'    =>  'auto_gallery',
            'value'         =>  array(
                'No'    => 0,
                'Yes'   => 1,
            ),
            'group'         =>  'Option Gallery'
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Loop',
            'param_name'    =>  'loop_gallery',
            'value'         =>  array(
                'No'    => 0,
                'Yes'   => 1,
            ),
            'group'         =>  'Option Gallery'
        ),

    )
) );