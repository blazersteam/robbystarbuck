<?php

vc_map( array(
    'base'      =>  'tz_menu',
    'name'      =>  'Menu',
    'icon'      =>  'tz_vc_icon_menu',
    'category'  =>  'Night club',
    'params'    =>  array(

        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Primary Menu',
            'param_name'    =>  'select_senu',
            'value'         =>  array(
                'Primary Menu'  =>  1
            ),
        ),

    )
) );