<?php
function tz_nightclub_event( $atts ) {

    $title_event = $sub_title_event = $category_event_arr = $posts_per_page = $orderby = $order = $txt_book_table = '';
    $color_title = $color_sub_title = $color_title_post_event = $color_address_events = $color_month_day = $color_year = $color_text_book = '';

    extract(shortcode_atts(array(

        'title_event'               =>  'Next Event',
        'sub_title_event'           =>  'Upcoming Event',
        'category_event_arr'        =>  '',
        'posts_per_page'            =>  3,
        'orderby'                   =>  'date',
        'order'                     =>  'desc',
        'txt_book_table'            =>  'Book the table',
        'color_title'               =>  '',
        'color_sub_title'           =>  '',
        'color_title_post_event'    =>  '',
        'color_address_events'      =>  '',
        'color_month_day'           =>  '',
        'color_year'                =>  '',
        'color_text_book'           =>  '',

    ), $atts));

    ob_start();

    $tz_tribe_cat = array();

    if ( $category_event_arr !='' ) :
        $cat_tribe_id = explode( ",",$category_event_arr );
        if( is_array( $cat_tribe_id ) ){
            sort( $cat_tribe_id );
            $count_cat  =   count( $cat_tribe_id );

            for( $i=0; $i<$count_cat; $i++ ){
                $tz_tribe_cat[]  =   (int)$cat_tribe_id[$i];
            }

        }else{
            $tz_tribe_cat[]    = (int)$cat_tribe_id;
        }
    endif;

    if ( isset( $cat_tribe_events ) && !empty( $cat_tribe_events ) ):


        $args = array(
            'post_status'       =>  'publish',
            'post_type'         =>  'tribe_events',
            'posts_per_page'    =>  $posts_per_page,
            'orderby'           =>  $orderby,
            'order'             =>  $order,
            'eventDisplay'      =>  'custom',
            'tax_query'         =>  array(
                array(
                    'taxonomy'  =>  'tribe_events_cat',
                    'field'     =>  'id',
                    'terms'     =>   $tz_tribe_cat
                )
            )
        );

    else:
        $args = array(
            'post_type'         =>  'tribe_events',
            'posts_per_page'    =>  $posts_per_page,
            'orderby'           =>  $orderby,
            'order'             =>  $order,
            'eventDisplay'      =>  'custom',
        );
    endif;

    $tribe_event = new WP_Query( $args );

?>

    <div class="tz_upcoming_event">
        <div class="row tz_custom_row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 tz_custom_padding_right">
                <div class="tz_upcoming_event_image">

                    <?php

                    if ( $tribe_event->have_posts() ) :
                        while ( $tribe_event->have_posts() ) :
                            $tribe_event->the_post();
                            if ( $tribe_event->current_post == 0 ) :
                    ?>

                            <img src="<?php the_post_thumbnail_url( 'full' ); ?>" alt="<?php echo get_bloginfo('title'); ?>">

                    <?php
                            endif;
                        endwhile;
                    endif;
                    wp_reset_postdata();

                    ?>

                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 tz_custom_padding_left">
                <div class="tz_element_title_box">
                    <div class="tz_element_title">
                        <h3 class="tz_title" <?php echo( $color_title != '' ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>
                            <?php echo esc_attr( $title_event ); ?>
                        </h3>
                        <h2 class="tz_sub_title" <?php echo( $color_sub_title != '' ? 'style="color:' . esc_attr( $color_sub_title ) . '"' : '' ); ?>>
                            <?php echo esc_attr( $sub_title_event ); ?>
                        </h2>
                    </div>
                </div>
                <div class="tz_night_club_event">
                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                    <div class="viewport">
                        <div class="overview">
                            <?php

                            if ( $tribe_event->have_posts() ) :
                                while ( $tribe_event->have_posts() ) :
                                    $tribe_event->the_post();
                                    global $post;

                                    $tz_night_club_event_item_fist  =   '';

                                    if ( $tribe_event->current_post == 0 ) :
                                        $tz_night_club_event_item_fist = ' tz_night_club_event_item_fist';
                                    endif;

                                    $start_day = tribe_get_start_date( null, false, 'Y' );
                                    $start_month = tribe_get_start_date( null, false, 'M d' );

                            ?>
                                    <div class="tz_night_club_event_item<?php echo esc_attr( $tz_night_club_event_item_fist ); ?>" data-image="<?php the_post_thumbnail_url( 'full' ) ?>">
                                        <div class="tz_night_club_event_item_table">
                                            <h4 class="tz_title_event">
                                                <a class="tz_event_link" href="<?php the_permalink() ?>" <?php echo( $color_title_post_event != '' ? 'style="color:' . esc_attr( $color_title_post_event ) . '"' : '' ); ?>>
                                                    <?php the_title(); ?>
                                                </a>
                                                <?php if ( tribe_address_exists() ) : ?>
                                                    <span class="tz_night_club_event_address" <?php echo( $color_address_events != '' ? 'style="color:' . esc_attr( $color_address_events ) . '"' : '' ); ?>>
                                                        <?php echo tribe_get_full_address(); ?>
                                                    </span>
                                                <?php endif; ?>
                                            </h4>
                                            <div class="tz_date_event_book">
                                                <h4 class="tz_date_event">
                                                <span class="tz_date_event_month_day" <?php echo( $color_month_day != '' ? 'style="color:' . esc_attr( $color_month_day ) . '"' : '' ); ?>>
                                                    <?php echo balanceTags( $start_month ); ?>
                                                </span>
                                                <span class="tz_date_event_day" <?php echo( $color_year != '' ? 'style="color:' . esc_attr( $color_year ) . '"' : '' ); ?>>
                                                    <?php echo balanceTags( $start_day ); ?>
                                                </span>
                                                </h4>
                                                <h4 class="tz_event_book">
                                                    <a href="<?php the_permalink(); ?>" <?php echo( $color_text_book != '' ? 'style="color:' . esc_attr( $color_text_book ) . '"' : '' ); ?>>
                                                        <?php echo esc_attr( $txt_book_table ); ?>
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>

                            <?php
                                endwhile;
                            endif;
                            wp_reset_postdata();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php

    $tz_nightclub  =   ob_get_contents();
    ob_end_clean();
    return $tz_nightclub;

}
add_shortcode( 'tz_event','tz_nightclub_event' );

?>