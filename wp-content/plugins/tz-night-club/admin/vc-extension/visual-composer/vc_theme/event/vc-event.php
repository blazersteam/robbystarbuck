<?php

$cat_events_arr        =   array();
$category_events_arr   =   get_categories( array('taxonomy'=>'tribe_events_cat') );
if ( isset( $category_events_arr ) && !empty($category_events_arr) ) :
    foreach( $category_events_arr as $cate_events_arr ) {
        $cat_events_arr[$cate_events_arr->name]   =   $cate_events_arr->term_id;
    }
endif;

vc_map( array(
    'base'      =>  'tz_event',
    'name'      =>  'Events',
    'icon'      =>  'tz_vc_icon_event',
    'category'  =>  'Night club',
    'params'    =>  array(

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Title Event',
            'param_name'    =>  'title_event',
            'admin_label'   =>  true,
            'value'         =>  'Next Event',
            'group'         =>  'Events',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Sub Title Event',
            'param_name'    =>  'sub_title_event',
            'value'         =>  'Upcoming Event',
            'group'         =>  'Events',
        ),
        array(
            'type'          =>  'checkbox',
            'heading'       =>  'Category Events',
            'param_name'    =>  'category_event_arr',
            'value'         =>  $cat_events_arr,
            'description'   =>  'Choose category Events.',
            'group'         =>  'Events',
        ),
        array(
            'type'          =>  'textfield',
            'admin_label' 	=>  true,
            'heading'       =>  'Limit Events',
            'param_name'    =>  'posts_per_page',
            'value'         =>  3,
            'group'         =>  'Events',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Order By',
            'param_name'    =>  'orderby',
            'value'         =>  array(
                'Date'      =>  'date',
                'ID'        =>  'id',
                'Title'     =>  'title',
            ),
            'group'         =>  'Events',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Order',
            'param_name'    =>  'order',
            'value'         =>  array(
                'Z --> A'   =>  'desc',
                'A --> Z'   =>  'asc',
            ),
            'group'         =>  'Events',
        ),
        array(
            'type'          =>  'textfield',
            'admin_label' 	=>  true,
            'heading'       =>  'Text Book',
            'param_name'    =>  'txt_book_table',
            'value'         =>  'Book the table',
            'group'         =>  'Events',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title Event',
            'param_name'    =>  'color_title',
            'value'         =>  '',
            'group'         =>  'color',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Sub Title Event',
            'param_name'    =>  'color_sub_title',
            'value'         =>  '',
            'group'         =>  'color',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title Post Event',
            'param_name'    =>  'color_title_post_event',
            'value'         =>  '',
            'group'         =>  'color',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Address Events',
            'param_name'    =>  'color_address_events',
            'value'         =>  '',
            'group'         =>  'color',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Month Day',
            'param_name'    =>  'color_month_day',
            'value'         =>  '',
            'group'         =>  'color',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Year',
            'param_name'    =>  'color_year',
            'value'         =>  '',
            'group'         =>  'color',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Text Book',
            'param_name'    =>  'color_text_book',
            'value'         =>  '',
            'group'         =>  'color',
        ),

    )
) );