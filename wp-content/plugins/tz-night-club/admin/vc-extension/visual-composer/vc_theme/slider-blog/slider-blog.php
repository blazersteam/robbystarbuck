<?php
function tz_nightclub_slider_blog( $atts ) {

    $category_blog_arr = $posts_per_page = $orderby = $order = $muted_video = $pagination = $navigation = $auto = '';

    extract(shortcode_atts(array(

        'category_blog_arr' =>  '',
        'posts_per_page'    =>  3,
        'orderby'           =>  'date',
        'order'             =>  'desc',
        'muted_video'       =>  1,
        'pagination'        =>  1,
        'navigation'        =>  0,
        'auto'              =>  0

    ), $atts));

    ob_start();

    if ( isset($category_blog_arr) && !empty($category_blog_arr) ):
        $args = array(
            'post_type'         =>  'post',
            'cat'               =>  $category_blog_arr,
            'posts_per_page'    =>  $posts_per_page,
            'orderby'           =>  $orderby,
            'order'             =>  $order,
        );
    else:
        $args = array(
            'post_type'         =>  'post',
            'posts_per_page'    =>  $posts_per_page,
            'orderby'           =>  $orderby,
            'order'             =>  $order,
        );
    endif;

    $recent_news    =   new WP_Query( $args );

    ?>

    <div class="tz_slider_blog" data-pagination="<?php echo esc_attr( $pagination ); ?>" data-auto="<?php echo esc_attr( $auto ); ?>">
       <div id="slides">
           <div class="slides-container">
                <?php
                    if ( $recent_news->have_posts() ) :
                        while ( $recent_news->have_posts() ) :
                            global $post;
                            $recent_news->the_post();

                            $nightclub_portfolio_type   =   get_post_meta( $post -> ID, 'nightclub_portfolio_type',true );
                            $tz_nightclub_video_mp4     =   get_post_meta( $post -> ID, 'nightclub_portfolio_video_mp4', true );
                            $tz_nightclub_video_ogv     =   get_post_meta( $post -> ID, 'nightclub_portfolio_video_ogv', true );
                            $tz_nightclub_video_webm    =   get_post_meta( $post -> ID, 'nightclub_portfolio_video_webm', true );

                    ?>
                            <div class="tz_slides_post">

                                <?php if ( $nightclub_portfolio_type == 'video' ) : ?>

                                    <video class="video_background" autoplay loop <?php echo( $muted_video == 1 ? 'muted' : '' ); ?>>
                                        <source type="video/mp4" src="<?php echo esc_url( $tz_nightclub_video_mp4 ); ?>" />
                                        <source type="video/ogg" src="<?php echo esc_url( $tz_nightclub_video_ogv ); ?>" />
                                        <source type="video/webm" src="<?php echo esc_url( $tz_nightclub_video_webm ); ?>" />
                                    </video>

                                <?php

                                else:
                                    the_post_thumbnail('full');
                                endif;

                                ?>

                                <div class="tz_content_slider_post">
                                    <div class="ds-table-cell-left-center">
                                        <div class="tz_content_slider_box_post">
                                            <span class="tz_date_post">
                                                <?php echo get_the_date(); ?>
                                            </span>
                                            <h2 class="tz_title_post">
                                                <a href="<?php the_permalink() ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </h2>

                                            <?php if ( $nightclub_portfolio_type == 'video' ) : ?>

                                                <span class="tz_btn_play_video_bk">
                                                    <i class="fa fa-play" aria-hidden="true"></i>
                                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                                    <span class="tz_text_video_bk">
                                                        <?php esc_html_e('Play/Pause Video', tz_plazart); ?>
                                                    </span>
                                                </span>

                                            <?php endif; ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                <?php
                        endwhile;
                    endif;
                wp_reset_postdata();
                ?>
           </div>

           <?php if ( $navigation == 1 ) : ?>

           <nav class="slides-navigation">
               <a href="#" class="prev">
                   <i class="fa fa-angle-left" aria-hidden="true"></i>
               </a>
               <a href="#" class="next">
                   <i class="fa fa-angle-right" aria-hidden="true"></i>
               </a>
           </nav>

            <?php endif; ?>

       </div>
    </div>

    <?php

    $tz_nightclub  =   ob_get_contents();
    ob_end_clean();
    return $tz_nightclub;

}
add_shortcode( 'tz_slider_blog','tz_nightclub_slider_blog' );

?>