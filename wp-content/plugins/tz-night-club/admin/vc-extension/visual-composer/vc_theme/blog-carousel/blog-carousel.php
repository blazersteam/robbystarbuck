<?php
function tz_nightclub_blog_carousel( $atts ) {

    $category_blog_arr = $posts_per_page = $orderby = $order = $number_item = $pagination = $navigation = $auto = '';
    $title = $color_title = $sub_title = $color_sub_title = '';

    extract(shortcode_atts(array(

        'title'             =>  'Recent news',
        'color_title'       =>  '',
        'sub_title'         =>  'From our blog',
        'color_sub_title'   =>  '',
        'category_blog_arr' =>  '',
        'posts_per_page'    =>  3,
        'orderby'           =>  'date',
        'order'             =>  'desc',
        'number_item'       =>  4,
        'pagination'        =>  1,
        'navigation'        =>  0,
        'auto'              =>  0

    ), $atts));

    ob_start();

    if ( isset($category_blog_arr) && !empty($category_blog_arr) ):
        $args = array(
            'post_type'         =>  'post',
            'cat'               =>  $category_blog_arr,
            'posts_per_page'    =>  $posts_per_page,
            'orderby'           =>  $orderby,
            'order'             =>  $order,
        );
    else:
        $args = array(
            'post_type'         =>  'post',
            'posts_per_page'    =>  $posts_per_page,
            'orderby'           =>  $orderby,
            'order'             =>  $order,
        );
    endif;

    $recent_news    =   new WP_Query( $args );

    $tz_nightclub_carousel_box_top  =   '';
    if ( $title !='' || $sub_title != '' ) :
        $tz_nightclub_carousel_box_top  =   ' tz_blog_carousel_box_top';
    endif;

?>

    <div class="tz_blog_carousel_box<?php echo esc_attr( $tz_nightclub_carousel_box_top ); ?>">

        <?php if ( $title !='' || $sub_title != '' ) : ?>

            <div class="tz_element_title tz_element_title_blog_carousel">

                <?php if ( $title !='' ) : ?>

                    <h3 class="tz_title" <?php echo( $color_title != '' ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>
                        <?php echo esc_attr( $title ); ?>
                    </h3>

                <?php endif; ?>


                <?php if ( $sub_title !='' ) : ?>

                    <h2 class="tz_sub_title" <?php echo( $color_sub_title != '' ? 'style="color:' . esc_attr( $color_sub_title ) . '"' : '' ); ?>>
                        <?php echo esc_attr( $sub_title ); ?>
                    </h2>

                <?php endif; ?>

            </div>

        <?php endif; ?>

        <div class="tz_blog_carousel" data-limit-post="<?php echo esc_attr( $posts_per_page ); ?>" data-orderby="<?php echo esc_attr( $orderby ); ?>" data-order-post="<?php echo esc_attr( $order ); ?>">

            <div class="tz_box_blog_carousel">

                <span class="tz_partner_prev_next tz_partner_prev">
                    <i class="fa fa-long-arrow-left"></i>
                        <?php esc_html_e( 'Prev','nightclub' ); ?>
                </span>
                <span class="tz_partner_prev_next tz_partner_next">
                    <?php esc_html_e( 'Next','nightclub' ); ?>
                    <i class="fa fa-long-arrow-right"></i>
                </span>

                <div class="tz_blog_carousel_items owl-carousel owl-theme" data-number-item="<?php echo esc_attr( $number_item ); ?>">

                    <?php
                    if ( $recent_news->have_posts() ) :
                        while ( $recent_news->have_posts() ) :
                            $recent_news->the_post();

                            $tz_nightclub_post_type_image    =   get_post_meta( get_the_ID(),'nightclub_portfolio_image',true );

                            ?>
                            <div class="tz_blog_carousel_item">
                                <div class="tz_blog_carousel_item_bk">

                                    <?php

                                    if ( $tz_nightclub_post_type_image  != '' ) :

                                    ?>

                                        <img src="<?php echo esc_url( $tz_nightclub_post_type_image ); ?>" alt="<?php the_title(); ?>">

                                    <?php else: ?>

                                        <?php the_post_thumbnail( 'medium_large' ); ?>

                                    <?php endif; ?>

                                </div>
                                <div class="tz_blog_carousel_item_box">
                                    <p class="tz_blog_carousel_date">
                                        <?php echo get_the_date(); ?>
                                    </p>
                                    <h3>
                                        <a href="<?php the_permalink() ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            <?php
                        endwhile;
                    endif;
                    wp_reset_postdata();
                    ?>

                </div>
            </div>

            <div class="tz_filters_post">

                <?php

                $args = array(
                    'type'              => 'monthly',
                    'limit'             => '',
                    'format'            => 'custom',
                    'before'            => '<h4 class="tz_monthly_archives">',
                    'after'             => '</h4>',
                    'show_post_count'   => false,
                    'echo'              => 1,
                    'order'             => 'DESC',
                    'post_type'         => 'post'
                );

                ?>

                <span class="tz_next_pre_month tz_next_month">
                    <?php esc_html_e( 'Next Month','nightclub' ); ?>
                </span>
                <span class="tz_next_pre_month tz_pre_month">
                    <?php esc_html_e( 'Previous Month','nightclub' ); ?>
                </span>

                <div class="tz_filters_post_carousel owl-carousel owl-theme">

                    <?php wp_get_archives( $args ); ?>

                </div>


            </div>

        </div>
    </div>

<?php

    $tz_nightclub  =   ob_get_contents();
    ob_end_clean();
    return $tz_nightclub;

}
add_shortcode( 'tz_blog_carousel','tz_nightclub_blog_carousel' );

?>