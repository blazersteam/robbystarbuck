<?php

$tz_nightclub_cat_blog      = array();
$tz_nightclub_categories_b  = get_categories();

if ( isset( $tz_nightclub_categories_b ) && !empty($tz_nightclub_categories_b) ):
    foreach( $tz_nightclub_categories_b as $tz_nightclub_cate_blog ){
        $tz_nightclub_cat_blog[$tz_nightclub_cate_blog->name] = $tz_nightclub_cate_blog->term_id;
    }
endif;

vc_map( array(
    'base'      =>  'tz_blog_carousel',
    'name'      =>  'Blog Carousel',
    'icon'      =>  'tz_vc_icon_blog_carousel',
    'category'  =>  'Night club',
    'params'    =>  array(

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Title',
            'param_name'    =>  'title',
            'value'         =>  'Recent news',
            'group'         =>  'Text',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title',
            'param_name'    =>  'color_title',
            'value'         =>  '',
            'group'         =>  'Text',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Sub Title',
            'param_name'    =>  'sub_title',
            'value'         =>  'From our blog',
            'group'         =>  'Text',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Sub Title',
            'param_name'    =>  'color_sub_title',
            'value'         =>  '',
            'group'         =>  'Text',
        ),

        array(
            'type'          =>  'checkbox',
            'heading'       =>  'Category Blog',
            'param_name'    =>  'category_blog_arr',
            'class'         =>  'tz_slider_blog',
            'value'         =>  $tz_nightclub_cat_blog,
            'description'   =>  'Choose category blog.',
            'group'         =>  'Post',
        ),
        array(
            'type'          =>  'textfield',
            'admin_label' 	=>  true,
            'heading'       =>  'Limit Blog',
            'param_name'    =>  'posts_per_page',
            'value'         =>  3,
            'group'         =>  'Post',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Order By',
            'param_name'    =>  'orderby',
            'value'         =>  array(
                'Date'      =>  'date',
                'ID'        =>  'id',
                'Title'     =>  'title',
            ),
            'group'         =>  'Post',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Order',
            'param_name'    =>  'order',
            'value'         =>  array(
                'Z --> A'   =>  'desc',
                'A --> Z'   =>  'asc',
            ),
            'group'         =>  'Post',
        ),

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Number Item',
            'param_name'    =>  'number_item',
            'value'         =>  4,
            'group'         =>  'Setting Slider',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Pagination',
            'param_name'    =>  'pagination',
            'value'         =>  array(
                'Yes'   =>  1,
                'No'    =>  0,
            ),
            'group'         =>  'Setting Slider',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Navigation',
            'param_name'    =>  'navigation',
            'value'         =>  array(
                'No'    =>  0,
                'Yes'   =>  1,
            ),
            'group'         =>  'Setting Slider',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Auto',
            'param_name'    =>  'auto',
            'value'         =>  array(
                'No'    =>  0,
                'Yes'   =>  1,
            ),
            'group'         =>  'Setting Slider',
        ),

    )
) );