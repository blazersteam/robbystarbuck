<?php

vc_map( array(
    'base'      =>  'tz_title',
    'name'      =>  'Title',
    'icon'      =>  'tz_vc_icon_title',
    'category'  =>  'Night club',
    'params'    =>  array(

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Title',
            'param_name'    =>  'title',
            'admin_label'   =>  true,
            'value'         =>  'Welcome to 1040 Club',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title',
            'param_name'    =>  'color_title',
            'value'         =>  '',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Sub Title',
            'param_name'    =>  'sub_title',
            'value'         =>  'About',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Sub Title',
            'param_name'    =>  'color_sub_title',
            'value'         =>  '',
        ),

    )
) );