<?php
function tz_nightclub_title( $atts ) {

    $title = $color_title = $sub_title = $color_sub_title = '';

    extract(shortcode_atts(array(

        'title'             =>  'Welcome to 1040 Club',
        'color_title'       =>  '',
        'sub_title'         =>  'About',
        'color_sub_title'   =>  '',

    ), $atts));

    ob_start();

?>

    <div class="tz_element_title">
        <h3 class="tz_title" <?php echo( $color_title != '' ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>
            <?php echo esc_attr( $title ); ?>
        </h3>
        <h2 class="tz_sub_title" <?php echo( $color_sub_title != '' ? 'style="color:' . esc_attr( $color_sub_title ) . '"' : '' ); ?>>
            <?php echo esc_attr( $sub_title ); ?>
        </h2>
    </div>

<?php

    $tz_nightclub  =   ob_get_contents();
    ob_end_clean();
    return $tz_nightclub;

}
add_shortcode( 'tz_title','tz_nightclub_title' );

?>