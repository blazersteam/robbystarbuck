<?php

/* custom [row] */
vc_add_param('vc_row', array(
        "type"        =>  "dropdown",
        "class"       =>  "hide_in_vc_editor",
        "admin_label" =>  true,
        "heading"     =>  "Row Type",
        "weight"      =>    1,
        "param_name"  =>  "style_width",
        "value"       => array(
            "Default"               => "default",
            "Vertical Align Middle" => "vertical_middle"
        )
    )
);

vc_add_param('vc_row', array(
    "type"          => "colorpicker",
    "class"         => "hide_in_vc_editor",
    "heading"       => "Background Overlay",
    "param_name"    => "tz_background_overlay",
    'weight'        => 1,
));

vc_add_param('vc_row',array(
    "type"          => "checkbox",
    "class"         => "",
    "heading"       => __("Check this box to adjust padding for responsiveness( use for column )", "js_composer"),
    "param_name"    => "tz_responsiveness_padding",
    "value"         => array( '' => 1)
));
/* End custom [row] */

/* Start vc-slider-images */
class WPBakeryShortCode_Slider_Images  extends WPBakeryShortCodesContainer {}
vc_map( array(
    'name'                      =>  'Slider Images',
    'base'                      =>  'slider_images',
    'as_parent'                 =>  array('only'    =>  'slider_images_item'),
    'content_element'           =>  true,
    'category'                  =>  'Night club',
    'icon'                      =>  'tz_vc_icon_slider_images',
    'js_view'                   =>  'VcColumnView',
    'params'                    =>  array(

        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Auto Slider',
            'param_name'    =>  'auto_slider',
            'value'         =>  array(
                'No'    =>  0,
                'Yes'   =>  1
            ),
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Loop Slider',
            'param_name'    =>  'loop_slider',
            'value'         =>  array(
                'No'    =>  0,
                'Yes'   =>  1
            ),
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  'RTL Slider',
            'param_name'    =>  'rtl_slider',
            'value'         =>  array(
                'No'    =>  0,
                'Yes'   =>  1
            ),
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title Image',
            'param_name'    =>  'color_title_image',
            'value'         =>  '',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Padding Slider',
            'param_name'    =>  'padding_slider',
            'value'         =>  '',
            'description'   =>  'Ex: 15px 0 15px 0 ( top, right, left, bottom )'
        ),

    )
) );

/* Class WPBakeryShortCode_Slider_Images_Item */
class WPBakeryShortCode_Slider_Images_Item  extends WPBakeryShortCode {}
vc_map( array(
    'name'                      =>  'Slider Images Item',
    'base'                      =>  'slider_images_item',
    'icon'                      =>  'tz_vc_icon_slider_images_item',
    'category'                  =>  'Night club',
    'allowed_container_element' =>  'vc_row',
    'as_child'                  =>  array('only'    =>  'slider_images'),
    'params'                    =>  array(

        array(
            'type'          =>  'attach_image',
            'heading'       =>  'Upload Image',
            'param_name'    =>  'image_item',
            'value'         =>  '',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Title Image',
            'param_name'    =>  'title_image',
            'holder'        =>  'div',
            'admin_label'   =>  true,
            'value'         =>  '',
        ),

    )
) );
/* End vc-slider-images */

/* Start vc-services */
class WPBakeryShortCode_Services  extends WPBakeryShortCodesContainer {}
vc_map( array(
    'name'                      =>  'Services',
    'base'                      =>  'services',
    'as_parent'                 =>  array('only'    =>  'services_item'),
    'content_element'           =>  true,
    'category'                  =>  'Night club',
    'icon'                      =>  'tz_vc_icon_services',
    'js_view'                   =>  'VcColumnView',
    'params'                    =>  array(

        array(
            'type'          =>  'dropdown',
            'heading'       =>  'When Hovering, The Frame Width Increases',
            'param_name'    =>  'hover_width_increases',
            'value'         =>  array(
                'Yes'   =>  1,
                'No'    =>  0
            ),
        ),

    )
) );

class WPBakeryShortCode_Services_Item  extends WPBakeryShortCode {}
vc_map( array(
    'name'                      =>  'Services Item',
    'base'                      =>  'services_item',
    'icon'                      =>  'tz_vc_icon_services_item',
    'category'                  =>  'Night club',
    'allowed_container_element' =>  'vc_row',
    'as_child'                  =>  array('only'    =>  'services'),
    'params'                    =>  array(

        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Type Video',
            'param_name'    =>  'type_video',
            'admin_label'   =>  true,
            'value'         =>  array(
                'HTML5'     =>  1,
                'Vimeo'     =>  2,
                'Youtube'   =>  3
            ),
            'group'         =>  'Services',
        ),

        array(
            'type'          =>  'attach_image',
            'heading'       =>  'Background image vimeo or youtube',
            'param_name'    =>  'bk_vimeo_youtube',
            'value'         =>  '',
            'dependency'    => array( 'element' => "type_video", 'value' => array( '2', '3' ) ),
            'group'         =>  'Services'
        ),

        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Type Icon',
            'param_name'    =>  'type_icon',
            'value'         =>  array(
                'Font Awesome'  =>  1,
                'Icon Image'    =>  2,
            ),
            'group'         =>  'Services',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Icon',
            'param_name'    =>  'icon',
            'value'         =>  '',
            'description'   =>  'Ex: fa-music, <a href="http://fontawesome.io/icons/" target="_blank">font icon</a>',
            'dependency'    => Array('element' => "type_icon", 'value' => array('1')),
            'group'         =>  'Services',
        ),
        array(
            'type'          =>  'attach_image',
            'heading'       =>  'Upload Icon Image',
            'param_name'    =>  'image_icon',
            'value'         =>  '',
            'dependency'    => Array('element' => "type_icon", 'value' => array('2')),
            'group'         =>  'Services'
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Title',
            'param_name'    =>  'title',
            'admin_label'   =>  true,
            'value'         =>  '',
            'group'         =>  'Services',
        ),

        array(
            'type'          =>  'vc_link',
            'heading'       =>  'URL (Link) Title',
            'param_name'    =>  'custom_link_title',
            'description'   =>  'Add custom link.',
            'group'         =>  'Services'
        ),

        array(
            'type'          =>  'textarea_html',
            'heading'       =>  'Description',
            'param_name'    =>  'content',
            'value'         =>  '',
            'group'         =>  'Services',
        ),

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Video mp4 url eg: .mp4',
            'admin_label'   =>  true,
            'param_name'    =>  'video_mp4',
            'group'         =>  'Video HTML5',
            'dependency'    => Array('element' => "type_video", 'value' => array('1')),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Video ogv url eg: .ogv ',
            'admin_label'   =>  true,
            'param_name'    =>  'video_ogg',
            'group'         =>  'Video HTML5',
            'dependency'    => Array('element' => "type_video", 'value' => array('1')),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Video web url eg: .webm',
            'admin_label'   =>  true,
            'param_name'    =>  'video_web',
            'group'         =>  'Video HTML5',
            'dependency'    => Array('element' => "type_video", 'value' => array('1')),
        ),

        array(
            'type'          =>  'dropdown',
            'heading'       =>  'Muted Video',
            'param_name'    =>  'muted_video',
            'value'         =>  array(
                'No'    =>  0,
                'Yes'   =>  1,
            ),
            'group'         =>  'Video HTML5',
            'dependency'    => Array('element' => "type_video", 'value' => array('1')),
        ),

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Input id vimeo',
            'param_name'    =>  'id_video_vimeo',
            'group'         =>  'Vimeo',
            "description"   => "Click popup video. Example: Vimeo ID is 79095914 in this link https://vimeo.com/79095914",
            'dependency'    => Array('element' => "type_video", 'value' => array('2')),
        ),

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Input id youtube',
            'param_name'    =>  'id_video_tube',
            'group'         =>  'Youtube',
            "description"   => "Click popup video. Example: Youtube ID is 5fHif0T6aKA in this link https://www.youtube.com/watch?v=5fHif0T6aKA",
            'dependency'    => Array('element' => "type_video", 'value' => array('3')),
        ),

        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Icon',
            'param_name'    =>  'color_icon',
            'value'         =>  '',
            'group'         =>  'Color',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Title',
            'param_name'    =>  'color_title',
            'value'         =>  '',
            'group'         =>  'Color',
        ),

    )
) );
/* End vc-services */

/* Start vc_contact_info_open_hour */

class WPBakeryShortCode_Contact_Info_Open_Hour  extends WPBakeryShortCodesContainer {}
vc_map( array(
    'name'                      =>  'Info - Hour',
    'base'                      =>  'contact_info_open_hour',
    'as_parent'                 =>  array('only'    =>  'contact_info_open_hour_item'),
    'content_element'           =>  true,
    'category'                  =>  'Night club',
    'icon'                      =>  'tz_vc_icon_info_hour',
    'js_view'                   =>  'VcColumnView',
    'params'                    =>  array(

        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Text 1',
            'param_name'    =>  'color_text_1',
            'value'         =>  '',
        ),
        array(
            'type'          =>  'colorpicker',
            'heading'       =>  'Color Text 2',
            'param_name'    =>  'color_text_2',
            'value'         =>  '',
        ),

    )
) );

class WPBakeryShortCode_Contact_Info_Open_Hour_Item  extends WPBakeryShortCode {}
vc_map( array(
    'name'                      =>  'Info - Hour Item',
    'base'                      =>  'contact_info_open_hour_item',
    'icon'                      =>  'tz_vc_icon_info_hour_item',
    'category'                  =>  'Night club',
    'allowed_container_element' =>  'vc_row',
    'as_child'                  =>  array('only'    =>  'contact_info_open_hour'),
    'params'                    =>  array(

        array(
            'type'          =>  'textfield',
            'heading'       =>  'Text 1',
            'param_name'    =>  'text_1',
            'admin_label'   =>  true,
            'value'         =>  '',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  'Text 2',
            'param_name'    =>  'text_2',
            'value'         =>  '',
        ),

    )
) );
/* End vc_contact_info_open_hour */

