<?php

$text_1 = $text_2 = '';

extract( shortcode_atts( array(

    'text_1'    =>  '',
    'text_2'    =>  '',

), $atts ) );

?>
<div class="tz_night_club_info_hour_item">
    <span class="tz_night_club_info_hour_text_1">
        <?php echo esc_attr( $text_1 ); ?>
    </span>
    <span class="tz_night_club_info_hour_text_2">
        <?php echo esc_attr( $text_2 ); ?>
    </span>
</div>
