<?php

$auto_slider = $loop_slider = $rtl_slider = $color_title_image = $padding_slider = '';

extract( shortcode_atts( array(

    'auto_slider'       =>  0,
    'loop_slider'       =>  0,
    'rtl_slider'        =>  0,
    'color_title_image' =>  '',
    'padding_slider'    =>  '',

), $atts ) );


?>
<div class="tz_slider_images" <?php echo( $padding_slider != '' ? 'style="padding:' . esc_attr( $padding_slider ) . '"' : '' ); ?> <?php echo( $color_title_image != '' ? 'data-color="' . esc_attr( $color_title_image ) . '"' : '' ); ?> data-auto="<?php echo esc_attr( $auto_slider ); ?>" data-loop="<?php echo esc_attr( $loop_slider ); ?>" data-rtl="<?php echo esc_attr( $rtl_slider ); ?>">
    <div class="tz_slider_images_items owl-carousel owl-theme">
        <?php echo do_shortcode( $content ); ?>
    </div>
</div>
