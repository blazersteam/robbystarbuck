<?php

$image_item = $title_image = '';

extract( shortcode_atts( array(

    'image_item'    =>  '',
    'title_image'   =>  '',

), $atts ) );

?>
<div class="tz_slider_images_item">
    <div class="tz_slider_images_bk">
        <?php echo wp_get_attachment_image( $image_item,'full' ); ?>
    </div>
    <h3 class="tz_title_image">
        <?php echo balanceTags( $title_image ); ?>
    </h3>
</div>

