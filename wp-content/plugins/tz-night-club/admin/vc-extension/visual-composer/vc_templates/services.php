<?php

$hover_width_increases = '';

extract( shortcode_atts( array(

    'hover_width_increases' =>  1,

), $atts ) );


?>
<div class="tz_night_club_services" data-hover-width="<?php echo esc_attr( $hover_width_increases ); ?>">
    <div class="cf">
        <?php echo do_shortcode( $content ) ?>
    </div>
</div>