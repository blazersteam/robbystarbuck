<?php

$type_video = $type_icon = $icon = $image_icon = $title = $custom_link_title = $video_mp4 = $video_ogg = $video_ogg = $video_web = $bk_vimeo_youtube = $id_video_vimeo = $id_video_tube = '';
$color_icon = $color_title = $muted_video = '';

extract( shortcode_atts( array(

    'type_video'        =>  1,
    'type_icon'         =>  1,
    'icon'              =>  '',
    'image_icon'        =>  '',
    'title'             =>  '',
    'custom_link_title' =>  '',
    'video_mp4'         =>  '',
    'video_ogg'         =>  '',
    'video_web'         =>  '',
    'muted_video'       =>  0,
    'bk_vimeo_youtube'  =>  '',
    'id_video_vimeo'    =>  '',
    'id_video_tube'     =>  '',
    'color_icon'        =>  '',
    'color_title'       =>  '',

), $atts ) );

$vc_custom_link = vc_build_link( $custom_link_title );

if ( $type_video == 2 || $type_video == 3 ) :

    wp_enqueue_script( 'jquery.easy.opener' );
    wp_enqueue_script( 'jquery.fitvids' );

endif;

$content = wpb_js_remove_wpautop($content, true);

$tz_night_club_services = $tz_night_club_services_img = '';

if ( $type_video == 2 || $type_video == 3 ) :
    $tz_night_club_services_img = ' tz_night_club_services_bk_img';
endif;

if ( $type_icon == 2 ) {
    $tz_night_club_services = ' tz_night_club_services_icon_image';
}

?>
<div class="tz_night_club_services_item third inactive<?php echo esc_attr( $tz_night_club_services_img ); ?>" data-type-video="<?php echo esc_attr( $type_video ); ?>" <?php echo ( $bk_vimeo_youtube != '' && ( $type_video == 2 || $type_video == 3 ) ? 'style="background-image: url(' . esc_url( wp_get_attachment_image_url( $bk_vimeo_youtube, 'full' ) ) . ')"' : '' ); ?>>

    <?php if ( $type_video == 1 ) : ?>

        <video class="videoID" loop <?php echo( $muted_video == 1 ? 'muted' : '' ); ?>>
            <source type="video/mp4" src="<?php echo esc_url( $video_mp4 ); ?>" />
            <source type="video/ogg" src="<?php echo esc_url($video_ogg); ?>" />
            <source type="video/webm" src="<?php echo esc_url($video_web); ?>" />
        </video>

        <?php if ( $custom_link_title != '' ) : ?>

            <div class="video_iframe custom-video-iframe">

                <a class="link-title-service" <?php echo ( $vc_custom_link['target'] != '' ? 'target="' . esc_attr( $vc_custom_link['target'] ) . '"' : '') .' '. ( $vc_custom_link['url'] != '' ? 'href="' . esc_attr( $vc_custom_link['url'] ) . '"' : '' ) . ' '. ( $vc_custom_link['title'] != '' ? 'title="' . esc_attr( $vc_custom_link['title'] ) . '"' : '' )  ; ?> <?php echo( $color_title != '' ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>
                    <?php echo balanceTags( $title ); ?>
                </a>


            </div>

        <?php endif; ?>

    <?php else: ?>

        <div class="video_iframe">

            <?php if ( $type_video == 2 ) : ?>

                <a class="easy-opener tz_btn_easy" data-type="video" data-width="500" data-height="281" href="//player.vimeo.com/video/<?php echo esc_attr($id_video_vimeo); ?>">
                    <i class="fa fa-play"></i>
                </a>

            <?php else: ?>

                <a class="easy-opener tz_btn_easy" data-type="video" data-width="500" data-height="281" href="//www.youtube.com/embed/<?php echo esc_attr($id_video_tube);?>">
                    <i class="fa fa-play"></i>
                </a>

            <?php endif; ?>

            <?php if ( $custom_link_title != '' ) : ?>

                <a class="link-title-service" <?php echo ( $vc_custom_link['target'] != '' ? 'target="' . esc_attr( $vc_custom_link['target'] ) . '"' : '') .' '. ( $vc_custom_link['url'] != '' ? 'href="' . esc_attr( $vc_custom_link['url'] ) . '"' : '' ) . ' '. ( $vc_custom_link['title'] != '' ? 'title="' . esc_attr( $vc_custom_link['title'] ) . '"' : '' )  ; ?> <?php echo( $color_title != '' ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>
                    <?php echo balanceTags( $title ); ?>
                </a>

            <?php endif; ?>

        </div>

    <?php endif; ?>

    <div class="tz_night_club_services_content_item">
        <div class="ds-table">
            <div class="ds-table-cell-left-center">
                <div class="tz_night_club_services_content_box">
                    <h4 class="tz_night_club_services_title" <?php echo( $color_title != '' ? 'style="color:' . esc_attr( $color_title ) . '"' : '' ); ?>>

                        <?php echo balanceTags( $title ); ?>

                        <span class="tz_night_club_services_icon<?php echo esc_attr( $tz_night_club_services ); ?>">

                            <?php if ( $type_icon == 1 ) : ?>

                                <i class="fa <?php echo esc_attr( $icon ) ?>" <?php echo( $color_icon != '' ? 'style="color:' . esc_attr( $color_icon ) . '"' : '' ); ?>></i>

                            <?php

                            else:

                                echo wp_get_attachment_image($image_icon, 'full');

                            endif;

                            ?>

                        </span>

                    </h4>
                    <div class="tz_night_club_services_content">
                        <?php echo balanceTags( $content ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>