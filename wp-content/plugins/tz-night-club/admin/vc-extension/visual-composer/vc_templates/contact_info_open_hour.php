<?php

$color_text_1 = $color_text_2 = '';

extract( shortcode_atts( array(

    'color_text_1'      =>      '',
    'color_text_2'      =>      '',

), $atts ) );


?>
<div class="tz_night_club_info_hour" <?php echo( $color_text_1 != '' ? 'data-color-text-1="' . esc_attr( $color_text_1 ) .' "' : '' ); ?> <?php echo( $color_text_2 != '' ? 'data-color-text-2="' . esc_attr( $color_text_2 ) .' "' : '' ); ?>>
        <?php echo do_shortcode( $content ); ?>
</div>
