<?php

/*=====================================
 * Visual Composer
 =====================================*/

if ( class_exists('WPBakeryVisualComposerAbstract') ):
    function tzplazart_includevisual(){
        $dir_vc = dirname( __FILE__ );

        // VC Templates
        $vc_templates_dir = $dir_vc . '/visual-composer/vc_templates/';
        $vc_templates_dir_theme = $dir_vc . '/visual-composer/vc_theme/';
        vc_set_shortcodes_templates_dir($vc_templates_dir);

        require_once $vc_templates_dir_theme . 'menu/menu.php';
        require_once $vc_templates_dir_theme . 'menu/vc-menu.php';

        require_once $vc_templates_dir_theme . 'slider-blog/slider-blog.php';
        require_once $vc_templates_dir_theme . 'slider-blog/vc-slider-blog.php';

        require_once $vc_templates_dir_theme . 'book-table/book-table.php';
        require_once $vc_templates_dir_theme . 'book-table/vc-book-table.php';

        require_once $vc_templates_dir_theme . 'title/title.php';
        require_once $vc_templates_dir_theme . 'title/vc-title.php';

        require_once $vc_templates_dir_theme . 'event/event.php';
        require_once $vc_templates_dir_theme . 'event/vc-event.php';

        require_once $vc_templates_dir_theme . 'gallery/gallery.php';
        require_once $vc_templates_dir_theme . 'gallery/vc-gallery.php';

        require_once $vc_templates_dir_theme . 'video-blog-slider/video-blog-slider.php';
        require_once $vc_templates_dir_theme . 'video-blog-slider/vc-video-blog-slider.php';

        require_once $vc_templates_dir_theme . 'blog-carousel/blog-carousel.php';
        require_once $vc_templates_dir_theme . 'blog-carousel/vc-blog-carousel.php';

        require_once $vc_templates_dir_theme . 'coming-soon/coming-soon.php';
        require_once $vc_templates_dir_theme . 'coming-soon/vc-coming-soon.php';

        require_once $dir_vc . '/visual-composer/extend-composer.php';

    }

    add_action('init', 'tzplazart_includevisual', 20);
endif;

?>
