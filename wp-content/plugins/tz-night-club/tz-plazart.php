<?php
/**
 * @package Plazart
 */
/*
Plugin Name: Tz Night Club
Plugin URI: http://templaza.com/
Description: This is plugin for Templaza. This plugin allows you to create post types, taxonomies and Visual Composer's shortcodes
Version: 1.1.3
Author: Templaza
Author URI: http://template.com/
License: GPLv2 or later
*/


/**
 * This is the TZPlazart loader class.
 *
 * @package   TZPlazart
 * @author    templaza (http:://templaza.com)
 * @copyright Copyright (c) 2014, Templaza
 */

if ( !class_exists('TZ_Plazart') ):

    class TZ_Plazart{

        /*
         * This method loads other methods of the class.
         */
         function __construct(){
            /* load languages */
            $this -> load_languages();

            /*load all plazart*/
            $this -> load_plazart();

            /*load all script*/
            $this -> load_script();
        }

        /*
         * Load the languages before everything else.
         */
         function load_languages(){
            add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );
        }

        /*
         * Load the text domain.
         */
         function load_textdomain(){

            load_plugin_textdomain( tz_plazart, false, plugin_dir_url( __FILE__ ) . '/languages' );
        }

        /*
         * Load script
         */
         function load_script() {

            if( is_admin() ) :
                add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
            else :
                add_action( 'wp_enqueue_scripts', array( $this, 'front_end_scripts' ) );
            endif;

        }

        /*
         * Load TZPlazart on the 'after_setup_theme' action. Then filters will
         */
         function load_plazart(){

            $this -> constants();

            $this -> admin_includes();


        }

        /**
         * Constants
         */
         function constants() {

            define('tz_plazart', 'tz-plazart');

            define('PLUGIN_PREFIX', 'plazart');

            define('PLUGIN_PATH', plugin_dir_url( __FILE__ ));

            define('PLUGIN_SERVER_PATH',dirname( __FILE__ ) );
        }

        /*
         * Require file
         */
         function admin_includes(){
            require_once PLUGIN_SERVER_PATH.'/admin/admin-init.php';
        }

        /*
        * Require file
        */
        function  admin_scripts(){
            global $pagenow;
            if ('post-new.php' == $pagenow || 'post.php' == $pagenow) :
                wp_enqueue_style('thickbox');
                wp_enqueue_script('media-upload');
                wp_enqueue_script('thickbox');

                // load css
                wp_enqueue_style('fancybox', PLUGIN_PATH. 'assets/css/fancybox.css');
                wp_enqueue_style('shortocde_admin', PLUGIN_PATH. 'assets/css/shortocde_admin.css');

                // load js
                wp_register_script('jquery.fancybox_js', PLUGIN_PATH .'assets/js/jquery.fancybox.pack.js', false, false, $in_footer=true);
                wp_enqueue_script('jquery.fancybox_js')  ;
            endif;
        }

        /*
        * Require front_end_scripts
        */
        function front_end_scripts() {

            global $post;
            $plugin_photo = $post->post_content;

            /* Get Css */
            if ( has_shortcode( $plugin_photo,'tz_slider_blog' ) ) :

                wp_enqueue_style( 'superslides', PLUGIN_PATH . 'assets/css/superslides.css', array(), '' );

            endif;

            if ( has_shortcode( $plugin_photo, 'slider_images' ) || has_shortcode( $plugin_photo, 'tz_gallery' ) || has_shortcode( $plugin_photo, 'tz_blog_carousel' ) ) :

                wp_enqueue_style( 'owl-theme-default', PLUGIN_PATH . 'assets/css/owl.theme.default.min.css', array(), '2.1.0' );
                wp_enqueue_style( 'owl-carousel', PLUGIN_PATH . 'assets/css/owl.carousel.min.css', array(), '2.1.0' );

            endif;

            if ( has_shortcode( $plugin_photo, 'services' ) ) :

                wp_enqueue_style( 'easy-opener', PLUGIN_PATH . 'assets/css/easy-opener.css', array(), '' );

            endif;

            if ( has_shortcode( $plugin_photo, 'tz_gallery' ) ) :

                wp_enqueue_style( 'fancybox', PLUGIN_PATH . 'assets/css/fancybox.css', array(), '2.1.5' );

            endif;


            /* Get JS */
            if ( has_shortcode( $plugin_photo,'tz_slider_blog' ) ) :

                wp_enqueue_script( 'superslides', PLUGIN_PATH . 'assets/js/jquery.superslides.min.js', array(), '0.6.3', true );

            endif;

            if ( has_shortcode( $plugin_photo, 'tz_book_table' ) ) :

                wp_enqueue_script( 'datepicker', PLUGIN_PATH . 'assets/js/datepicker.js', array(), '', true );

            endif;

            if ( has_shortcode( $plugin_photo, 'slider_images' ) || has_shortcode( $plugin_photo, 'tz_gallery' ) || has_shortcode( $plugin_photo, 'tz_blog_carousel' ) ) :

                wp_enqueue_script( 'owl-carousel', PLUGIN_PATH . 'assets/js/owl.carousel.min.js', array(), '2.1.0', true );

            endif;

            if ( has_shortcode( $plugin_photo, 'services' ) ) :

                wp_register_script( 'jquery.easy.opener', PLUGIN_PATH . 'assets/js/jquery.easy-opener.min.js', array(), '', true );

                wp_register_script( 'jquery.fitvids', PLUGIN_PATH . 'assets/js/jquery.fitvids.min.js', array(), '', true );

            endif;

            if ( has_shortcode( $plugin_photo, 'tz_event' ) ) :

                wp_enqueue_script( 'tinyscrollbar', PLUGIN_PATH . 'assets/js/jquery.tinyscrollbar.min.js', array(), '2.4.2', true );

            endif;

            if ( has_shortcode( $plugin_photo, 'tz_video_blog_slider' ) ) :

                wp_enqueue_script( 'video', PLUGIN_PATH . 'assets/js/video.js', array(), '', true );
                wp_enqueue_script( 'bigvideo', PLUGIN_PATH . 'assets/js/bigvideo.js', array(), '', true );

            endif;

            if ( has_shortcode( $plugin_photo, 'tz_gallery' ) ) :

                wp_enqueue_script( 'nightclub-resize', PLUGIN_PATH . 'assets/js/resize.js', array(), '', true );

                wp_enqueue_script( 'fancybox', PLUGIN_PATH . 'assets/js/jquery.fancybox.pack.js', array(), '2.1.5', true );

            endif;

            if ( has_shortcode( $plugin_photo, 'tz_blog_carousel' ) ) :

                /* Blog-month-js */
                wp_enqueue_script( 'tz-1040nightclub-blog-month', PLUGIN_PATH . 'assets/js/blog-month.js', array(), '', true );

                $tz_1040nightclub_admin_url         = admin_url('admin-ajax.php');
                $tz_1040nightclub_arg_blog_month    = array( 'url' => $tz_1040nightclub_admin_url );
                wp_localize_script('tz-1040nightclub-blog-month', 'tz_1040nightclub_blog_month_var', $tz_1040nightclub_arg_blog_month);
                /* End Blog-month-js */

            endif;

            if ( has_shortcode( $plugin_photo,'tz_slider_blog' ) || has_shortcode( $plugin_photo, 'tz_book_table' ) || has_shortcode( $plugin_photo, 'slider_images' ) || has_shortcode( $plugin_photo, 'services' ) || has_shortcode( $plugin_photo,'tz_event' ) || has_shortcode( $plugin_photo, 'tz_video_blog_slider' ) || has_shortcode( $plugin_photo, 'tz_gallery' ) || has_shortcode( $plugin_photo, 'contact_info_open_hour' ) ) :

                wp_enqueue_script( 'element-shortcode', PLUGIN_PATH . 'assets/js/element-shortcode.js', array(), '1.1.0', true );

            endif;

        }

    }

    $oj_plazart = new TZ_Plazart();

endif;

?>