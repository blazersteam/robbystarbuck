<?php

function tz_1040nightclub_setup(){
    /**
     * Text domain
     */
    load_theme_textdomain('1040nightclub', get_template_directory() . '/languages');


    /**
     * plazarttheme setup.
     *
     * Set up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support post thumbnails.
     *
     */
    //Enable support for Header (tz-demo)
    add_theme_support( 'custom-header' );

    //Enable support for Background (tz-demo)
    add_theme_support( 'custom-background' );

    //Enable support for Post Thumbnails
    add_theme_support('post-thumbnails');

    // Add RSS feed links to <head> for posts and comments.
    add_theme_support( 'automatic-feed-links' );

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menu('primary','Primary Menu');

    // add theme support title-tag
    add_theme_support( 'title-tag' );

    /*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
    add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', tz_1040nightclub_fonts_url() ) );



}
add_action( 'after_setup_theme', 'tz_1040nightclub_setup' );


/**
 * Required: include plugin theme sidebars
 */
require get_template_directory() . '/extension/theme-functions.php';

/*
 * Required: include plugin theme scripts
 */
require get_template_directory() . '/extension/tz-process-option.php';


if ( class_exists('OT_Loader') ):

    /*
     * Required: Theme option
     */
    require get_template_directory() . '/extension/ot-support/theme-options.php';

    /*
     * Required: Metabox
     */
    require get_template_directory() . '/extension/ot-support/add-meta-boxes.php';
endif;


/*
 *  method add global javascript variable THEME_PREFIX to admin_head
 */
function tz_1040nightclub_addto_header() {
    ?>
    <script type="text/javascript">
        var themeprefix = '<?php echo esc_js('tz_1040nightclub_theme') ?>';
    </script>
<?php
}
add_action('admin_head', 'tz_1040nightclub_addto_header');
add_action('wp_head', 'tz_1040nightclub_addto_header');


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
    $content_width = 900;


/**
 * Show full editor
 */
function tz_1040nightclub_ilc_mce_buttons( $buttons ){
    array_push($buttons,
        "backcolor",
        "anchor",
        "hr",
        "sub",
        "sup",
        "fontselect",
        "fontsizeselect",
        "styleselect",
        "cleanup"
    );
    return $buttons;
}
add_filter("mce_buttons_2", "tz_1040nightclub_ilc_mce_buttons");

/*
 * Adds JavaScript to pages with the comment form to support
 * sites with threaded comments (when in use).
 */
if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
    wp_enqueue_script( 'comment-reply' );

if ( ! function_exists( 'tz_1040nightclub_paging_nav' ) ) {

    function tz_1040nightclub_paging_nav() {
        global $wp_query, $wp_rewrite;
        // Don't print empty markup if there's only one page.
        if ( $wp_query->max_num_pages < 2 ) {
            return;
        }

        $tz_1040nightclub_paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
        $tz_1040nightclub_pagenum_link = html_entity_decode( get_pagenum_link() );
        $tz_1040nightclub_query_args   = array();
        $tz_1040nightclub_url_parts    = explode( '?', $tz_1040nightclub_pagenum_link );

        if ( isset( $tz_1040nightclub_url_parts[1] ) ) {
            wp_parse_str( $tz_1040nightclub_url_parts[1], $tz_1040nightclub_query_args );
        }

        $tz_1040nightclub_pagenum_link = remove_query_arg( array_keys( $tz_1040nightclub_query_args ), $tz_1040nightclub_pagenum_link );
        $tz_1040nightclub_pagenum_link = trailingslashit( $tz_1040nightclub_pagenum_link ) . '%_%';

        $tz_1040nightclub_format  = $wp_rewrite->using_index_permalinks() && ! strpos( $tz_1040nightclub_pagenum_link, 'index.php' ) ? 'index.php/' : '';
        $tz_1040nightclub_format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';
        // Set up paginated links.
        $tz_1040nightclub_links = paginate_links( array(
            'base'     => $tz_1040nightclub_pagenum_link,
            'format'   => $tz_1040nightclub_format,
            'total'    => $wp_query->max_num_pages,
            'current'  => $tz_1040nightclub_paged,
            'mid_size' => 1,
            'add_args' => array_map( 'urlencode', $tz_1040nightclub_query_args ),
            'prev_text' => esc_html__( 'Previous', '1040nightclub' ),
            'next_text' => esc_html__( 'Next', '1040nightclub' ),
        ) );

        if ( $tz_1040nightclub_links ) :

            ?>
            <nav class="navigation paging-navigation" role="navigation">
                <div class="tzpagination2 loop-pagination">
                    <?php echo balanceTags($tz_1040nightclub_links); ?>
                </div><!-- .pagination -->
            </nav><!-- .navigation -->
        <?php
        endif;
    }

}

if ( ! function_exists( 'tz_1040nightclub_custom_paging_nav' ) ) {
    function tz_1040nightclub_custom_paging_nav($tz_1040nightclub_query_total) {
        global $wp_query, $wp_rewrite;
        // Don't print empty markup if there's only one page.
        if ( $tz_1040nightclub_query_total < 2 ) {
            return;
        }

        $tz_1040nightclub_paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
        $tz_1040nightclub_pagenum_link = html_entity_decode( get_pagenum_link() );
        $tz_1040nightclub_query_args   = array();
        $tz_1040nightclub_url_parts    = explode( '?', $tz_1040nightclub_pagenum_link );

        if ( isset( $tz_1040nightclub_url_parts[1] ) ) {
            wp_parse_str( $tz_1040nightclub_url_parts[1], $tz_1040nightclub_query_args );
        }

        $tz_1040nightclub_pagenum_link = remove_query_arg( array_keys( $tz_1040nightclub_query_args ), $tz_1040nightclub_pagenum_link );
        $tz_1040nightclub_pagenum_link = trailingslashit( $tz_1040nightclub_pagenum_link ) . '%_%';

        $tz_1040nightclub_format  = $wp_rewrite->using_index_permalinks() && ! strpos( $tz_1040nightclub_pagenum_link, 'index.php' ) ? 'index.php/' : '';
        $tz_1040nightclub_format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';
        // Set up paginated links.
        $tz_1040nightclub_links = paginate_links( array(
            'base'     => $tz_1040nightclub_pagenum_link,
            'format'   => $tz_1040nightclub_format,
            'total'    => $tz_1040nightclub_query_total,
            'current'  => $tz_1040nightclub_paged,
            'mid_size' => 1,
            'add_args' => array_map( 'urlencode', $tz_1040nightclub_query_args ),
            'prev_text' => esc_html__( 'Previous', '1040nightclub' ),
            'next_text' => esc_html__( 'Next', '1040nightclub' ),
        ) );

        if ( $tz_1040nightclub_links ) :

            ?>
            <nav class="navigation paging-navigation" role="navigation">
                <div class="tzpagination2 loop-pagination">
                    <?php echo balanceTags($tz_1040nightclub_links); ?>
                </div><!-- .pagination -->
            </nav><!-- .navigation -->
        <?php
        endif;
    }
}

/*
 * Method add ot_get_option
 */

if(!is_admin()):

    if ( ! function_exists( 'ot_get_option' ) ) {
        function ot_get_option( $tz_1040nightclub_option_id, $tz_1040nightclub_default = '' ) {
            /* get the saved options */
            $tz_1040nightclub_options = get_option( 'option_tree' );
            /* look for the saved value */
            if ( isset( $tz_1040nightclub_options[$tz_1040nightclub_option_id] ) && '' != $tz_1040nightclub_options[$tz_1040nightclub_option_id] ) {
                return $tz_1040nightclub_options[$tz_1040nightclub_option_id];
            }
            return $tz_1040nightclub_default;
        }
    }

endif;

/*
 * ADD GOOGLE FONT
 */
if ( ! function_exists( 'tz_1040nightclub_fonts_url' ) ) :
    
    function tz_1040nightclub_fonts_url() {
        $fonts_url = '';
        $fonts     = array();
        $subsets   = 'latin,latin-ext';

        /*
         * Translators: If there are characters in your language that are not supported
         * by Noto Sans, translate this to 'off'. Do not translate into your own language.
         */
        if ( 'off' !== _x( 'on', 'Catamaran font: on or off', '1040nightclub' ) ) {
            $fonts[] = 'Catamaran:600,500,400,300,200,100';
        }

        /*
         * Translators: To add an additional character subset specific to your language,
         * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
         */
        $subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', '1040nightclub' );

        if ( 'cyrillic' == $subset ) {
            $subsets .= ',cyrillic,cyrillic-ext';
        } elseif ( 'greek' == $subset ) {
            $subsets .= ',greek,greek-ext';
        } elseif ( 'devanagari' == $subset ) {
            $subsets .= ',devanagari';
        } elseif ( 'vietnamese' == $subset ) {
            $subsets .= ',vietnamese';
        }

        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), 'https://fonts.googleapis.com/css' );
        }

        return $fonts_url;
    }

endif;

//////////////////////////////////////////////////////////////////
// TWITTER AMPERSAND ENTITY DECODE
//////////////////////////////////////////////////////////////////
function tz_1040nightclub_social_title( $tz_1040nightclub_title ) {
    $tz_1040nightclub_title = html_entity_decode( $tz_1040nightclub_title );
    $tz_1040nightclub_title = urlencode( $tz_1040nightclub_title );
    return $tz_1040nightclub_title;
}

/* ---------------------------------------------------------------------------
 * SSL | Compatibility
 * --------------------------------------------------------------------------- */
if( ! function_exists( 'tz_1040nightclub_ssl' ) ) {
    function tz_1040nightclub_ssl( $echo = false ){
        $tz_1040nightclub_ssl = '';
        if( is_ssl() ) $tz_1040nightclub_ssl = 's';
        if( $echo ){
            echo $tz_1040nightclub_ssl;
        }
        return $tz_1040nightclub_ssl;
    }
}

/* ---------------------------------------------------------------------------
 * Fonts | Selected in Theme Options
 * --------------------------------------------------------------------------- */
if( ! function_exists( 'tz_1040nightclub_fonts_selected' ) ) {
    function tz_1040nightclub_fonts_selected(){
        $tz_1040nightclub_fonts = array();

        $tz_1040nightclub_fonts['content'] 		    = ot_get_option( 'nightclub_font_content', 'Raleway' );
        $tz_1040nightclub_fonts['menu'] 	        = ot_get_option( 'nightclub_font_menu', 'Raleway' );
        $tz_1040nightclub_fonts['headings']         = ot_get_option( 'nightclub_font_headings','Raleway' );
        $tz_1040nightclub_fonts['headingsSmall']    = ot_get_option( 'nightclub_font_headings_small', 'Raleway' );

        return $tz_1040nightclub_fonts;
    }
}
/* End select google font */

add_filter( 'get_the_archive_title', function ($tz_1040nightclub_title) {

    if ( is_category() ) {

        $tz_1040nightclub_title = single_cat_title( '', false );

    } elseif ( is_tag() ) {

        $tz_1040nightclub_title = single_tag_title( '', false );

    } elseif ( is_author() ) {

        $tz_1040nightclub_title = '<span class="vcard">' . get_the_author() . '</span>' ;

    }

    return $tz_1040nightclub_title;

});

/*method activie plugin*/
require get_template_directory() . '/plugins/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'tz_1040nightclub_register_required_plugins' );
function tz_1040nightclub_register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $tz_1040nightclub_plugins = array(

        // This is an example of how to include a plugin pre-packaged with a theme
        array(
            'name'     				=> 'Visual Composer', // The plugin name
            'slug'     				=> 'js_composer', // The plugin slug (typically the folder name)
            'source'   				=> get_template_directory_uri() . '/plugins/js_composer.zip', // The plugin source
            'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
            'version' 				=> '5.6', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
        ),
		array(
            'name'     				=> 'Options Tree', // The plugin name
            'slug'     				=> 'option-tree', // The plugin slug (typically the folder name)
            'source'   				=> get_stylesheet_directory() . '/plugins/option-tree.zip', // The plugin source
            'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
            'version' 				=> '2.6.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
        ),
		array(
            'name'     				=> 'Music Press', // The plugin name
            'slug'     				=> 'music-press', // The plugin slug (typically the folder name)
            'source'   				=> get_stylesheet_directory() . '/plugins/music-press.zip', // The plugin source
            'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
            'version' 				=> '1.0.2', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name'     				=> 'Tz Night Club', // The plugin name
            'slug'     				=> 'tz-night-club', // The plugin slug (typically the folder name)
            'source'   				=> get_template_directory_uri() . '/plugins/tz-night-club.zip', // The plugin source
            'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
            'version' 				=> '1.1.3', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
        ),

        // This is an example of how to include a plugin from the WordPress Plugin Repository        
        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => true,
        ),
        array(
            'name'      => 'NewsLetter',
            'slug'      => 'newsletter',
            'required'  => true,
        ),
        array(
            'name'      => 'The Events Calendar',
            'slug'      => 'the-events-calendar',
            'required'  => true,
        ),
        array(
            'name'      => 'Event Tickets',
            'slug'      => 'event-tickets',
            'required'  => true,
        ),
        array(
            'name'      => 'Music Press',
            'slug'      => 'music-press',
            'required'  => true,
        ),
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $tz_1040nightclub_config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php',            // Parent menu slug.
        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
    );

    tgmpa( $tz_1040nightclub_plugins, $tz_1040nightclub_config );

}

/* ajax blog month */
add_action('wp_ajax_tz_1040nightclub_blog_month','tz_1040nightclub_blog_month');
add_action('wp_ajax_nopriv_tz_1040nightclub_blog_month','tz_1040nightclub_blog_month');

function tz_1040nightclub_blog_month() {

    $tz_1040nightclub_limit_post    =   $_POST['limit'];
    $tz_1040nightclub_order_post    =   $_POST['order'];
    $tz_1040nightclub_date_post     =   $_POST['date'];

    $tz_1040nightclub_timestamp = strtotime( $tz_1040nightclub_date_post );
    $tz_1040nightclub_month  =   date( 'm', $tz_1040nightclub_timestamp );
    $tz_1040nightclub_year   =   date( 'Y', $tz_1040nightclub_timestamp );



    $tz_1040nightclub_args = array(
        'post_type'         =>  'post',
        'posts_per_page'    =>  $tz_1040nightclub_limit_post,
        'order'             =>  $tz_1040nightclub_order_post,
        'year'              =>  $tz_1040nightclub_year,
        'monthnum'          =>  $tz_1040nightclub_month,
    );

    $tz_1040nightclub_recent_news    =   new WP_Query( $tz_1040nightclub_args );


        if ( $tz_1040nightclub_recent_news->have_posts() ) :
            while ( $tz_1040nightclub_recent_news->have_posts() ) :
                $tz_1040nightclub_recent_news->the_post();

                $tz_1040nightclub_post_type_image    =   get_post_meta( get_the_ID(),'nightclub_portfolio_image',true );

    ?>
                <div class="tz_blog_carousel_item">
                    <div class="tz_blog_carousel_item_bk">
                        <?php

                        if ( $tz_1040nightclub_post_type_image  != '' ) :

                        ?>

                            <img src="<?php echo esc_url( $tz_1040nightclub_post_type_image ); ?>" alt="<?php the_title(); ?>">

                        <?php else: ?>

                            <?php the_post_thumbnail( 'medium_large' ); ?>

                        <?php endif; ?>
                    </div>
                    <div class="tz_blog_carousel_item_box">
                        <p class="tz_blog_carousel_date">
                            <?php echo get_the_date(); ?>
                        </p>
                        <h3>
                            <a href="<?php the_permalink() ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>
                    </div>
                </div>
    <?php
            endwhile;
        endif;
        wp_reset_postdata();

}
/* End ajax blog month */

/*  Theme Scripts    */
add_action('init', 'tz_1040nightclub_register_theme_scripts');
function tz_1040nightclub_register_theme_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php') {

        if (is_admin()) {
            add_action('admin_enqueue_scripts', 'tz_1040nightclub_register_back_end_scripts');
        }else{
            add_action('wp_enqueue_scripts', 'tz_1040nightclub_register_front_end_styles');
            add_action('wp_enqueue_scripts', 'tz_1040nightclub_register_front_end_scripts');
        }
    }
}

//Register Back-End script
function tz_1040nightclub_register_back_end_scripts(){

    wp_enqueue_style('tz-1040nightclub-admin-styles', get_template_directory_uri() . '/extension/assets/css/admin-styles.css');
    wp_enqueue_style('tz-1040nightclub-option', get_template_directory_uri() . '/extension/assets/css/tz-theme-options.css');


    wp_register_script('tz-1040nightclub-portfolio-meta-boxes', get_template_directory_uri() . '/extension/assets/js/portfolio-meta-boxes.js', array(), false, $in_footer=true );
    wp_enqueue_script('tz-1040nightclub-portfolio-meta-boxes');

    wp_register_script('tz-1040nightclub-portfolio-theme-option', get_template_directory_uri() . '/extension/assets/js/portfolio-theme-option.js', array(), false, $in_footer=true );
    wp_enqueue_script('tz-1040nightclub-portfolio-theme-option');
}

//Register Front-End Styles
function tz_1040nightclub_register_front_end_styles() {
    /* bootstrap */

    if( is_child_theme() == false ){

        wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', false );

    }

    /* font-awesome */
    wp_enqueue_style('font-awesome', get_template_directory_uri().'/css/font-awesome.min.css', false );

    /* font-linea-arrows */
    wp_enqueue_style('linea-arrows', get_template_directory_uri().'/css/linea_arrows.css', false );
    wp_enqueue_style('linea-basic', get_template_directory_uri().'/css/linea_basic.css', false );
    wp_enqueue_style('linea-music', get_template_directory_uri().'/css/linea_music.css', false );
    wp_enqueue_style('linear-icons', get_template_directory_uri().'/css/linearicons.css', false );

    /* font-google */
    wp_enqueue_style( 'tz-1040nightclub-fonts', tz_1040nightclub_fonts_url(), array(), null );

    /* Start Google Fonts */
    $tz_1040nightclub_on_off_google_font   =  ot_get_option( 'nightclub_select_font_theme', 1 ) ;
    $tz_1040nightclub_fonts                =   tz_1040nightclub_fonts_selected();

    // style & weight
    if( $tz_1040nightclub_weight = ot_get_option('nightclub_font_weight','') ){

        $tz_1040nightclub_weight = ':'. implode( ',', $tz_1040nightclub_weight );

    }

    // subset
    $tz_1040nightclub_subset   =   ot_get_option( 'nightclub_font_subset','' );
    if ( $tz_1040nightclub_subset !='' ) {

        $tz_1040nightclub_subset = '&amp;subset='. str_replace(' ', '', $tz_1040nightclub_subset);
    }

    if ( $tz_1040nightclub_on_off_google_font != 1 ) :

        foreach( $tz_1040nightclub_fonts as $font ){

            $tz_1040nightclub_font_slug = str_replace(' ', '+', $font);
            wp_enqueue_style( $tz_1040nightclub_font_slug, 'http'. tz_1040nightclub_ssl() .'://fonts.googleapis.com/css?family='. $tz_1040nightclub_font_slug . $tz_1040nightclub_weight . $tz_1040nightclub_subset );

        }

    endif;
    /* End Google Fonts */

    /* multi-level-menu */
    wp_enqueue_style('tz-1040nightclub-component', get_template_directory_uri().'/css/component.css', false );

    /* style css */
    if( is_child_theme() == false ){

        wp_enqueue_style('tz-1040nightclub-style', get_template_directory_uri() . '/style.css', false );

    }

    wp_enqueue_style( 'tz-1040nightclub-custom-style', get_stylesheet_uri(), false );

}

//Register Front-End Scripts
function tz_1040nightclub_register_front_end_scripts() {

    wp_enqueue_script( 'tz-1040nightclub-html5', get_template_directory_uri().'/js/lib/html5.js', array('jquery'), false, $in_footer=false );
    wp_script_add_data( 'tz-1040nightclub-html5', 'conditional', 'lt IE 9' );

    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri().'/js/lib/bootstrap.min.js', array('jquery'), false, $in_footer=true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    /* multi-level-menu */
    wp_enqueue_script( 'tz-1040nightclub-modernizr-custom', get_template_directory_uri().'/js/lib/menu/modernizr-custom.js', array('jquery'), false, $in_footer=true );
    wp_enqueue_script( 'tz-1040nightclub-classie', get_template_directory_uri().'/js/lib/menu/classie.js', array('jquery'), false, $in_footer=true );
    wp_enqueue_script( 'tz-1040nightclub-dummydata', get_template_directory_uri().'/js/lib/menu/dummydata.js', array('jquery'), false, $in_footer=true );
    wp_enqueue_script( 'tz-1040nightclub-main', get_template_directory_uri().'/js/lib/menu/main.js', array('jquery'), false, $in_footer=true );
    wp_enqueue_script( 'tz-1040nightclub-multi-level-menu', get_template_directory_uri().'/js/lib/menu/multi-level-menu.js', array('jquery'), false, $in_footer=true );
    /* End multi-level-menu */

    if ( is_page_template('template-comingsoon.php') ) :

        wp_register_script('jquery-countdown', get_template_directory_uri() . '/js/lib/jquery.countdown.min.js', false, false, $in_footer=true);
        wp_enqueue_script('jquery-countdown');

    endif;

    wp_enqueue_script( 'tz-1040nightclub-custom', get_template_directory_uri().'/js/custom.js', array('jquery'), false, $in_footer=true );

}

if( is_admin() ){
    /* theme info page - displays information for support inquiries */
    include_once( get_template_directory() . '/admin/themeinfo/index.php' );

    /* theme demo importer */
    include_once( get_template_directory() . '/admin/tz-importer.php' );
}

/**
 * Filter the upload size limit for non-administrators.
 *
 * @param string $size Upload size limit (in bytes).
 * @return int (maybe) Filtered size limit.
 */
function filter_site_upload_size_limit( $size ) {
    // Set the upload size limit to 60 MB for users lacking the 'manage_options' capability.
    if ( ! current_user_can( 'manage_options' ) ) {
        // 160 MB.
        $size = 160 * 1024 * 1024;
    }
    return $size;
}
add_filter( 'upload_size_limit', 'filter_site_upload_size_limit', 160 );


@ini_set( 'upload_max_size' , '512M' );
@ini_set( 'post_max_size', '512M');
@ini_set( 'max_execution_time', '600' );


?>
