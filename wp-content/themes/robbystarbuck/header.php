<?php
/*
 * The Header for our theme.
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php bloginfo('description'); ?>" />

    <?php wp_head(); ?>
</head>
<body id="bd" <?php body_class(); ?>>

<?php

$tz_1040nightclub_show_loading = ot_get_option( 'nightclub_TzGlobalOptionLoading', 0 );
if( isset($tz_1040nightclub_show_loading) && $tz_1040nightclub_show_loading == 1 ):

?>
    <div id="tzloadding">
        <div id="tzloadding_box">
            <div class="ds-table">
                <div class="ds-table-cell">
                    <?php

                    $tz_1040nightclub_loading = ot_get_option('nightclub_TzGlobalOptionUploadLoading');

                    if( isset($tz_1040nightclub_loading) && !empty($tz_1040nightclub_loading) ):

                        ?>

                        <img class="loadding_img" src="<?php echo esc_url($tz_1040nightclub_loading); ?>" alt="<?php esc_html_e('loading...','1040nightclub') ?>">

                    <?php else: ?>

                        <img class="loadding_img" src="<?php echo esc_url(get_template_directory_uri().'/images/loadding.GIF'); ?>" alt="<?php esc_html_e('loading...','1040nightclub') ?>">

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
