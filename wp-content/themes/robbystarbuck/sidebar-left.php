
<div class="tz_nightclub_sidebar">

    <?php

        if ( is_active_sidebar( 'nightclub-sidebar-main-2' ) ) :

            dynamic_sidebar('nightclub-sidebar-main-2');

        endif;

    ?>

</div>
