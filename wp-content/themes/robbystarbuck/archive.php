<?php

get_header();

//  Blog options show hide

$tz_1040nightclub_blog_sidebar      =   ot_get_option( 'nightclub_TZBlogSidebar',1 );

$tz_night_club_column   =   '';

if ( $tz_1040nightclub_blog_sidebar == 3 ) :

    $tz_night_club_column = 'col-lg-6 col-md-6';

elseif ( $tz_1040nightclub_blog_sidebar == 1 || $tz_1040nightclub_blog_sidebar == 2 ):

    $tz_night_club_column = 'col-lg-9 col-md-9';

else:

    $tz_night_club_column = 'col-lg-12 col-md-12';

endif;

get_template_part('template_inc/inc','menu');

?>

<section class="home-post tz_cat_padding">
    <div class="row">

        <?php if ( $tz_1040nightclub_blog_sidebar == 2 || $tz_1040nightclub_blog_sidebar == 3 ) : ?>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <?php get_sidebar( 'left' ); ?>
            </div>

        <?php endif; ?>

        <div class="<?php echo esc_attr( $tz_night_club_column ); ?> col-sm-12 col-xs-12">

            <?php

            if ( have_posts() ) :

                the_archive_title( '<h1 class="tz_nightclub_title_cat tzarchive">', '</h1>' );
                the_archive_description( '<div class="taxonomy-description">', '</div>' );

            endif;

            ?>


            <?php

            if ( have_posts() ) : while (have_posts()) : the_post() ;

                $tz_1040nightclub_posy_image_cat    =   get_post_meta( get_the_ID(),'nightclub_post_image_cat',true );

            ?>

                <article id='post-<?php the_ID(); ?>' class="post-item">

                    <?php

                    if ( $tz_1040nightclub_posy_image_cat != '' ) :
                        echo '<img src="'.esc_url( $tz_1040nightclub_posy_image_cat ).'" alt="'.get_bloginfo('title').'" />';
                    else:
                        the_post_thumbnail( 'full' );
                    endif;

                    ?>

                    <h3>
                        <a href="<?php the_permalink() ?>">
                            <?php the_title(); ?>
                        </a>
                    </h3>

                    <?php if ( has_category() ): ?>

                        <div class="tz_single_tag">
                            <span class="tz_icon_single">
                                <i class="fa fa-folder" aria-hidden="true"></i>
                                <?php the_category( ', ' ); ?>
                            </span>
                        </div>

                    <?php endif; ?>

                    <?php if ( get_the_tags () !=  false ): ?>

                        <div class="tz_single_tag">
                            <span class="tz_icon_single">
                                <i class="fa fa-tags fa-rotate-90" aria-hidden="true"></i>
                                <?php the_tags('',' '); ?>
                            </span>
                        </div>

                    <?php endif; ?>

                    <div class="description">
                        <?php the_excerpt(); ?>
                    </div>
                </article>

                <?php

            endwhile; // end while ( have_posts )
            endif; // end if ( have_posts )

            tz_1040nightclub_paging_nav();

            ?>

        </div>

        <?php if ( $tz_1040nightclub_blog_sidebar == 1 || $tz_1040nightclub_blog_sidebar == 3 ) : ?>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <?php get_sidebar(); ?>
            </div>

        <?php endif; ?>

    </div>
</section>

<?php

get_template_part('template_inc/inc','footer-home-page');
get_footer();

?>

