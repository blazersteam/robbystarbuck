<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
    return;
?>

<div id="comments" class="comments-area">

    <?php if ( have_comments() ) : ?>
        <div class="tz-Comment">
            <h3 class="TzCommentTitle">
                <?php echo get_comments_number();?>&nbsp;<?php esc_html_e('Comments','1040nightclub');?>
            </h3>

            <ol class="comment-list">
                <?php wp_list_comments( array( 'callback' => 'tz_1040nightclub_comment', 'style' => 'ol' ) ); ?>
            </ol><!-- .comment-list -->

            <?php
            // Are there comments to navigate through?
            if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
            ?>
                <nav class="navigation comment-navigation" role="navigation">
                    <h1 class="screen-reader-text section-heading"><?php esc_html_e( 'Comment navigation','1040nightclub' ); ?></h1>
                    <div class="nav-previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments','1040nightclub' ) ); ?></div>
                    <div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;','1040nightclub' ) ); ?></div>
                </nav><!-- .comment-navigation -->
            <?php endif; // Check for comment navigation ?>

            <?php if ( ! comments_open() && get_comments_number() ) : ?>
                <p class="no-comments"><?php esc_html_e( 'Comments are closed.' ,'1040nightclub' ); ?></p>
            <?php endif; ?>
        </div>
    <?php endif; // have_comments() ?>

    <?php

    $tz_1040nightclub_commenter = wp_get_current_commenter();

    $tz_1040nightclub_req = get_option( 'require_name_email' );
    $tz_1040nightclub_comments_args = ( $tz_1040nightclub_req ? " aria-required='true'" : '' );

    $tz_1040nightclub_comments_args = array(

        'title_reply'       => '<span>'.esc_html__( 'Leave a comment','1040nightclub' ).'</span>',

        'fields' => apply_filters( 'comment_form_default_fields',
            array(

                'comment_notes_before' => '<div class="tz_fields_comment"><div class="row">',

                'author' => '<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12"><div class="form-comment-item"><input id="author" placeholder="'.esc_html__('Full Name','1040nightclub').'" class="form-control" name="author" type="text" value="' . esc_attr( $tz_1040nightclub_commenter['comment_author'] ) . '" size="30" ' . $tz_1040nightclub_comments_args . ' /></div></div>',

                'email' => '<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12"><div class="form-comment-item"><input id="email" placeholder="'.esc_html__('Your Email','1040nightclub').'" class="form-control" name="email" type="text" value="' . esc_attr( $tz_1040nightclub_commenter['comment_author_email'] ) . '" size="30" ' . $tz_1040nightclub_comments_args . ' /></div></div>',

                'comment_notes_after' => '</div></div>',

            )
        ),

        'comment_field' => '<div class="form-comment-item tz_fields_comment_box"><textarea rows="7" id="comment" placeholder="'.esc_html__('Comment','1040nightclub').'" name="comment" class="form-control"></textarea></div>',

    );

    comment_form( $tz_1040nightclub_comments_args );
    ?>

</div><!-- #comments -->