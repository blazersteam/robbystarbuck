<header class="tz-header">
    <div class="tz_header_content">
        <div class="tz_header_logo pull-left">
            <?php tz_1040nightclub_logo(); ?>
        </div>
        <div class="tz_button_menu">
            <div class="tz_menu_icon pull-right">
                <div class="tz_bar_menu">
                    <span class="bar1"></span>
                    <span class="bar2"></span>
                    <span class="bar3"></span>
                </div>
                <span class="tz_button_menu_text">
                    <?php esc_html_e( 'Menu', '1040nightclub' ) ?>
                </span>
            </div>
        </div>
    </div>
    <div class="tz_menu">
        <span class="tz_btn_close_menu">
            <i class="linea-arrows-remove"></i>
        </span>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <?php

        if ( has_nav_menu('primary') ) :
            tz_1040nightclub_menu_them();
        ?>

        <?php else: ?>

            <nav id="ml-menu" class="menu tz_nav_menu">
                <div class="menu__wrap">
                    <ul data-menu="main" class="menu__level main-menu">
                        <li>
                            <a href="<?php echo get_admin_url().'/nav-menus.php'; ?>">
                                <?php esc_html_e('ADD TO MENU','1040nightclub'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

        <?php endif; ?>
        <!-- /.navbar-collapse -->
    </div>
</header>