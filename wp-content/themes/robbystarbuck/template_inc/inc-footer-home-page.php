<?php

$tz_1040nightclub_copyright     =   ot_get_option( 'nightclub_copyright','Copyright &copy; Templaza' );
$tz_1040nightclub_music_press   =   ot_get_option( 'nightclub_music_press','' );

$tz_1040nightclub_class_footer_fix  =   '';
$tz_1040nightclub_class_row =   'col-lg-6 col-md-6';

if ( is_page_template('template-homepage.php') ) {
    $tz_1040nightclub_class_footer_fix  =   ' tz_footer_fix';
}

if ( is_page_template('template-homepage.php') && $tz_1040nightclub_music_press !='' ) :

    $tz_1040nightclub_class_row         =   'col-lg-3 col-md-3';

endif;

?>
<footer class="tz_footer<?php echo esc_attr( $tz_1040nightclub_class_footer_fix ); ?>">
    <div class="tz_footer_content">
        <div class="row">
            <div class="<?php echo esc_attr( $tz_1040nightclub_class_row ); ?> col-xs-12 col-sm-12">
                <div class="tz_footer_copyright">
                    <?php echo do_shortcode( $tz_1040nightclub_copyright ); ?>
                </div>
            </div>

            <?php if ( is_page_template('template-homepage.php') && $tz_1040nightclub_music_press !='' ) : ?>

                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                    <div class="tz_footer_music_press">
                        <?php echo do_shortcode( $tz_1040nightclub_music_press ); ?>
                    </div>
                </div>

            <?php endif; ?>

            <div class="<?php echo esc_attr( $tz_1040nightclub_class_row ); ?> col-xs-12 col-sm-12">
                <div class="tz_footer_social_network">
                    <?php tz_1040nightclub_social_network_footer(); ?>
                </div>
            </div>
        </div>

        <?php if ( is_page_template('template-homepage.php') && $tz_1040nightclub_music_press !='' ) : ?>

            <span class="tz_button_music"></span>

        <?php endif; ?>

    </div>
</footer>