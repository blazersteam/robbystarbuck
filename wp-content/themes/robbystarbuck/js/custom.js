"use strict";

jQuery(document).ready(function(){

    "use strict";

    /* Header */
    var $tz_header = jQuery( '.tz-header' );

    if ( $tz_header.length ) {

        jQuery( '.tz_menu_icon' ).on( "click", function() {
            jQuery(this).parents('.tz-header').find('.tz_menu').fadeIn(350);
        });

        jQuery( '.tz_btn_close_menu' ).on( "click", function() {
            jQuery(this).parent().fadeOut(350);
        });

    }
    /* End Header */

    /* Navigation Pagination */
    var $tz_page_pagination = jQuery( '.tz_page_pagination' );

    if ( $tz_page_pagination.length ) {

        var $data_page_id   = jQuery( '.tz_template_homepage' ).data('page-id'),
            $tz_id          = jQuery( '.tz_pagination_id' );

        $tz_id.each(function(){
            var $tz_id_val =  jQuery(this).data('id');

            if ( $tz_id_val === $data_page_id ) {
                jQuery(this).addClass('tz_page_active');
            }
        });

    }
    /* End Navigation Pagination */

    /* Coming Soon */
    var $tz_1040nightclub_coming_soon = jQuery( '.tz_nightclub_coming_soon' );

    if ( $tz_1040nightclub_coming_soon.length ) {

        $tz_1040nightclub_coming_soon.parents('.wpb_column').first().addClass('tz_wpb_column_coming_soon');

        var $tz_1040nightclub_countdown =   jQuery( '.tz_nightclub_countdown'),
            $data_countdown         =   $tz_1040nightclub_countdown.data('countdown'),
            $data_text_day          =   $tz_1040nightclub_countdown.data('text-day'),
            $data_text_hours        =   $tz_1040nightclub_countdown.data('text-hours'),
            $data_text_minutes      =   $tz_1040nightclub_countdown.data('text-minutes'),
            $data_text_seconds      =   $tz_1040nightclub_countdown.data('text-seconds'),
            $newsletter             =   jQuery( '.newsletter'),
            $tz_social_item         =   jQuery( '.tz_coming_soon_social_network' );

        jQuery('#clock').countdown($data_countdown, function(event) {
            var $this = jQuery(this).html(event.strftime(''
                + '<div class="tz_countdown_time"><h3>%D</h3><p>'+$data_text_day+'</p></div>'
                + '<span class="tz_icon_count_down">:</span>'
                + '<div class="tz_countdown_time"><h3>%H</h3><p>'+$data_text_hours+'</p></div>'
                + '<span class="tz_icon_count_down">:</span>'
                + '<div class="tz_countdown_time"><h3>%M</h3><p>'+$data_text_minutes+'</p></div>'
                + '<span class="tz_icon_count_down">:</span>'
                + '<div class="tz_countdown_time"><h3>%S</h3></span><p>'+$data_text_seconds+'</p></div>'
            ));
        });

        if ( $newsletter.length ) {
            jQuery("#newsletter-email").attr("placeholder", "Enter your email");
        }

        if ( $tz_social_item.length ) {
            var $data_color_social          =   $tz_social_item.data('color-social'),
                $data_color_social_hover    =   $tz_social_item.data('color-social-hover'),
                $tz_social_network_item     =   jQuery( '.tz_social_network_item' );

            if ( $data_color_social !== undefined ) {
                $tz_social_network_item.css("color", $data_color_social);
            }

            if ( $data_color_social_hover === undefined && $data_color_social !== undefined ) {

                $tz_social_network_item.hover(function(){
                    jQuery(this).css("color", "#d90057");
                }, function(){
                    jQuery(this).css("color", $data_color_social);
                });

            }

            if ( $data_color_social_hover !== undefined && $data_color_social !== undefined ) {

                $tz_social_network_item.hover(function(){
                    jQuery(this).css("color", $data_color_social_hover);
                }, function(){
                    jQuery(this).css("color", $data_color_social);
                });

            }

        }

    }
    /* End Coming Soon */

    /* Start Events Calendar  */
    var $tribe_events_footer    =   jQuery( '#tribe-events-footer' );
    if ( $tribe_events_footer.length ) {

        jQuery( '.tribe-events-sub-nav .tribe-events-nav-previous a span').html('<i class="lnr lnr-arrow-left"></i>');
        jQuery( '.tribe-events-sub-nav .tribe-events-nav-next a span').html('<i class="lnr lnr-arrow-right"></i>');

    }
    /* End Events Calendar  */

    /* Music Press */
    var $tz_footer_music_press = jQuery( '.tz_footer_music_press' );

    if ( $tz_footer_music_press.length ) {

        jQuery( '.tz_footer_music_press .jp-audio .jp-previous').html('<i class="fa fa-backward" aria-hidden="true"></i>');
        jQuery( '.tz_footer_music_press .jp-audio .jp-next').html('<i class="fa fa-forward" aria-hidden="true"></i>');

        jQuery( '.tz_footer_music_press .jp-audio .jp-controls').after('<span class="jp-image-bk"></span>');

        jQuery( '.tz_footer_music_press .jp-audio .jp-time-holder' ).after('<div class="jp-list-play-music"><i class="icon-music-playlist"></i></div>');

        jQuery( '.jp-list-play-music' ).on("click",function(){
            jQuery( '.jp-playlist').slideToggle(500);
        });

        jQuery( '.tz_button_music' ).on("click",function(){
            jQuery( '.tz_footer_music_press').slideToggle(500);
            jQuery(this).toggleClass('tz_button_music_open',500);
        });

    }
    /* End Music Press */

});

jQuery(window).scroll(function() {

    "use strict";

    var $tz_header      =   jQuery('.tz-header'),
        $tz_scrollTop   =   jQuery(window).scrollTop(),
        $tz_height_menu =   $tz_header.height();

    if ( $tz_header.length ) {

        if ( $tz_scrollTop > $tz_height_menu ) {
            $tz_header.addClass('tz_header_scroll');
        }else {
            $tz_header.removeClass('tz_header_scroll');
        }

    }

});

jQuery(window).on("load",function() {

    "use strict";

    jQuery('#tzloadding').remove();

});