<?php

get_header();
get_template_part('template_inc/inc','menu');

?>

<section class="home-post tz_cat_padding">
    <div class="row">

        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <?php
                if ( have_posts() ) : while (have_posts()) : the_post() ;
            ?>
                <article id='post-<?php the_ID(); ?>' class="post-item">

                    <?php if ( has_post_thumbnail() ) : ?>

                        <div class="entry-thumbnail">
                            <?php the_post_thumbnail(); ?>
                        </div>

                    <?php endif; ?>

                    <h3 class="tz_title_page">
                        <a href="<?php the_permalink() ?>">
                            <?php the_title(); ?>
                        </a>
                    </h3>

                    <?php if ( is_sticky() && is_home() && ! is_paged() ): ?>

                        <div class="tz_stickyPost">
                            <span><?php esc_html_e('Featured', '1040nightclub'); ?></span>
                        </div>

                    <?php endif; ?>

                    <div class="author">
                        <?php esc_html_e( 'Author','1040nightclub' ); ?>
                        <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' )) ); ?>">
                            <?php the_author(); ?>
                        </a>
                    </div>

                    <div class="description">
                        <?php

                        if ( has_excerpt() ) :
                            the_excerpt();
                        else:
                            the_content();
                            wp_link_pages( array(
                                'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', '1040nightclub' ) . '</span>',
                                'after'       => '</div>',
                                'link_before' => '<span>',
                                'link_after'  => '</span>',
                            ) );
                        endif;

                        ?>
                    </div>

                </article>
            <?php

                    endwhile; // end while ( have_posts )
                endif; // end if ( have_posts )

            tz_1040nightclub_paging_nav();

            ?>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <?php get_sidebar(); ?>
        </div>

    </div>
</section>

<?php

get_template_part('template_inc/inc','footer-home-page');
get_footer();

?>

