<?php

// enable and register custom sidebar
add_action('widgets_init', 'tz_1040nightclub_slug_widgets_init');
function tz_1040nightclub_slug_widgets_init()
{
    if (function_exists('register_sidebar')) {
        // default sidebar array
        $tz_1040nightclub_sidebar_attr = array(
            'name' => '',
            'id' => '',
            'description' => '',
            'before_widget' => '<aside id="%1$s" class="%2$s widget">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="module-title title-widget"><span>',
            'after_title' => '</span></h3>'
        );
        $tz_1040nightclub_sidebars = array(
            "nightclub-sidebar-main" => array("Display sidebar on all page", "Main Sidebar Right"),
            "nightclub-sidebar-main-2" => array("Display sidebar on all page", "Main Sidebar Left"),
            "nightclub-footer-1" => array("Display footer on all page", "Footer 1"),
            "nightclub-footer-2" => array("Display footer on all page", "Footer 2"),
            "nightclub-footer-3" => array("Display footer on all page", "Footer 3"),
            "nightclub-footer-4" => array("Display footer on all page", "Footer 4"),
        );
        foreach ($tz_1040nightclub_sidebars as $tz_1040nightclub_key => $tz_1040nightclub_value) {
            $tz_1040nightclub_sidebar_attr['id'] = $tz_1040nightclub_key;
            $tz_1040nightclub_sidebar_attr['description'] = $tz_1040nightclub_value[0];
            $tz_1040nightclub_sidebar_attr['name'] = $tz_1040nightclub_value[1];
            register_sidebar($tz_1040nightclub_sidebar_attr);
        }
    }
}

/* move_comment_field to bottom */
function tz_1040nightclub_move_comment_field_to_bottom($tz_1040nightclub_fields)
{
    $tz_1040nightclub_comment_field = $tz_1040nightclub_fields['comment'];
    unset($tz_1040nightclub_fields['comment']);
    $tz_1040nightclub_fields['comment'] = $tz_1040nightclub_comment_field;
    return $tz_1040nightclub_fields;
}

add_filter('comment_form_fields', 'tz_1040nightclub_move_comment_field_to_bottom');
/* End move_comment_field to bottom */

/* Logo */
if (!function_exists('tz_1040nightclub_logo')) :

    function tz_1040nightclub_logo()
    {

        $tz_1040nightclub_img_url = ot_get_option('nightclub_logo', '');

        ?>

        <a class="tz_logo" href="<?php echo esc_url(get_home_url('/')); ?>" title="<?php bloginfo('name'); ?>">

            <?php

            if (!empty($tz_1040nightclub_img_url)) :
                echo '<img src="' . esc_url($tz_1040nightclub_img_url) . '" alt="' . get_bloginfo('title') . '" />';
            else :
                echo '<h1 class="site-title">' . esc_attr(get_bloginfo('name')) . '</h1>' . '<h2 class="site-description">' . esc_attr(get_bloginfo('description')) . '</h2>';
            endif;

            ?>

        </a>

        <?php
    }

endif;
/* End Logo */

/* Menu */
if (!function_exists('tz_1040nightclub_menu_them')) :

    function tz_1040nightclub_menu_them()
    {

        $tz_1040nightclub_menu_name = 'primary';
        $tz_1040nightclub_locations = get_nav_menu_locations();
        $tz_1040nightclub_menu = wp_get_nav_menu_object($tz_1040nightclub_locations[$tz_1040nightclub_menu_name]);
        $tz_1040nightclub_menu_items = wp_get_nav_menu_items($tz_1040nightclub_menu->term_id, array('order' => 'DESC'));
        $tz_1040nightclub_menu_count = count($tz_1040nightclub_menu_items);

        ?>

        <nav id="ml-menu" class="menu tz_nav_menu">
            <div class="menu__wrap">
                <ul data-menu="main" class="menu__level main-menu">
                    <?php
                    foreach ($tz_1040nightclub_menu_items as $tz_1040nightclub_menu_item) :

                        $tz_1040nightclub_menu_has_item = $tz_1040nightclub_menu_item->ID;
                        $tz_1040nightclub_has_children = get_posts(
                            array(
                                'post_type' => 'nav_menu_item',
                                'nopaging' => true,
                                'numberposts' => 1,
                                'meta_key' => '_menu_item_menu_item_parent',
                                'meta_value' => $tz_1040nightclub_menu_has_item
                            )
                        );
                        // set up title and url
                        $tz_1040nightclub_menu_title = $tz_1040nightclub_menu_item->title;
                        $tz_1040nightclub_menu_link = $tz_1040nightclub_menu_item->url;

                        if (!$tz_1040nightclub_menu_item->menu_item_parent) :
                            ?>

                            <li class="menu__item">
                                <a class="menu__link"
                                   href="<?php echo esc_url($tz_1040nightclub_menu_link); ?>" <?php echo($tz_1040nightclub_has_children ? 'data-submenu="submenu-' . esc_attr($tz_1040nightclub_menu_has_item) . '"' : ''); ?>>
                                    <?php echo esc_attr($tz_1040nightclub_menu_title); ?>
                                </a>
                            </li>

                        <?php
                        endif;
                    endforeach;
                    ?>
                </ul>
                <!-- Sub Menu -->
                <?php

                for ($j = 0; $j < $tz_1040nightclub_menu_count; $j++) :

                    $tz_1040nightclub_parent_id = $tz_1040nightclub_menu_items[$j]->ID;
                    $tz_1040nightclub_children = get_posts(
                        array(
                            'post_type' => 'nav_menu_item',
                            'nopaging' => true,
                            'numberposts' => 1,
                            'meta_key' => '_menu_item_menu_item_parent',
                            'meta_value' => $tz_1040nightclub_parent_id
                        )
                    );

                    if ($tz_1040nightclub_children) :

                        ?>
                        <ul data-menu="submenu-<?php echo esc_attr($tz_1040nightclub_parent_id); ?>"
                            class="menu__level">
                            <?php

                            for ($z = 0; $z < $tz_1040nightclub_menu_count; $z++) :

                                if ($tz_1040nightclub_parent_id == $tz_1040nightclub_menu_items[$z]->menu_item_parent) :

                                    $tz_1040nightclub_parent_id_2 = $tz_1040nightclub_menu_items[$z]->ID;
                                    $tz_1040nightclub_children_2 = get_posts(
                                        array(
                                            'post_type' => 'nav_menu_item',
                                            'nopaging' => true,
                                            'numberposts' => 1,
                                            'meta_key' => '_menu_item_menu_item_parent',
                                            'meta_value' => $tz_1040nightclub_parent_id_2
                                        )
                                    );
                                    $tz_1040nightclub_menu_link_sub = $tz_1040nightclub_menu_items[$z]->url;
                                    $tz_1040nightclub_menu_title_sub = $tz_1040nightclub_menu_items[$z]->title;

                                    ?>

                                    <li class="menu__item">
                                        <a class="menu__link"
                                           href="<?php echo esc_url($tz_1040nightclub_menu_link_sub); ?>" <?php echo($tz_1040nightclub_children_2 ? 'data-submenu="submenu-' . esc_attr($tz_1040nightclub_parent_id_2) . '"' : ''); ?>>
                                            <?php echo esc_attr($tz_1040nightclub_menu_title_sub); ?>
                                        </a>
                                    </li>

                                <?php
                                endif;
                            endfor;
                            ?>
                        </ul>
                    <?php
                    endif;
                endfor;
                ?>
                <!-- End Sub Menu -->
            </div>
        </nav>

        <?php

    }

endif;
/* End Menu */

/* Social network */
function tz_1040nightclub_get_social_network()
{
    return array(
        array('id' => 'facebook-square', 'title' => 'facebook'),
        array('id' => 'twitter-square', 'title' => 'twitter'),
        array('id' => 'flickr', 'title' => 'flickr'),
        array('id' => 'behance-square', 'title' => 'behance'),
        array('id' => 'instagram', 'title' => 'instagram'),
        array('id' => 'digg', 'title' => 'digg'),
        array('id' => 'dribbble', 'title' => 'dribbble'),
        array('id' => 'dropbox', 'title' => 'dropbox'),
        array('id' => 'google-plus-square', 'title' => 'google-plus'),
        array('id' => 'linkedin-square', 'title' => 'linkedin'),
        array('id' => 'foursquare', 'title' => 'foursquare'),
        array('id' => 'pinterest-square', 'title' => 'pinterest'),
        array('id' => 'skype', 'title' => 'skype'),
        array('id' => 'tumblr-square', 'title' => 'tumblr'),
        array('id' => 'vimeo-square', 'title' => 'vimeo'),
        array('id' => 'youtube-square', 'title' => 'youtube'),
    );
}

if (!function_exists('tz_1040nightclub_social_network')) :

    function tz_1040nightclub_social_network()
    {

        /* get the option array */
        $tz_1040nightclub_social_networks = tz_1040nightclub_get_social_network();

        foreach ($tz_1040nightclub_social_networks as $tz_1040nightclub_social) :

            $tz_1040nightclub_social_url = ot_get_option('nightclub_social_network_' . $tz_1040nightclub_social['title']);

            if ($tz_1040nightclub_social_url != '') :
                ?>

                <a class="tz_social_network_item" href="<?php echo esc_url($tz_1040nightclub_social_url); ?>"
                   target="_blank">
                    <i class="fa fa-<?php echo esc_attr($tz_1040nightclub_social['id']); ?>" aria-hidden="true"></i>
                </a>

            <?php
            endif;
        endforeach;

    }

endif;

if (!function_exists('tz_1040nightclub_social_network_footer')) :

    function tz_1040nightclub_social_network_footer()
    {

        /* get the option array */
        $tz_1040nightclub_social_networks = tz_1040nightclub_get_social_network();

        foreach ($tz_1040nightclub_social_networks as $tz_1040nightclub_social) :

            $tz_1040nightclub_social_url = ot_get_option('nightclub_social_network_' . $tz_1040nightclub_social['title']);

            if ($tz_1040nightclub_social_url != '') :
                ?>

                <a class="tz_social_network_item_footer" href="<?php echo esc_url($tz_1040nightclub_social_url); ?>"
                   target="_blank">
                    <i class="fa fa-<?php echo esc_attr($tz_1040nightclub_social['title']); ?>" aria-hidden="true"></i>
                </a>

            <?php
            endif;
        endforeach;

    }

endif;
/* End Social network */

/*  Comment */
if (!function_exists('tz_1040nightclub_comment')) :
    function tz_1040nightclub_comment($comment, $args, $depth)
    {

        $tz_1040nightclub_time_comment = ot_get_option('nightclub_single_time_comment', 0);

        $GLOBALS['comment'] = $comment;
        switch ($comment->comment_type) :
            case 'pingback' :
            case 'trackback' :
                // Display trackbacks differently than normal comments.
                ?>
                <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
                <p>
                    <?php esc_html_e('Pingback:', '1040nightclub'); ?><?php comment_author_link(); ?><?php edit_comment_link(esc_html__('(Edit)', '1040nightclub'), '<span class="edit-link">', '</span>'); ?>
                </p>
                <?php
                break;
            default :
                // Proceed with normal comments.

                ?>
                <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
                <div id="comment-<?php comment_ID(); ?>" class="comment-body">
                    <?php if ('0' == $comment->comment_approved) : ?>
                        <p class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', '1040nightclub'); ?></p>
                    <?php endif; ?>
                    <div class="comment-author">
                        <?php echo get_avatar($comment, 70); ?>
                    </div>
                    <div class="comment-content">
                        <div class="tz_comment_author">
                                    <span class="name">
                                        <?php comment_author_link(); ?>
                                    </span>
                            <i class="fa fa-circle" aria-hidden="true"></i>
                            <span class="comment-metadata">

                                        <?php if ($tz_1040nightclub_time_comment == 1) : ?>

                                            <?php echo get_comment_date(get_option('date_format')) ?><?php esc_html_e('at', '1040nightclub'); ?><?php echo get_comment_time(get_option('time_format')); ?>

                                        <?php else: ?>

                                            <?php printf(_x('%s ago', '%s = human-readable time difference', '1040nightclub'), human_time_diff(get_comment_time('U'), current_time('timestamp'))); ?>

                                        <?php endif; ?>

                                    </span>
                        </div>

                        <?php comment_text(); ?>

                        <?php
                        edit_comment_link(esc_html__('Edit ', '1040nightclub'));
                        comment_reply_link(array_merge($args, array('reply_text' => esc_html__('Reply', '1040nightclub'), 'depth' => $depth, 'max_depth' => $args['max_depth'])));
                        ?>

                    </div><!--comment-content -->

                </div><!-- #comment-## -->
                <?php
                break;
        endswitch; // end comment_type check
    }
endif;
/*  End Comment */

/*  navigation */
if (!function_exists('tz_1040nightclub_getPrevNext')) :

    function tz_1040nightclub_getPrevNext()
    {

        $tz_1040nightclub_page_check = ot_get_option('nightclub_TzGlobalOption_pagination_home_page_check', '');
        if ($tz_1040nightclub_page_check == 1) {
            $tz_1040nightclub_page_check_menu = ot_get_option('nightclub_page_check_menu', '');
            $tz_1040nightclub_page_list = get_pages(
                array(
                    'sort_column' => 'ID',
                    'sort_order' => 'DESC',
                    'include' => $tz_1040nightclub_page_check_menu,
                    'meta_key' => '_wp_page_template',
                    'meta_value' => 'template-homepage.php'
                )
            );
            $tz_1040nightclub_pages = array();

            foreach ($tz_1040nightclub_page_list as $tz_1040nightclub_page) {
                $tz_1040nightclub_pages[] += $tz_1040nightclub_page->ID;
            }

            $tz_1040nightclub_current = array_search(get_the_ID(), $tz_1040nightclub_pages);

            if ($tz_1040nightclub_current > 0) {
                $tz_1040nightclub_prevID = $tz_1040nightclub_pages[$tz_1040nightclub_current - 1];
            }

            $tz_1040nightclub_nextID = $tz_1040nightclub_pages[$tz_1040nightclub_current + 1];
            ?>
            <div class="tz_navigation_pagination">

                <?php if (!empty($tz_1040nightclub_prevID)) : ?>
                    <div class="tz_navigation_next_prev">
                        <a href="<?php echo esc_url(get_permalink($tz_1040nightclub_prevID)); ?>"
                           title="<?php echo esc_attr(get_the_title($tz_1040nightclub_prevID)); ?>">
                            <i class="tz_icon_next_prev linea-arrows-left" aria-hidden="true"></i>
                        </a>
                    </div>
                <?php endif; ?>

                <div class="tz_page_pagination">
                    <?php foreach ($tz_1040nightclub_page_list as $tz_1040nightclub_page) : ?>
                        <a class="tz_pagination_id" href="<?php echo esc_url(get_permalink($tz_1040nightclub_page->ID)); ?>"
                           data-id="<?php echo esc_attr($tz_1040nightclub_page->ID); ?>">
                            <span class="tz_pagination_title"><?php echo esc_attr(get_the_title($tz_1040nightclub_page->ID)); ?></span>
                            <span class="tz_circle_pagination"></span>
                        </a>
                    <?php endforeach; ?>
                </div>

                <?php if (!empty($tz_1040nightclub_nextID)) : ?>
                    <div class="tz_navigation_next_prev">
                        <a href="<?php echo esc_url(get_permalink($tz_1040nightclub_nextID)); ?>"
                           title="<?php echo esc_attr(get_the_title($tz_1040nightclub_nextID)); ?>">
                            <i class="tz_icon_next_prev linea-arrows-right" aria-hidden="true"></i>
                        </a>
                    </div>
                <?php endif; ?>

            </div>
            <?php
        } else {
            $tz_1040nightclub_page_check_menu_string = ot_get_option('nightclub_TzGlobalOption_pagination_home_page_data_input', '');
            $tz_1040nightclub_page_check_menu = explode(',', $tz_1040nightclub_page_check_menu_string);
            $tz_1040nightclub_pages = array();

            foreach ($tz_1040nightclub_page_check_menu as $tz_1040nightclub_page) {
                $tz_1040nightclub_pages[] += $tz_1040nightclub_page;
            }

            $tz_1040nightclub_current = array_search(get_the_ID(), $tz_1040nightclub_pages);


            if ($tz_1040nightclub_current > 0) {
                $tz_1040nightclub_prevID = $tz_1040nightclub_pages[$tz_1040nightclub_current - 1];
            }

            $tz_1040nightclub_nextID = $tz_1040nightclub_pages[$tz_1040nightclub_current + 1];
            ?>
            <div class="tz_navigation_pagination">

                <?php if (!empty($tz_1040nightclub_prevID)) : ?>
                    <div class="tz_navigation_next_prev">
                        <a href="<?php echo esc_url(get_permalink($tz_1040nightclub_prevID)); ?>"
                           title="<?php echo esc_attr(get_the_title($tz_1040nightclub_prevID)); ?>">
                            <i class="tz_icon_next_prev linea-arrows-left" aria-hidden="true"></i>
                        </a>
                    </div>
                <?php endif; ?>

                <div class="tz_page_pagination">
                    <?php foreach ($tz_1040nightclub_page_check_menu as $tz_1040nightclub_page) : ?>
                        <a class="tz_pagination_id" href="<?php echo esc_url(get_permalink($tz_1040nightclub_page)); ?>"
                           data-id="<?php echo esc_attr($tz_1040nightclub_page); ?>">
                            <span class="tz_pagination_title"><?php echo esc_attr(get_the_title($tz_1040nightclub_page)); ?></span>
                            <span class="tz_circle_pagination"></span>
                        </a>
                    <?php endforeach; ?>
                </div>

                <?php if (!empty($tz_1040nightclub_nextID)) : ?>
                    <div class="tz_navigation_next_prev">
                        <a href="<?php echo esc_url(get_permalink($tz_1040nightclub_nextID)); ?>"
                           title="<?php echo esc_attr(get_the_title($tz_1040nightclub_nextID)); ?>">
                            <i class="tz_icon_next_prev linea-arrows-right" aria-hidden="true"></i>
                        </a>
                    </div>
                <?php endif; ?>

            </div>
            <?php

        }

    }

endif;

/*  End navigation */

?>