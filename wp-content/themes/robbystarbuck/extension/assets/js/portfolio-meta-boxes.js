/*global jQuery: false, themeprefix: false */

jQuery(function(){
  "use strict";

  var $type_value = jQuery('#nightclub_portfolio_type'),
      $nightclub_portfolio_floating         =   jQuery('#setting_nightclub_portfolio_floating'),
      $nightclub_portfolio_slideshows_style =   jQuery('#setting_nightclub_portfolio_slideshows_style'),
      $nightclub_portfolio_slideshows       =   jQuery('#setting_nightclub_portfolio_slideshows'),
      $nightclub_portfolio_video_mp4        =   jQuery('#setting_nightclub_portfolio_video_mp4'),
      $nightclub_portfolio_video_ogv        =   jQuery('#setting_nightclub_portfolio_video_ogv'),
      $nightclub_portfolio_video_webm       =   jQuery('#setting_nightclub_portfolio_video_webm'),
      $nightclub_portfolio_gallery          =   jQuery('#setting_nightclub_portfolio_gallery'),
      $nightclub_portfolio_soundCloud_id    =   jQuery('#setting_nightclub_portfolio_soundCloud_id'),
      $nightclub_portfolio_Quote_content    =   jQuery('#setting_nightclub_portfolio_Quote_content'),
      $nightclub_portfolio_Quote_ds         =   jQuery('#setting_nightclub_portfolio_Quote_ds');

  switch ( $type_value.val() ) {
      case 'floating':
          $nightclub_portfolio_floating.slideDown();
          $nightclub_portfolio_slideshows_style.slideUp();
          $nightclub_portfolio_slideshows.slideUp();
          $nightclub_portfolio_video_mp4.slideUp();
          $nightclub_portfolio_video_ogv.slideUp();
          $nightclub_portfolio_video_webm.slideUp();
          $nightclub_portfolio_gallery.slideUp();
          $nightclub_portfolio_soundCloud_id.slideUp();
          $nightclub_portfolio_Quote_content.slideUp();
          $nightclub_portfolio_Quote_ds.slideUp();
          break;
      case 'video':
          $nightclub_portfolio_floating.slideUp();
          $nightclub_portfolio_slideshows_style.slideUp();
          $nightclub_portfolio_slideshows.slideUp();
          $nightclub_portfolio_video_mp4.slideDown();
          $nightclub_portfolio_video_ogv.slideDown();
          $nightclub_portfolio_video_webm.slideDown();
          $nightclub_portfolio_gallery.slideUp();
          $nightclub_portfolio_soundCloud_id.slideUp();
          $nightclub_portfolio_Quote_content.slideUp();
          $nightclub_portfolio_Quote_ds.slideUp();
          break;
      case 'slideshows':
          $nightclub_portfolio_floating.slideUp();
          $nightclub_portfolio_slideshows_style.slideDown();
          $nightclub_portfolio_slideshows.slideDown();
          $nightclub_portfolio_video_mp4.slideUp();
          $nightclub_portfolio_video_ogv.slideUp();
          $nightclub_portfolio_video_webm.slideUp();
          $nightclub_portfolio_gallery.slideUp();
          $nightclub_portfolio_soundCloud_id.slideUp();
          $nightclub_portfolio_Quote_content.slideUp();
          $nightclub_portfolio_Quote_ds.slideUp();
          break;
      case 'image':
          $nightclub_portfolio_floating.slideUp();
          $nightclub_portfolio_slideshows_style.slideDown();
          $nightclub_portfolio_slideshows.slideUp();
          $nightclub_portfolio_video_mp4.slideUp();
          $nightclub_portfolio_video_ogv.slideUp();
          $nightclub_portfolio_video_webm.slideUp();
          $nightclub_portfolio_gallery.slideUp();
          $nightclub_portfolio_soundCloud_id.slideUp();
          $nightclub_portfolio_Quote_content.slideUp();
          $nightclub_portfolio_Quote_ds.slideUp();
          break;
      case 'gallery':
          $nightclub_portfolio_floating.slideUp();
          $nightclub_portfolio_slideshows_style.slideUp();
          $nightclub_portfolio_slideshows.slideUp();
          $nightclub_portfolio_video_mp4.slideUp();
          $nightclub_portfolio_video_ogv.slideUp();
          $nightclub_portfolio_video_webm.slideUp();
          $nightclub_portfolio_gallery.slideDown();
          $nightclub_portfolio_soundCloud_id.slideUp();
          $nightclub_portfolio_Quote_content.slideUp();
          $nightclub_portfolio_Quote_ds.slideUp();
          break;
      case 'audio':
          $nightclub_portfolio_floating.slideUp();
          $nightclub_portfolio_slideshows_style.slideUp();
          $nightclub_portfolio_slideshows.slideUp();
          $nightclub_portfolio_video_mp4.slideUp();
          $nightclub_portfolio_video_ogv.slideUp();
          $nightclub_portfolio_video_webm.slideUp();
          $nightclub_portfolio_gallery.slideUp();
          $nightclub_portfolio_soundCloud_id.slideDown();
          $nightclub_portfolio_Quote_content.slideUp();
          $nightclub_portfolio_Quote_ds.slideUp();
          break;
      case 'quote':
          $nightclub_portfolio_floating.slideUp();
          $nightclub_portfolio_slideshows_style.slideUp();
          $nightclub_portfolio_slideshows.slideUp();
          $nightclub_portfolio_video_mp4.slideUp();
          $nightclub_portfolio_video_ogv.slideUp();
          $nightclub_portfolio_video_webm.slideUp();
          $nightclub_portfolio_gallery.slideUp();
          $nightclub_portfolio_soundCloud_id.slideUp();
          $nightclub_portfolio_Quote_content.slideDown();
          $nightclub_portfolio_Quote_ds.slideDown();
          break;
      default :
          $nightclub_portfolio_floating.slideUp();
          $nightclub_portfolio_slideshows_style.slideUp();
          $nightclub_portfolio_slideshows.slideUp();
          $nightclub_portfolio_video_mp4.slideUp();
          $nightclub_portfolio_video_ogv.slideUp();
          $nightclub_portfolio_video_webm.slideUp();
          $nightclub_portfolio_gallery.slideUp();
          $nightclub_portfolio_soundCloud_id.slideUp();
          $nightclub_portfolio_Quote_content.slideUp();
          $nightclub_portfolio_Quote_ds.slideUp();
          break;
  }

    $type_value.change(function(){
        switch (jQuery(this).val()) {
            case 'floating':
                $nightclub_portfolio_floating.slideDown();
                $nightclub_portfolio_slideshows_style.slideUp();
                $nightclub_portfolio_slideshows.slideUp();
                $nightclub_portfolio_video_mp4.slideUp();
                $nightclub_portfolio_video_ogv.slideUp();
                $nightclub_portfolio_video_webm.slideUp();
                $nightclub_portfolio_gallery.slideUp();
                $nightclub_portfolio_soundCloud_id.slideUp();
                $nightclub_portfolio_Quote_content.slideUp();
                $nightclub_portfolio_Quote_ds.slideUp();
                break;
            case 'video':
                $nightclub_portfolio_floating.slideUp();
                $nightclub_portfolio_slideshows_style.slideUp();
                $nightclub_portfolio_slideshows.slideUp();
                $nightclub_portfolio_video_mp4.slideDown();
                $nightclub_portfolio_video_ogv.slideDown();
                $nightclub_portfolio_video_webm.slideDown();
                $nightclub_portfolio_gallery.slideUp();
                $nightclub_portfolio_soundCloud_id.slideUp();
                $nightclub_portfolio_Quote_content.slideUp();
                $nightclub_portfolio_Quote_ds.slideUp();
                break;
            case 'slideshows':
                $nightclub_portfolio_floating.slideUp();
                $nightclub_portfolio_slideshows_style.slideDown();
                $nightclub_portfolio_slideshows.slideDown();
                $nightclub_portfolio_video_mp4.slideUp();
                $nightclub_portfolio_video_ogv.slideUp();
                $nightclub_portfolio_video_webm.slideUp();
                $nightclub_portfolio_gallery.slideUp();
                $nightclub_portfolio_soundCloud_id.slideUp();
                $nightclub_portfolio_Quote_content.slideUp();
                $nightclub_portfolio_Quote_ds.slideUp();
                break;
            case 'image':
                $nightclub_portfolio_floating.slideUp();
                $nightclub_portfolio_slideshows_style.slideDown();
                $nightclub_portfolio_slideshows.slideUp();
                $nightclub_portfolio_video_mp4.slideUp();
                $nightclub_portfolio_video_ogv.slideUp();
                $nightclub_portfolio_video_webm.slideUp();
                $nightclub_portfolio_gallery.slideUp();
                $nightclub_portfolio_soundCloud_id.slideUp();
                $nightclub_portfolio_Quote_content.slideUp();
                $nightclub_portfolio_Quote_ds.slideUp();
                break;
            case 'gallery':
                $nightclub_portfolio_floating.slideUp();
                $nightclub_portfolio_slideshows_style.slideUp();
                $nightclub_portfolio_slideshows.slideUp();
                $nightclub_portfolio_video_mp4.slideUp();
                $nightclub_portfolio_video_ogv.slideUp();
                $nightclub_portfolio_video_webm.slideUp();
                $nightclub_portfolio_gallery.slideDown();
                $nightclub_portfolio_soundCloud_id.slideUp();
                $nightclub_portfolio_Quote_content.slideUp();
                $nightclub_portfolio_Quote_ds.slideUp();
                break;
            case 'audio':
                $nightclub_portfolio_floating.slideUp();
                $nightclub_portfolio_slideshows_style.slideUp();
                $nightclub_portfolio_slideshows.slideUp();
                $nightclub_portfolio_video_mp4.slideUp();
                $nightclub_portfolio_video_ogv.slideUp();
                $nightclub_portfolio_video_webm.slideUp();
                $nightclub_portfolio_gallery.slideUp();
                $nightclub_portfolio_soundCloud_id.slideDown();
                $nightclub_portfolio_Quote_content.slideUp();
                $nightclub_portfolio_Quote_ds.slideUp();
                break;
            case 'quote':
                $nightclub_portfolio_floating.slideUp();
                $nightclub_portfolio_slideshows_style.slideUp();
                $nightclub_portfolio_slideshows.slideUp();
                $nightclub_portfolio_video_mp4.slideUp();
                $nightclub_portfolio_video_ogv.slideUp();
                $nightclub_portfolio_video_webm.slideUp();
                $nightclub_portfolio_gallery.slideUp();
                $nightclub_portfolio_soundCloud_id.slideUp();
                $nightclub_portfolio_Quote_content.slideDown();
                $nightclub_portfolio_Quote_ds.slideDown();
                break;
            default :
                $nightclub_portfolio_floating.slideUp();
                $nightclub_portfolio_slideshows_style.slideUp();
                $nightclub_portfolio_slideshows.slideUp();
                $nightclub_portfolio_video_mp4.slideUp();
                $nightclub_portfolio_video_ogv.slideUp();
                $nightclub_portfolio_video_webm.slideUp();
                $nightclub_portfolio_gallery.slideUp();
                $nightclub_portfolio_soundCloud_id.slideUp();
                $nightclub_portfolio_Quote_content.slideUp();
                $nightclub_portfolio_Quote_ds.slideUp();
                break;
        }
    });


    /* BK home page */
    var $tz_1040nightclub_hide_show_bk_image    =   jQuery( '#nightclub_hide_show_bk_image'),
        $tz_1040nightclub_bk_image_page_home    =   jQuery( '#setting_nightclub_bk_image_page_home' );

    switch ( $tz_1040nightclub_hide_show_bk_image.val() ){
        case '1':
            $tz_1040nightclub_bk_image_page_home.slideDown();
            break;
        default :
            $tz_1040nightclub_bk_image_page_home.slideUp();
            break;

    }

    $tz_1040nightclub_hide_show_bk_image.change(function(){

        var $tz_1040nightclub_hide_show_bk_image_value = jQuery(this).val();

        switch ($tz_1040nightclub_hide_show_bk_image_value){
            case '1':
                $tz_1040nightclub_bk_image_page_home.slideDown();
                break;
            default :
                $tz_1040nightclub_bk_image_page_home.slideUp();
                break;
        }
    });
    /* End home page */

  var $checkpage            =   jQuery('#page_template'),
      $page_home_meta_box   =   jQuery('#page_home_meta_box');

    switch ( $checkpage.val() ){
        case 'template-homepage.php':
            $page_home_meta_box.css('display','block');
            break;
        default :
            $page_home_meta_box.css('display','none');
            break;

    }

    $checkpage.change(function(){

     var $value = jQuery(this).val();

      switch ($value){
          case 'template-homepage.php':
              $page_home_meta_box.css('display','block');
              break;
          default :
              $page_home_meta_box.css('display','none');
              break;
      }
  });

});
