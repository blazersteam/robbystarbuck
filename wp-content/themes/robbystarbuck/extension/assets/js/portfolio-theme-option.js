jQuery(document).ready(function(){

    /* Method Social Network */
    jQuery("#tab_social_network").toggle(function(){
        jQuery('#tab_color_social_network').slideDown();
    }, function(){
        jQuery('#tab_color_social_network').slideUp();
    });
    /* End Method Social Network */

    /* jquery style option */
    jQuery("#tab_TzSyle").toggle(function(){
        jQuery('#tab_TZFamily').slideDown();
    },function(){
        jQuery('#tab_TZFamily').slideUp();
    });
    /* End jquery style option */

    /* method font family */
    var tz_select_font_theme        =   jQuery("#nightclub_select_font_theme"),
        tz_select_font_theme_val    =   tz_select_font_theme.attr('value'),
        TzFontTextFamily            =   jQuery('#setting_nightclub_font_text_family'),
        TzFontContent               =   jQuery('#setting_nightclub_font_content'),
        TzFontMenu                  =   jQuery('#setting_nightclub_font_menu'),
        TzFontBigHeading            =   jQuery('#setting_nightclub_font_headings'),
        TzFontSmallHeading          =   jQuery('#setting_nightclub_font_headings_small'),
        TzFontInfoGoogle            =   jQuery('#setting_nightclub_font_info_google'),
        TzFontWeight                =   jQuery('#setting_nightclub_font_weight'),
        TzFontSubset                =   jQuery('#setting_nightclub_font_subset'),
        TzFontStylet_wrap           =   jQuery('.TzFontStylet-wrap');

    TzFontStylet_wrap.addClass('TzFontStyle_display');

    switch (tz_select_font_theme_val){
        case '1':
            TzFontTextFamily.slideUp('slow');
            TzFontContent.slideUp('slow');
            TzFontMenu.slideUp('slow');
            TzFontBigHeading.slideUp('slow');
            TzFontSmallHeading.slideUp('slow');
            TzFontInfoGoogle.slideUp('slow');
            TzFontSubset.slideUp('slow');
            TzFontWeight.slideUp('slow');
            break;
        case '2':
            TzFontTextFamily.slideDown('slow');
            TzFontContent.slideDown('slow');
            TzFontMenu.slideDown('slow');
            TzFontBigHeading.slideDown('slow');
            TzFontSmallHeading.slideDown('slow');
            TzFontInfoGoogle.slideDown('slow');
            TzFontWeight.slideDown('slow');
            TzFontSubset.slideDown('slow');
            TzFontStylet_wrap.removeClass('TzFontStyle_display');
            break;
        case '3':
            TzFontTextFamily.slideDown('slow');
            TzFontContent.slideDown('slow');
            TzFontMenu.slideDown();
            TzFontBigHeading.slideDown('slow');
            TzFontSmallHeading.slideDown('slow');
            TzFontInfoGoogle.slideDown('slow');
            TzFontWeight.slideDown('slow');
            TzFontSubset.slideDown('slow');
            TzFontStylet_wrap.addClass('TzFontStyle_display');

            break;
    }

    tz_select_font_theme.change(function(){
        var FontCheck = tz_select_font_theme.attr('value');
        switch (FontCheck){
            case '1':
                TzFontTextFamily.slideUp('slow');
                TzFontContent.slideUp('slow');
                TzFontMenu.slideUp('slow');
                TzFontBigHeading.slideUp('slow');
                TzFontSmallHeading.slideUp('slow');
                TzFontInfoGoogle.slideUp('slow');
                TzFontSubset.slideUp('slow');
                TzFontWeight.slideUp('slow');
                break;
            case '2':
                TzFontTextFamily.slideDown('slow');
                TzFontContent.slideDown('slow');
                TzFontMenu.slideDown('slow');
                TzFontBigHeading.slideDown('slow');
                TzFontSmallHeading.slideDown('slow');
                TzFontInfoGoogle.slideDown('slow');
                TzFontWeight.slideDown('slow');
                TzFontSubset.slideDown('slow');
                TzFontStylet_wrap.removeClass('TzFontStyle_display');
                break;
            case '3':
                TzFontTextFamily.slideDown('slow');
                TzFontContent.slideDown('slow');
                TzFontMenu.slideDown('slow');
                TzFontBigHeading.slideDown('slow');
                TzFontSmallHeading.slideDown('slow');
                TzFontInfoGoogle.slideDown('slow');
                TzFontWeight.slideDown('slow');
                TzFontSubset.slideDown('slow');
                TzFontStylet_wrap.addClass('TzFontStyle_display');
                break;
        }
    });
    /*  End method font family */

    // jquery favicon option
    var $valuefavicon       =   jQuery('#nightclub_favicon_onoff'),
        $valuefavicon_val   =   $valuefavicon.attr('value');
    if( $valuefavicon_val == 'yes' ){
        jQuery('#setting_nightclub_favicon').slideDown();
    }else{
        jQuery('#setting_nightclub_favicon').slideUp();
    }

    $valuefavicon.change(function(){
        if(jQuery(this).attr('value')=='yes'){
            jQuery('#setting_nightclub_favicon').slideDown();
        }else{
            jQuery('#setting_nightclub_favicon').slideUp();
        }
    });

/* footer */
    var $nightclub_footer_columns           =   jQuery('#nightclub_footer_columns'),
        $option_tree_ui_radio_images_eq0    =   jQuery('#setting_plazartthemefooter_image .option-tree-ui-radio-images:eq(0)'),
        $option_tree_ui_radio_images_eq1    =   jQuery('#setting_plazartthemefooter_image .option-tree-ui-radio-images:eq(1)'),
        $option_tree_ui_radio_images_eq2    =   jQuery('#setting_plazartthemefooter_image .option-tree-ui-radio-images:eq(2)'),
        $option_tree_ui_radio_images_eq3    =   jQuery('#setting_plazartthemefooter_image .option-tree-ui-radio-images:eq(3)');

    $nightclub_footer_columns.change(function(){

        var footerchange = jQuery(this).attr('value');

        switch (footerchange){
            case'1':
                $option_tree_ui_radio_images_eq0.slideDown();
                $option_tree_ui_radio_images_eq1.slideUp();
                $option_tree_ui_radio_images_eq2.slideUp();
                $option_tree_ui_radio_images_eq3.slideUp();
                break;
            case'2':
                $option_tree_ui_radio_images_eq0.slideDown();
                $option_tree_ui_radio_images_eq1.slideDown();
                $option_tree_ui_radio_images_eq2.slideUp();
                $option_tree_ui_radio_images_eq3.slideUp();
                break;
            case'3':
                $option_tree_ui_radio_images_eq0.slideDown();
                $option_tree_ui_radio_images_eq1.slideDown();
                $option_tree_ui_radio_images_eq2.slideDown();
                $option_tree_ui_radio_images_eq3.slideUp();
                break;
            case'4':
                $option_tree_ui_radio_images_eq0.slideDown();
                $option_tree_ui_radio_images_eq1.slideDown();
                $option_tree_ui_radio_images_eq2.slideDown();
                $option_tree_ui_radio_images_eq3.slideDown();
                break;
            default :
                $option_tree_ui_radio_images_eq0.slideDown();
                $option_tree_ui_radio_images_eq1.slideDown();
                $option_tree_ui_radio_images_eq2.slideDown();
                $option_tree_ui_radio_images_eq3.slideDown();
                break;
        }
    });

    var footervalue =  $nightclub_footer_columns.attr('value');

    switch (footervalue){
        case'1':
            $option_tree_ui_radio_images_eq0.slideDown();
            $option_tree_ui_radio_images_eq1.slideUp();
            $option_tree_ui_radio_images_eq2.slideUp();
            $option_tree_ui_radio_images_eq3.slideUp();
            break;
        case'2':
            $option_tree_ui_radio_images_eq0.slideDown();
            $option_tree_ui_radio_images_eq1.slideDown();
            $option_tree_ui_radio_images_eq2.slideUp();
            $option_tree_ui_radio_images_eq3.slideUp();
            break;
        case'3':
            $option_tree_ui_radio_images_eq0.slideDown();
            $option_tree_ui_radio_images_eq1.slideDown();
            $option_tree_ui_radio_images_eq2.slideDown();
            $option_tree_ui_radio_images_eq3.slideUp();
            break;
        case'4':
            $option_tree_ui_radio_images_eq0.slideDown();
            $option_tree_ui_radio_images_eq1.slideDown();
            $option_tree_ui_radio_images_eq2.slideDown();
            $option_tree_ui_radio_images_eq3.slideDown();
            break;
        default :
            $option_tree_ui_radio_images_eq0.slideDown();
            $option_tree_ui_radio_images_eq1.slideDown();
            $option_tree_ui_radio_images_eq2.slideDown();
            $option_tree_ui_radio_images_eq3.slideDown();
            break;
    }
    /* End footer */

    /*   Theme Color   */
    var $themecolor                 = jQuery('#nightclub_TZTypecolor'),
        $nightclub_TZThemecustom    =   jQuery('#setting_nightclub_TZThemecustom'),
        $nightclub_TZThemecolor     =   jQuery('#setting_nightclub_TZThemecolor');

    switch ( $themecolor.val() ){
        case '1':
            $nightclub_TZThemecustom.slideDown();
            $nightclub_TZThemecolor.slideUp();
            break;
        default :
            $nightclub_TZThemecustom.slideUp();
            $nightclub_TZThemecolor.slideDown();
            break;

    }

    $themecolor.change(function(){

        var $themecolor2 = jQuery(this).val();
        switch ($themecolor2){
            case '1':
                $nightclub_TZThemecustom.slideDown();
                $nightclub_TZThemecolor.slideUp();
                break;
            default :
                $nightclub_TZThemecustom.slideUp();
                $nightclub_TZThemecolor.slideDown();
                break;

        }
    });
    /* End Theme Color   */

});



// Background Type Event

var $nightclub_background_type  =   jQuery('#nightclub_background_type');

$nightclub_background_type.on('change', function () {
    "use strict";

    var value = jQuery(this).val();
    if (String(value) === 'none') {
        jQuery('#setting_nightclub_background_pattern, ' +
            '#setting_nightclub_background_single_image').slideUp();
        jQuery('#setting_nightclub_TZBackgroundColor').slideDown();
    }else if (String(value) === 'pattern') {
        jQuery('#setting_nightclub_background_pattern').slideDown();
        jQuery('#setting_nightclub_background_single_image').slideUp();
        jQuery('#setting_nightclub_TZBackgroundColor').slideUp();
    }else {
        jQuery('#setting_nightclub_background_pattern').slideUp();
        jQuery('#setting_nightclub_background_single_image').slideDown();
        jQuery('#setting_nightclub_TZBackgroundColor').slideUp();
    }
});

//new
var $nightclub_homepage_menu  =   jQuery('#nightclub_TzGlobalOption_pagination_home_page_check');

$nightclub_homepage_menu.on('change', function () {
    "use strict";

    var value = jQuery(this).val();
    if (String(value) === '1') {
        jQuery('#setting_nightclub_page_check_menu').slideDown();
        jQuery('#setting_nightclub_TzGlobalOption_pagination_home_page_data_input').slideUp();
    }else{
        jQuery('#setting_nightclub_page_check_menu').slideUp();
        jQuery('#setting_nightclub_TzGlobalOption_pagination_home_page_data_input').slideDown();
    }
});

var background_type = $nightclub_background_type.val();
if (String(background_type) === 'none') {
    jQuery('#setting_nightclub_background_pattern, ' +
        '#setting_nightclub_background_single_image').slideUp();
    jQuery('#setting_nightclub_TZBackgroundColor').slideDown();
}else if (String(background_type) === 'pattern') {
    jQuery('#setting_nightclub_background_pattern').slideDown();
    jQuery('#setting_nightclub_background_single_image').slideUp();
} else {
    jQuery('#setting_nightclub_background_pattern').slideUp();
    jQuery('#setting_nightclub_background_single_image').slideDown();

}

// Background Pattern Preview
jQuery('#setting_nightclub_background_pattern .background_pattern').on('click', function () {
    "use strict";
    var $wpcontent  =   jQuery('#wpcontent');
    if ( $wpcontent.length > 0 ) {
        $wpcontent.css('background', 'url("' + jQuery(this).attr('src') + '") repeat');
    }
});