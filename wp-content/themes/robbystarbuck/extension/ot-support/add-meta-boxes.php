<?php
/**
 * Initialize the meta boxes.
 */

add_action( 'admin_init', 'tz_1040nightclub_custom_meta_boxes');

/*
 * Methor add meta boxes for custom post type
 */
function tz_1040nightclub_custom_meta_boxes(){

    /**
     * Create a custom meta boxes array that we pass to
     * the OptionTree Meta Box API Class.
     */

    $tz_1040nightclub_post_meta_box         =   array(
        'id'          =>  'post_meta_box',
        'title'       =>  esc_html__('Post Option', '1040nightclub'),
        'desc'        =>  '',
        'pages'       => array( 'post'),
        'context'     => 'normal',
        'priority'    => 'high',
        'fields'      => array(

            array(
                'label'     => esc_html__('Image on Category Page', '1040nightclub'),
                'id'        => 'nightclub_post_image_cat',
                'type'      => 'upload',
                'desc'      => ''
            ),
            array(
                'label'     =>  esc_html__('Post Type', '1040nightclub'),
                'id'        =>  'nightclub_portfolio_type',
                'type'      =>  'select',
                'desc'      =>  esc_html__('Option type Post', '1040nightclub'),
                'std'       =>  'none',
                'rows'      => '',
                'post_type' => '',
                'taxonomy'  => '',
                'class'     => '',
                'choices'   =>  array(
                    array(
                        'value' => 'none',
                        'label' => esc_html__('None', '1040nightclub')
                    ),
                    array(
                        'value' => 'video',
                        'label' => esc_html__('Video', '1040nightclub')
                    ),
                ),
            ),
            array(
                'label'     => esc_html__('Video MP4', '1040nightclub'),
                'id'        => 'nightclub_portfolio_video_mp4',
                'type'      => 'upload',
                'desc'      => '',
                'std'       => '',
                'rows'      => '',
                'post_type' => '',
                'taxonomy'  => ''
            ),
            array(
                'label'     => esc_html__('Video OGV', '1040nightclub'),
                'id'        => 'nightclub_portfolio_video_ogv',
                'type'      => 'upload',
                'desc'      => '',
                'std'       => '',
                'rows'      => '',
                'post_type' => '',
                'taxonomy'  => ''
            ),
            array(
                'label'     => esc_html__('Video WEBM', '1040nightclub'),
                'id'        => 'nightclub_portfolio_video_webm',
                'type'      => 'upload',
                'desc'      => '',
                'std'       => '',
                'rows'      => '',
                'post_type' => '',
                'taxonomy'  => ''
            ),
            array(
                'label'     => esc_html__('Image', '1040nightclub'),
                'id'        => 'nightclub_portfolio_image',
                'type'      => 'upload',
                'desc'      => esc_html__('Use of blog carousel element','1040nightclub')
            ),
            array(
                'label'     => esc_html__('Image on Single Post', '1040nightclub'),
                'id'        => 'nightclub_post_image_single',
                'type'      => 'upload',
                'desc'      => ''
            ),
        )
    );

    $tz_1040nightclub_tribe_events_meta_box =   array(
        'id'          =>  'tribe_events_box',
        'title'       =>  esc_html__('Events Calendar Option', '1040nightclub'),
        'desc'        =>  '',
        'pages'       => array( 'tribe_events'),
        'context'     => 'normal',
        'priority'    => 'high',
        'fields'      =>  array(
            array(
                'label'     => esc_html__('Image', '1040nightclub'),
                'id'        => 'nightclub_tribe_events_image',
                'type'      => 'upload',
                'desc'      => ''
            ),
        )
    );

    $tz_1040nightclub_page_home_box         =   array(
        'id'        =>  'page_home_meta_box',
        'title'     =>  esc_html__('Home Page Option', '1040nightclub'),
        'desc'      =>  '',
        'pages'     => array( 'page'),
        'context'   => 'normal',
        'priority'  => 'high',
        'fields'    =>  array(
            array(
                'label'     =>  esc_html__('Show Or Hide Background Image', '1040nightclub'),
                'id'        =>  'nightclub_hide_show_bk_image',
                'type'      =>  'select',
                'std'       =>  0,
                'choices'   =>  array(
                    array(
                        'value' => 0,
                        'label' => esc_html__('Hide', '1040nightclub')
                    ),
                    array(
                        'value' => 1,
                        'label' => esc_html__('Show', '1040nightclub')
                    ),
                ),

            ),
            array(
                'label'     => esc_html__('background Image', '1040nightclub'),
                'id'        => 'nightclub_bk_image_page_home',
                'type'      => 'upload',
                'desc'      => ''
            ),
        )
    );

    /**
     * Register our meta boxes using the
     * ot_register_meta_box() function.
     */
    ot_register_meta_box( $tz_1040nightclub_post_meta_box );

    ot_register_meta_box( $tz_1040nightclub_tribe_events_meta_box );

    ot_register_meta_box( $tz_1040nightclub_page_home_box );

}
?>