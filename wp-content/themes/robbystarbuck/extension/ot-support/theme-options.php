<?php


update_option( 'ot_hide_cleanup', 1 );
/*
 * Initialize the options before anything else.
 */

/**
 * Optional: set 'ot_show_pages' filter to false.
 * This will hide the settings & documentation pages.
 */
add_filter( 'ot_show_pages', '__return_false' );

add_action('admin_init','tz_1040nightclub_theme_options',1);

/*
 * Build the custom settings & update OptionTree.
*/

function tz_1040nightclub_theme_options() {

    /**
     * Get a copy of the saved settings array.
     */
    $tz_1040nightclub_saved_settings = get_option('option_tree_settings', array());

    $tz_1040nightclub_font_option       =   array(
        array(
            'value'  =>  'ABeeZee',
            'label'  =>  esc_html__('ABeeZee','1040nightclub'),
        ),
        array(
            'value'  =>  'Abel',
            'label'  =>  esc_html__('Abel','1040nightclub'),
        ),
        array(
            'value'  =>  'Abril Fatface',
            'label'  =>  esc_html__('Abril Fatface','1040nightclub'),
        ),
        array(
            'value'  =>  'Aclonica',
            'label'  =>  esc_html__('Aclonica','1040nightclub'),
        ),
        array(
            'value'  =>  'Acme',
            'label'  =>  esc_html__('Acme','1040nightclub'),
        ),
        array(
            'value'  =>  'Actor',
            'label'  =>  esc_html__('Actor','1040nightclub'),
        ),
        array(
            'value'  =>  'Adamina',
            'label'  =>  esc_html__('Adamina','1040nightclub'),
        ),
        array(
            'value'  =>  'Advent Pro',
            'label'  =>  esc_html__('Advent Pro','1040nightclub'),
        ),
        array(
            'value'  =>  'Aguafina Script',
            'label'  =>  esc_html__('Aguafina Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Akronim',
            'label'  =>  esc_html__('Akronim','1040nightclub'),
        ),
        array(
            'value'  =>  'Aladin',
            'label'  =>  esc_html__('Aladin','1040nightclub'),
        ),
        array(
            'value'  =>  'Aldrich',
            'label'  =>  esc_html__('Aldrich','1040nightclub'),
        ),
        array(
            'value'  =>  'Alef',
            'label'  =>  esc_html__('Alef','1040nightclub'),
        ),
        array(
            'value'  =>  'Alegreya',
            'label'  =>  esc_html__('Alegreya','1040nightclub'),
        ),
        array(
            'value'  =>  'Alegreya SC',
            'label'  =>  esc_html__('Alegreya SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Alegreya Sans',
            'label'  =>  esc_html__('Alegreya Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Alegreya Sans SC',
            'label'  =>  esc_html__('Alegreya Sans SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Alex Brush',
            'label'  =>  esc_html__('Alex Brush','1040nightclub'),
        ),
        array(
            'value'  =>  'Alfa Slab One',
            'label'  =>  esc_html__('Alfa Slab One','1040nightclub'),
        ),
        array(
            'value'  =>  'Alice',
            'label'  =>  esc_html__('Alice','1040nightclub'),
        ),
        array(
            'value'  =>  'Alike',
            'label'  =>  esc_html__('Alike','1040nightclub'),
        ),
        array(
            'value'  =>  'Alike Angular',
            'label'  =>  esc_html__('Alike Angular','1040nightclub'),
        ),
        array(
            'value'  =>  'Allan',
            'label'  =>  esc_html__('Allan','1040nightclub'),
        ),
        array(
            'value'  =>  'Allerta',
            'label'  =>  esc_html__('Allerta','1040nightclub'),
        ),
        array(
            'value'  =>  'Allerta Stencil',
            'label'  =>  esc_html__('Allerta Stencil','1040nightclub'),
        ),
        array(
            'value'  =>  'Allura',
            'label'  =>  esc_html__('Allura','1040nightclub'),
        ),
        array(
            'value'  =>  'Almendra',
            'label'  =>  esc_html__('Almendra','1040nightclub'),
        ),
        array(
            'value'  =>  'Almendra Display',
            'label'  =>  esc_html__('Almendra Display','1040nightclub'),
        ),
        array(
            'value'  =>  'Almendra SC',
            'label'  =>  esc_html__('Almendra SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Amarante',
            'label'  =>  esc_html__('Amarante','1040nightclub'),
        ),
        array(
            'value'  =>  'Amaranth',
            'label'  =>  esc_html__('Amaranth','1040nightclub'),
        ),
        array(
            'value'  =>  'Amatic SC',
            'label'  =>  esc_html__('Amatic SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Amethysta',
            'label'  =>  esc_html__('Amethysta','1040nightclub'),
        ),
        array(
            'value'  =>  'Anaheim',
            'label'  =>  esc_html__('Anaheim','1040nightclub'),
        ),
        array(
            'value'  =>  'Andada',
            'label'  =>  esc_html__('Andada','1040nightclub'),
        ),
        array(
            'value'  =>  'Andika',
            'label'  =>  esc_html__('Andika','1040nightclub'),
        ),
        array(
            'value'  =>  'Angkor',
            'label'  =>  esc_html__('Angkor','1040nightclub'),
        ),
        array(
            'value'  =>  'Annie Use Your Telescope',
            'label'  =>  esc_html__('Annie Use Your Telescope','1040nightclub'),
        ),
        array(
            'value'  =>  'Anonymous Pro',
            'label'  =>  esc_html__('Anonymous Pro','1040nightclub'),
        ),
        array(
            'value'  =>  'Antic',
            'label'  =>  esc_html__('Antic','1040nightclub'),
        ),
        array(
            'value'  =>  'Antic Didone',
            'label'  =>  esc_html__('Antic Didone','1040nightclub'),
        ),
        array(
            'value'  =>  'Antic Slab',
            'label'  =>  esc_html__('Antic Slab','1040nightclub'),
        ),
        array(
            'value'  =>  'Anton',
            'label'  =>  esc_html__('Anton','1040nightclub'),
        ),
        array(
            'value'  =>  'Arapey',
            'label'  =>  esc_html__('Arapey','1040nightclub'),
        ),
        array(
            'value'  =>  'Arbutus',
            'label'  =>  esc_html__('Arbutus','1040nightclub'),
        ),
        array(
            'value'  =>  'Arbutus Slab',
            'label'  =>  esc_html__('Arbutus Slab','1040nightclub'),
        ),
        array(
            'value'  =>  'Architects Daughter',
            'label'  =>  esc_html__('Architects Daughter','1040nightclub'),
        ),
        array(
            'value'  =>  'Archivo Black',
            'label'  =>  esc_html__('Archivo Black','1040nightclub'),
        ),
        array(
            'value'  =>  'Archivo Narrow',
            'label'  =>  esc_html__('Archivo Narrow','1040nightclub'),
        ),
        array(
            'value'  =>  'Arimo',
            'label'  =>  esc_html__('Arimo','1040nightclub'),
        ),
        array(
            'value'  =>  'Arizonia',
            'label'  =>  esc_html__('Arizonia','1040nightclub'),
        ),
        array(
            'value'  =>  'Armata',
            'label'  =>  esc_html__('Armata','1040nightclub'),
        ),
        array(
            'value'  =>  'Artifika',
            'label'  =>  esc_html__('Artifika','1040nightclub'),
        ),
        array(
            'value'  =>  'Arvo',
            'label'  =>  esc_html__('Arvo','1040nightclub'),
        ),
        array(
            'value'  =>  'Asap',
            'label'  =>  esc_html__('Asap','1040nightclub'),
        ),
        array(
            'value'  =>  'Asset',
            'label'  =>  esc_html__('Asset','1040nightclub'),
        ),
        array(
            'value'  =>  'Astloch',
            'label'  =>  esc_html__('Astloch','1040nightclub'),
        ),
        array(
            'value'  =>  'Asul',
            'label'  =>  esc_html__('Asul','1040nightclub'),
        ),
        array(
            'value'  =>  'Atomic Age',
            'label'  =>  esc_html__('Atomic Age','1040nightclub'),
        ),
        array(
            'value'  =>  'Aubrey',
            'label'  =>  esc_html__('Aubrey','1040nightclub'),
        ),
        array(
            'value'  =>  'Audiowide',
            'label'  =>  esc_html__('Audiowide','1040nightclub'),
        ),
        array(
            'value'  =>  'Autour One',
            'label'  =>  esc_html__('Autour One','1040nightclub'),
        ),
        array(
            'value'  =>  'Average',
            'label'  =>  esc_html__('Average','1040nightclub'),
        ),
        array(
            'value'  =>  'Average Sans',
            'label'  =>  esc_html__('Average Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Averia Gruesa Libre',
            'label'  =>  esc_html__('Averia Gruesa Libre','1040nightclub'),
        ),
        array(
            'value'  =>  'Averia Libre',
            'label'  =>  esc_html__('Averia Libre','1040nightclub'),
        ),
        array(
            'value'  =>  'Averia Sans Libre',
            'label'  =>  esc_html__('Averia Sans Libre','1040nightclub'),
        ),
        array(
            'value'  =>  'Averia Serif Libre',
            'label'  =>  esc_html__('Averia Serif Libre','1040nightclub'),
        ),
        array(
            'value'  =>  'Bad Script',
            'label'  =>  esc_html__('Bad Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Balthazar',
            'label'  =>  esc_html__('Balthazar','1040nightclub'),
        ),
        array(
            'value'  =>  'Bangers',
            'label'  =>  esc_html__('Bangers','1040nightclub'),
        ),
        array(
            'value'  =>  'Basic',
            'label'  =>  esc_html__('Basic','1040nightclub'),
        ),
        array(
            'value'  =>  'Battambang',
            'label'  =>  esc_html__('Battambang','1040nightclub'),
        ),
        array(
            'value'  =>  'Baumans',
            'label'  =>  esc_html__('Baumans','1040nightclub'),
        ),
        array(
            'value'  =>  'Bayont',
            'label'  =>  esc_html__('Bayon','1040nightclub'),
        ),
        array(
            'value'  =>  'Belgrano',
            'label'  =>  esc_html__('Belgrano','1040nightclub'),
        ),
        array(
            'value'  =>  'Belleza',
            'label'  =>  esc_html__('Belleza','1040nightclub'),
        ),
        array(
            'value'  =>  'BenchNine',
            'label'  =>  esc_html__('BenchNine','1040nightclub'),
        ),
        array(
            'value'  =>  'Bentham',
            'label'  =>  esc_html__('Bentham','1040nightclub'),
        ),
        array(
            'value'  =>  'Berkshire Swash',
            'label'  =>  esc_html__('Berkshire Swash','1040nightclub'),
        ),
        array(
            'value'  =>  'Bevan',
            'label'  =>  esc_html__('Bevan','1040nightclub'),
        ),
        array(
            'value'  =>  'Bigelow Rules',
            'label'  =>  esc_html__('Bigelow Rules','1040nightclub'),
        ),
        array(
            'value'  =>  'Bigshot One',
            'label'  =>  esc_html__('Bigshot One','1040nightclub'),
        ),
        array(
            'value'  =>  'Bilbo',
            'label'  =>  esc_html__('Bilbo','1040nightclub'),
        ),
        array(
            'value'  =>  'Bilbo Swash Caps',
            'label'  =>  esc_html__('Bilbo Swash Caps','1040nightclub'),
        ),
        array(
            'value'  =>  'Bitter',
            'label'  =>  esc_html__('Bitter','1040nightclub'),
        ),
        array(
            'value'  =>  'Black Ops One',
            'label'  =>  esc_html__('Black Ops One','1040nightclub'),
        ),
        array(
            'value'  =>  'Bokor',
            'label'  =>  esc_html__('Bokor','1040nightclub'),
        ),
        array(
            'value'  =>  'Bonbon',
            'label'  =>  esc_html__('Bonbon','1040nightclub'),
        ),
        array(
            'value'  =>  'Boogaloo',
            'label'  =>  esc_html__('Boogaloo','1040nightclub'),
        ),
        array(
            'value'  =>  'Bowlby One',
            'label'  =>  esc_html__('Bowlby One','1040nightclub'),
        ),
        array(
            'value'  =>  'Bowlby One SC',
            'label'  =>  esc_html__('Bowlby One SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Brawler',
            'label'  =>  esc_html__('Brawler','1040nightclub'),
        ),
        array(
            'value'  =>  'Bree Serif',
            'label'  =>  esc_html__('Bree Serif','1040nightclub'),
        ),
        array(
            'value'  =>  'Bubblegum Sans',
            'label'  =>  esc_html__('Bubblegum Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Bubbler One',
            'label'  =>  esc_html__('Bubbler One','1040nightclub'),
        ),
        array(
            'value'  =>  'Buda',
            'label'  =>  esc_html__('Buda','1040nightclub'),
        ),
        array(
            'value'  =>  'Buenard',
            'label'  =>  esc_html__('Buenard','1040nightclub'),
        ),
        array(
            'value'  =>  'Butcherman',
            'label'  =>  esc_html__('Butcherman','1040nightclub'),
        ),
        array(
            'value'  =>  'Butterfly Kids',
            'label'  =>  esc_html__('Butterfly Kids','1040nightclub'),
        ),
        array(
            'value'  =>  'Cabin',
            'label'  =>  esc_html__('Cabin','1040nightclub'),
        ),
        array(
            'value'  =>  'Cabin Condensed',
            'label'  =>  esc_html__('Cabin Condensed','1040nightclub'),
        ),
        array(
            'value'  =>  'Cabin Sketch',
            'label'  =>  esc_html__('Cabin Sketch','1040nightclub'),
        ),
        array(
            'value'  =>  'Caesar Dressing',
            'label'  =>  esc_html__('Caesar Dressing','1040nightclub'),
        ),
        array(
            'value'  =>  'Cagliostro',
            'label'  =>  esc_html__('Cagliostro','1040nightclub'),
        ),
        array(
            'value'  =>  'Calligraffitti',
            'label'  =>  esc_html__('Calligraffitti','1040nightclub'),
        ),
        array(
            'value'  =>  'Cambo',
            'label'  =>  esc_html__('Cambo','1040nightclub'),
        ),
        array(
            'value'  =>  'Candal',
            'label'  =>  esc_html__('Candal','1040nightclub'),
        ),
        array(
            'value'  =>  'Cantarell',
            'label'  =>  esc_html__('Cantarell','1040nightclub'),
        ),
        array(
            'value'  =>  'Cantata One',
            'label'  =>  esc_html__('Cantata One','1040nightclub'),
        ),
        array(
            'value'  =>  'Cantora One',
            'label'  =>  esc_html__('Cantora One','1040nightclub'),
        ),
        array(
            'value'  =>  'Capriola',
            'label'  =>  esc_html__('Capriola','1040nightclub'),
        ),
        array(
            'value'  =>  'Cardo',
            'label'  =>  esc_html__('Cardo','1040nightclub'),
        ),
        array(
            'value'  =>  'Carme',
            'label'  =>  esc_html__('Carme','1040nightclub'),
        ),
        array(
            'value'  =>  'Carrois Gothic',
            'label'  =>  esc_html__('Carrois Gothic','1040nightclub'),
        ),
        array(
            'value'  =>  'Carrois Gothic SC',
            'label'  =>  esc_html__('Carrois Gothic SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Carter One',
            'label'  =>  esc_html__('Carter One','1040nightclub'),
        ),
        array(
            'value'  =>  'Caudex',
            'label'  =>  esc_html__('Caudex','1040nightclub'),
        ),
        array(
            'value'  =>  'Cedarville Cursive',
            'label'  =>  esc_html__('Cedarville Cursive','1040nightclub'),
        ),
        array(
            'value'  =>  'Ceviche One',
            'label'  =>  esc_html__('Ceviche One','1040nightclub'),
        ),
        array(
            'value'  =>  'Changa One',
            'label'  =>  esc_html__('Changa One','1040nightclub'),
        ),
        array(
            'value'  =>  'Chango',
            'label'  =>  esc_html__('Chango','1040nightclub'),
        ),
        array(
            'value'  =>  'Chau Philomene One',
            'label'  =>  esc_html__('Chau Philomene One','1040nightclub'),
        ),
        array(
            'value'  =>  'Chela One',
            'label'  =>  esc_html__('Chela One','1040nightclub'),
        ),
        array(
            'value'  =>  'Chelsea Market',
            'label'  =>  esc_html__('Chelsea Market','1040nightclub'),
        ),
        array(
            'value'  =>  'Chenla',
            'label'  =>  esc_html__('Chenla','1040nightclub'),
        ),
        array(
            'value'  =>  'Cherry Cream Soda',
            'label'  =>  esc_html__('Cherry Cream Soda','1040nightclub'),
        ),
        array(
            'value'  =>  'Cherry Swash',
            'label'  =>  esc_html__('Cherry Swash','1040nightclub'),
        ),
        array(
            'value'  =>  'Chewy',
            'label'  =>  esc_html__('Chewy','1040nightclub'),
        ),
        array(
            'value'  =>  'Chicle',
            'label'  =>  esc_html__('Chicle','1040nightclub'),
        ),
        array(
            'value'  =>  'Chivo',
            'label'  =>  esc_html__('Chivo','1040nightclub'),
        ),
        array(
            'value'  =>  'Cinzel',
            'label'  =>  esc_html__('Cinzel','1040nightclub'),
        ),
        array(
            'value'  =>  'Cinzel Decorative',
            'label'  =>  esc_html__('Cinzel Decorative','1040nightclub'),
        ),
        array(
            'value'  =>  'Clicker Script',
            'label'  =>  esc_html__('Clicker Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Coda',
            'label'  =>  esc_html__('Coda','1040nightclub'),
        ),
        array(
            'value'  =>  'Coda Caption',
            'label'  =>  esc_html__('Coda Caption','1040nightclub'),
        ),
        array(
            'value'  =>  'Codystar',
            'label'  =>  esc_html__('Codystar','1040nightclub'),
        ),
        array(
            'value'  =>  'Combo',
            'label'  =>  esc_html__('Combo','1040nightclub'),
        ),
        array(
            'value'  =>  'Comfortaa',
            'label'  =>  esc_html__('Comfortaa','1040nightclub'),
        ),
        array(
            'value'  =>  'Coming Soon',
            'label'  =>  esc_html__('Coming Soon','1040nightclub'),
        ),
        array(
            'value'  =>  'Concert One',
            'label'  =>  esc_html__('Concert One','1040nightclub'),
        ),
        array(
            'value'  =>  'Condiment',
            'label'  =>  esc_html__('Condiment','1040nightclub'),
        ),
        array(
            'value'  =>  'Content',
            'label'  =>  esc_html__('Content','1040nightclub'),
        ),
        array(
            'value'  =>  'Contrail One',
            'label'  =>  esc_html__('Contrail One','1040nightclub'),
        ),
        array(
            'value'  =>  'Convergence',
            'label'  =>  esc_html__('Convergence','1040nightclub'),
        ),
        array(
            'value'  =>  'Cookie',
            'label'  =>  esc_html__('Cookie','1040nightclub'),
        ),
        array(
            'value'  =>  'Copse',
            'label'  =>  esc_html__('Copse','1040nightclub'),
        ),
        array(
            'value'  =>  'Corben',
            'label'  =>  esc_html__('Corben','1040nightclub'),
        ),
        array(
            'value'  =>  'Courgette',
            'label'  =>  esc_html__('Courgette','1040nightclub'),
        ),
        array(
            'value'  =>  'Cousine',
            'label'  =>  esc_html__('Cousine','1040nightclub'),
        ),
        array(
            'value'  =>  'Coustard',
            'label'  =>  esc_html__('Coustard','1040nightclub'),
        ),
        array(
            'value'  =>  'Covered By Your Grace',
            'label'  =>  esc_html__('Covered By Your Grace','1040nightclub'),
        ),
        array(
            'value'  =>  'Crafty Girls',
            'label'  =>  esc_html__('Crafty Girls','1040nightclub'),
        ),
        array(
            'value'  =>  'Creepster',
            'label'  =>  esc_html__('Creepster','1040nightclub'),
        ),
        array(
            'value'  =>  'Crete Round',
            'label'  =>  esc_html__('Crete Round','1040nightclub'),
        ),
        array(
            'value'  =>  'Crimson Text',
            'label'  =>  esc_html__('Crimson Text','1040nightclub'),
        ),
        array(
            'value'  =>  'Croissant One',
            'label'  =>  esc_html__('Croissant One','1040nightclub'),
        ),
        array(
            'value'  =>  'Crushed',
            'label'  =>  esc_html__('Crushed','1040nightclub'),
        ),
        array(
            'value'  =>  'Cuprum',
            'label'  =>  esc_html__('Cuprum','1040nightclub'),
        ),
        array(
            'value'  =>  'Cutive',
            'label'  =>  esc_html__('Cutive','1040nightclub'),
        ),
        array(
            'value'  =>  'Cutive Mono',
            'label'  =>  esc_html__('Cutive Mono','1040nightclub'),
        ),
        array(
            'value'  =>  'Damion',
            'label'  =>  esc_html__('Damion','1040nightclub'),
        ),
        array(
            'value'  =>  'Dancing Script',
            'label'  =>  esc_html__('Dancing Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Dangrek',
            'label'  =>  esc_html__('Dangrek','1040nightclub'),
        ),
        array(
            'value'  =>  'Dawning of a New Day',
            'label'  =>  esc_html__('Dawning of a New Day','1040nightclub'),
        ),
        array(
            'value'  =>  'Days One',
            'label'  =>  esc_html__('Days One','1040nightclub'),
        ),
        array(
            'value'  =>  'Delius',
            'label'  =>  esc_html__('Delius','1040nightclub'),
        ),
        array(
            'value'  =>  'Delius Swash Caps',
            'label'  =>  esc_html__('Delius Swash Caps','1040nightclub'),
        ),
        array(
            'value'  =>  'Delius Unicase',
            'label'  =>  esc_html__('Delius Unicase','1040nightclub'),
        ),
        array(
            'value'  =>  'Denk One',
            'label'  =>  esc_html__('Denk One','1040nightclub'),
        ),
        array(
            'value'  =>  'Devonshire',
            'label'  =>  esc_html__('Devonshire','1040nightclub'),
        ),
        array(
            'value'  =>  'Didact Gothic',
            'label'  =>  esc_html__('Didact Gothic','1040nightclub'),
        ),
        array(
            'value'  =>  'Diplomata',
            'label'  =>  esc_html__('Diplomata','1040nightclub'),
        ),
        array(
            'value'  =>  'Diplomata SC',
            'label'  =>  esc_html__('Diplomata SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Domine',
            'label'  =>  esc_html__('Domine','1040nightclub'),
        ),
        array(
            'value'  =>  'Donegal One',
            'label'  =>  esc_html__('Donegal One','1040nightclub'),
        ),
        array(
            'value'  =>  'Doppio One',
            'label'  =>  esc_html__('Doppio One','1040nightclub'),
        ),
        array(
            'value'  =>  'Dorsa',
            'label'  =>  esc_html__('Dorsa','1040nightclub'),
        ),
        array(
            'value'  =>  'Dosis',
            'label'  =>  esc_html__('Dosis','1040nightclub'),
        ),
        array(
            'value'  =>  'Dr Sugiyama',
            'label'  =>  esc_html__('Dr Sugiyama','1040nightclub'),
        ),
        array(
            'value'  =>  'Droid Sans',
            'label'  =>  esc_html__('Droid Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Droid Sans Mono',
            'label'  =>  esc_html__('Droid Sans Mono','1040nightclub'),
        ),
        array(
            'value'  =>  'Droid Serif',
            'label'  =>  esc_html__('Droid Serif','1040nightclub'),
        ),
        array(
            'value'  =>  'Duru Sans',
            'label'  =>  esc_html__('Duru Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Dynalight',
            'label'  =>  esc_html__('Dynalight','1040nightclub'),
        ),
        array(
            'value'  =>  'EB Garamond',
            'label'  =>  esc_html__('EB Garamond','1040nightclub'),
        ),
        array(
            'value'  =>  'Eagle Lake',
            'label'  =>  esc_html__('Eagle Lake','1040nightclub'),
        ),
        array(
            'value'  =>  'Eater',
            'label'  =>  esc_html__('Eater','1040nightclub'),
        ),
        array(
            'value'  =>  'Economica',
            'label'  =>  esc_html__('Economica','1040nightclub'),
        ),
        array(
            'value'  =>  'Ek Mukta',
            'label'  =>  esc_html__('Ek Mukta','1040nightclub'),
        ),
        array(
            'value'  =>  'Electrolize',
            'label'  =>  esc_html__('Electrolize','1040nightclub'),
        ),
        array(
            'value'  =>  'Elsie',
            'label'  =>  esc_html__('Elsie','1040nightclub'),
        ),
        array(
            'value'  =>  'Elsie Swash Caps',
            'label'  =>  esc_html__('Elsie Swash Caps','1040nightclub'),
        ),
        array(
            'value'  =>  'Emblema One',
            'label'  =>  esc_html__('Emblema One','1040nightclub'),
        ),
        array(
            'value'  =>  'Emilys Candy',
            'label'  =>  esc_html__('Emilys Candy','1040nightclub'),
        ),
        array(
            'value'  =>  'Engagement',
            'label'  =>  esc_html__('Engagement','1040nightclub'),
        ),
        array(
            'value'  =>  'Englebert',
            'label'  =>  esc_html__('Englebert','1040nightclub'),
        ),
        array(
            'value'  =>  'Enriqueta',
            'label'  =>  esc_html__('Enriqueta','1040nightclub'),
        ),
        array(
            'value'  =>  'Erica One',
            'label'  =>  esc_html__('Erica One','1040nightclub'),
        ),
        array(
            'value'  =>  'Esteban',
            'label'  =>  esc_html__('Esteban','1040nightclub'),
        ),
        array(
            'value'  =>  'Euphoria Script',
            'label'  =>  esc_html__('Euphoria Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Ewert',
            'label'  =>  esc_html__('Ewert','1040nightclub'),
        ),
        array(
            'value'  =>  'Exo',
            'label'  =>  esc_html__('Exo','1040nightclub'),
        ),
        array(
            'value'  =>  'Exo 2',
            'label'  =>  esc_html__('Exo 2','1040nightclub'),
        ),
        array(
            'value'  =>  'Expletus Sans',
            'label'  =>  esc_html__('Expletus Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Fanwood Text',
            'label'  =>  esc_html__('Fanwood Text','1040nightclub'),
        ),
        array(
            'value'  =>  'Fascinate',
            'label'  =>  esc_html__('Fascinate','1040nightclub'),
        ),
        array(
            'value'  =>  'Fascinate Inline',
            'label'  =>  esc_html__('Fascinate Inline','1040nightclub'),
        ),
        array(
            'value'  =>  'Faster One',
            'label'  =>  esc_html__('Faster One','1040nightclub'),
        ),
        array(
            'value'  =>  'Fasthand',
            'label'  =>  esc_html__('Fasthand','1040nightclub'),
        ),
        array(
            'value'  =>  'Fauna One',
            'label'  =>  esc_html__('Fauna One','1040nightclub'),
        ),
        array(
            'value'  =>  'Federant',
            'label'  =>  esc_html__('Federant','1040nightclub'),
        ),
        array(
            'value'  =>  'Federo',
            'label'  =>  esc_html__('Federo','1040nightclub'),
        ),
        array(
            'value'  =>  'Felipa',
            'label'  =>  esc_html__('Felipa','1040nightclub'),
        ),
        array(
            'value'  =>  'Fenix',
            'label'  =>  esc_html__('Fenix','1040nightclub'),
        ),
        array(
            'value'  =>  'Finger Paint',
            'label'  =>  esc_html__('Finger Paint','1040nightclub'),
        ),
        array(
            'value'  =>  'Fira Mono',
            'label'  =>  esc_html__('Fira Mono','1040nightclub'),
        ),
        array(
            'value'  =>  'Fira Sans',
            'label'  =>  esc_html__('Fira Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Fjalla One',
            'label'  =>  esc_html__('Fjalla One','1040nightclub'),
        ),
        array(
            'value'  =>  'Fjord One',
            'label'  =>  esc_html__('Fjord One','1040nightclub'),
        ),
        array(
            'value'  =>  'Flamenco',
            'label'  =>  esc_html__('Flamenco','1040nightclub'),
        ),
        array(
            'value'  =>  'Flavors',
            'label'  =>  esc_html__('Flavors','1040nightclub'),
        ),
        array(
            'value'  =>  'Fondamento',
            'label'  =>  esc_html__('Fondamento','1040nightclub'),
        ),
        array(
            'value'  =>  'Fontdiner Swanky',
            'label'  =>  esc_html__('Fontdiner Swanky','1040nightclub'),
        ),
        array(
            'value'  =>  'Forum',
            'label'  =>  esc_html__('Forum','1040nightclub'),
        ),
        array(
            'value'  =>  'Francois One',
            'label'  =>  esc_html__('Francois One','1040nightclub'),
        ),
        array(
            'value'  =>  'Freckle Face',
            'label'  =>  esc_html__('Freckle Face','1040nightclub'),
        ),
        array(
            'value'  =>  'Fredericka the Great',
            'label'  =>  esc_html__('Fredericka the Great','1040nightclub'),
        ),
        array(
            'value'  =>  'Fredoka One',
            'label'  =>  esc_html__('Fredoka One','1040nightclub'),
        ),
        array(
            'value'  =>  'Freehand',
            'label'  =>  esc_html__('Freehand','1040nightclub'),
        ),
        array(
            'value'  =>  'Fresca',
            'label'  =>  esc_html__('Fresca','1040nightclub'),
        ),
        array(
            'value'  =>  'Frijole',
            'label'  =>  esc_html__('Frijole','1040nightclub'),
        ),
        array(
            'value'  =>  'Fruktur',
            'label'  =>  esc_html__('Fruktur','1040nightclub'),
        ),
        array(
            'value'  =>  'Fugaz One',
            'label'  =>  esc_html__('Fugaz One','1040nightclub'),
        ),
        array(
            'value'  =>  'GFS Didot',
            'label'  =>  esc_html__('GFS Didot','1040nightclub'),
        ),
        array(
            'value'  =>  'GFS Neohellenic',
            'label'  =>  esc_html__('GFS Neohellenic','1040nightclub'),
        ),
        array(
            'value'  =>  'Gabriela',
            'label'  =>  esc_html__('Gabriela','1040nightclub'),
        ),
        array(
            'value'  =>  'Gafata',
            'label'  =>  esc_html__('Gafata','1040nightclub'),
        ),
        array(
            'value'  =>  'Galdeano',
            'label'  =>  esc_html__('Galdeano','1040nightclub'),
        ),
        array(
            'value'  =>  'Galindo',
            'label'  =>  esc_html__('Galindo','1040nightclub'),
        ),
        array(
            'value'  =>  'Gentium Basic',
            'label'  =>  esc_html__('Gentium Basic','1040nightclub'),
        ),
        array(
            'value'  =>  'Gentium Book Basic',
            'label'  =>  esc_html__('Gentium Book Basic','1040nightclub'),
        ),
        array(
            'value'  =>  'Geo',
            'label'  =>  esc_html__('Geo','1040nightclub'),
        ),
        array(
            'value'  =>  'Geostar',
            'label'  =>  esc_html__('Geostar','1040nightclub'),
        ),
        array(
            'value'  =>  'Geostar Fill',
            'label'  =>  esc_html__('Geostar Fill','1040nightclub'),
        ),
        array(
            'value'  =>  'Germania One',
            'label'  =>  esc_html__('Germania One','1040nightclub'),
        ),
        array(
            'value'  =>  'Gilda Display',
            'label'  =>  esc_html__('Gilda Display','1040nightclub'),
        ),
        array(
            'value'  =>  'Give You Glory',
            'label'  =>  esc_html__('Give You Glory','1040nightclub'),
        ),
        array(
            'value'  =>  'Glass Antiqua',
            'label'  =>  esc_html__('Glass Antiqua','1040nightclub'),
        ),
        array(
            'value'  =>  'Glegoo',
            'label'  =>  esc_html__('Glegoo','1040nightclub'),
        ),
        array(
            'value'  =>  'Gloria Hallelujah',
            'label'  =>  esc_html__('Gloria Hallelujah','1040nightclub'),
        ),
        array(
            'value'  =>  'Goblin One',
            'label'  =>  esc_html__('Goblin One','1040nightclub'),
        ),
        array(
            'value'  =>  'Gochi Hand',
            'label'  =>  esc_html__('Gochi Hand','1040nightclub'),
        ),
        array(
            'value'  =>  'Gorditas',
            'label'  =>  esc_html__('Gorditas','1040nightclub'),
        ),
        array(
            'value'  =>  'Goudy Bookletter 1911',
            'label'  =>  esc_html__('Goudy Bookletter 1911','1040nightclub'),
        ),
        array(
            'value'  =>  'Graduate',
            'label'  =>  esc_html__('Graduate','1040nightclub'),
        ),
        array(
            'value'  =>  'Grand Hotel',
            'label'  =>  esc_html__('Grand Hotel','1040nightclub'),
        ),
        array(
            'value'  =>  'Gravitas One',
            'label'  =>  esc_html__('Goblin One','1040nightclub'),
        ),
        array(
            'value'  =>  'Great Vibes',
            'label'  =>  esc_html__('Great Vibes','1040nightclub'),
        ),
        array(
            'value'  =>  'Griffy',
            'label'  =>  esc_html__('Griffy','1040nightclub'),
        ),
        array(
            'value'  =>  'Gruppo',
            'label'  =>  esc_html__('Gruppo','1040nightclub'),
        ),
        array(
            'value'  =>  'Gudea',
            'label'  =>  esc_html__('Gudea','1040nightclub'),
        ),
        array(
            'value'  =>  'Habibi',
            'label'  =>  esc_html__('Habibi','1040nightclub'),
        ),
        array(
            'value'  =>  'Halant',
            'label'  =>  esc_html__('Halant','1040nightclub'),
        ),
        array(
            'value'  =>  'Hammersmith One',
            'label'  =>  esc_html__('Hammersmith One','1040nightclub'),
        ),
        array(
            'value'  =>  'Hanalei',
            'label'  =>  esc_html__('Hanalei','1040nightclub'),
        ),
        array(
            'value'  =>  'Hanalei Fill',
            'label'  =>  esc_html__('Hanalei Fill','1040nightclub'),
        ),
        array(
            'value'  =>  'Handlee',
            'label'  =>  esc_html__('Handlee','1040nightclub'),
        ),
        array(
            'value'  =>  'Hanuman',
            'label'  =>  esc_html__('Hanuman','1040nightclub'),
        ),
        array(
            'value'  =>  'Happy Monkey',
            'label'  =>  esc_html__('Happy Monkey','1040nightclub'),
        ),
        array(
            'value'  =>  'Headland One',
            'label'  =>  esc_html__('Headland One','1040nightclub'),
        ),
        array(
            'value'  =>  'Henny Penny',
            'label'  =>  esc_html__('Henny Penny','1040nightclub'),
        ),
        array(
            'value'  =>  'Herr Von Muellerhoff',
            'label'  =>  esc_html__('Herr Von Muellerhoff','1040nightclub'),
        ),
        array(
            'value'  =>  'Hind',
            'label'  =>  esc_html__('Hind','1040nightclub'),
        ),
        array(
            'value'  =>  'Holtwood One SC',
            'label'  =>  esc_html__('Holtwood One SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Homemade Apple',
            'label'  =>  esc_html__('Homemade Apple','1040nightclub'),
        ),
        array(
            'value'  =>  'Homenaje',
            'label'  =>  esc_html__('Homenaje','1040nightclub'),
        ),
        array(
            'value'  =>  'IM Fell DW Pica',
            'label'  =>  esc_html__('IM Fell DW Pica','1040nightclub'),
        ),
        array(
            'value'  =>  'IM Fell DW Pica SC',
            'label'  =>  esc_html__('IM Fell DW Pica SC','1040nightclub'),
        ),
        array(
            'value'  =>  'IM Fell Double Pica',
            'label'  =>  esc_html__('IM Fell Double Pica','1040nightclub'),
        ),
        array(
            'value'  =>  'IM Fell Double Pica SC',
            'label'  =>  esc_html__('IM Fell Double Pica SC','1040nightclub'),
        ),
        array(
            'value'  =>  'IM Fell English',
            'label'  =>  esc_html__('IM Fell English','1040nightclub'),
        ),
        array(
            'value'  =>  'IM Fell English SC',
            'label'  =>  esc_html__('IM Fell English SC','1040nightclub'),
        ),
        array(
            'value'  =>  'IM Fell French Canon',
            'label'  =>  esc_html__('IM Fell French Canon','1040nightclub'),
        ),
        array(
            'value'  =>  'IM Fell French Canon SC',
            'label'  =>  esc_html__('IM Fell French Canon SC','1040nightclub'),
        ),
        array(
            'value'  =>  'IM Fell Great Primer',
            'label'  =>  esc_html__('IM Fell Great Primer','1040nightclub'),
        ),
        array(
            'value'  =>  'IM Fell Great Primer SC',
            'label'  =>  esc_html__('IM Fell Great Primer SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Iceberg',
            'label'  =>  esc_html__('Iceberg','1040nightclub'),
        ),
        array(
            'value'  =>  'Iceland',
            'label'  =>  esc_html__('Iceland','1040nightclub'),
        ),
        array(
            'value'  =>  'Imprima',
            'label'  =>  esc_html__('Imprima','1040nightclub'),
        ),
        array(
            'value'  =>  'Inconsolata',
            'label'  =>  esc_html__('Inconsolata','1040nightclub'),
        ),
        array(
            'value'  =>  'Inder',
            'label'  =>  esc_html__('Inder','1040nightclub'),
        ),
        array(
            'value'  =>  'Indie Flower',
            'label'  =>  esc_html__('Indie Flower','1040nightclub'),
        ),
        array(
            'value'  =>  'Inika',
            'label'  =>  esc_html__('Inika','1040nightclub'),
        ),
        array(
            'value'  =>  'Irish Grover',
            'label'  =>  esc_html__('Irish Grover','1040nightclub'),
        ),
        array(
            'value'  =>  'Istok Web',
            'label'  =>  esc_html__('Istok Web','1040nightclub'),
        ),
        array(
            'value'  =>  'Italiana',
            'label'  =>  esc_html__('Italiana','1040nightclub'),
        ),
        array(
            'value'  =>  'Italianno',
            'label'  =>  esc_html__('Italianno','1040nightclub'),
        ),
        array(
            'value'  =>  'Jacques Francois',
            'label'  =>  esc_html__('Jacques Francois','1040nightclub'),
        ),
        array(
            'value'  =>  'Jacques Francois Shadow',
            'label'  =>  esc_html__('Jacques Francois Shadow','1040nightclub'),
        ),
        array(
            'value'  =>  'Jim Nightshade',
            'label'  =>  esc_html__('Jim Nightshade','1040nightclub'),
        ),
        array(
            'value'  =>  'Jockey One',
            'label'  =>  esc_html__('Jockey One','1040nightclub'),
        ),
        array(
            'value'  =>  'Jolly Lodger',
            'label'  =>  esc_html__('Jolly Lodger','1040nightclub'),
        ),
        array(
            'value'  =>  'Josefin Sans',
            'label'  =>  esc_html__('Josefin Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Josefin Slab',
            'label'  =>  esc_html__('Josefin Slab','1040nightclub'),
        ),
        array(
            'value'  =>  'Joti One',
            'label'  =>  esc_html__('Joti One','1040nightclub'),
        ),
        array(
            'value'  =>  'Judson',
            'label'  =>  esc_html__('Judson','1040nightclub'),
        ),
        array(
            'value'  =>  'Julee',
            'label'  =>  esc_html__('Julee','1040nightclub'),
        ),
        array(
            'value'  =>  'Julius Sans One',
            'label'  =>  esc_html__('Julius Sans One','1040nightclub'),
        ),
        array(
            'value'  =>  'Junge',
            'label'  =>  esc_html__('Junge','1040nightclub'),
        ),
        array(
            'value'  =>  'Jura',
            'label'  =>  esc_html__('Jura','1040nightclub'),
        ),
        array(
            'value'  =>  'Just Another Hand',
            'label'  =>  esc_html__('Just Another Hand','1040nightclub'),
        ),
        array(
            'value'  =>  'Just Me Again Down Here',
            'label'  =>  esc_html__('Just Me Again Down Here','1040nightclub'),
        ),
        array(
            'value'  =>  'Kalam',
            'label'  =>  esc_html__('Kalam','1040nightclub'),
        ),
        array(
            'value'  =>  'Kameron',
            'label'  =>  esc_html__('Kameron','1040nightclub'),
        ),
        array(
            'value'  =>  'Kantumruy',
            'label'  =>  esc_html__('Kantumruy','1040nightclub'),
        ),
        array(
            'value'  =>  'Karla',
            'label'  =>  esc_html__('Karla','1040nightclub'),
        ),
        array(
            'value'  =>  'Karma',
            'label'  =>  esc_html__('Karma','1040nightclub'),
        ),
        array(
            'value'  =>  'Kaushan Script',
            'label'  =>  esc_html__('Kaushan Scriptd','1040nightclub'),
        ),
        array(
            'value'  =>  'Kavoon',
            'label'  =>  esc_html__('Kavoon','1040nightclub'),
        ),
        array(
            'value'  =>  'Kdam Thmor',
            'label'  =>  esc_html__('Kdam Thmor','1040nightclub'),
        ),
        array(
            'value'  =>  'Keania One',
            'label'  =>  esc_html__('Keania One','1040nightclub'),
        ),
        array(
            'value'  =>  'Kelly Slab',
            'label'  =>  esc_html__('Kelly Slab','1040nightclub'),
        ),
        array(
            'value'  =>  'Kenia',
            'label'  =>  esc_html__('Kenia','1040nightclub'),
        ),
        array(
            'value'  =>  'Khand',
            'label'  =>  esc_html__('Khand','1040nightclub'),
        ),
        array(
            'value'  =>  'Khmer',
            'label'  =>  esc_html__('Khmer','1040nightclub'),
        ),
        array(
            'value'  =>  'Kite One',
            'label'  =>  esc_html__('Kite One','1040nightclub'),
        ),
        array(
            'value'  =>  'Knewave',
            'label'  =>  esc_html__('Knewave','1040nightclub'),
        ),
        array(
            'value'  =>  'Kotta One',
            'label'  =>  esc_html__('Kotta One','1040nightclub'),
        ),
        array(
            'value'  =>  'Koulen',
            'label'  =>  esc_html__('Koulen','1040nightclub'),
        ),
        array(
            'value'  =>  'Kranky',
            'label'  =>  esc_html__('Kranky','1040nightclub'),
        ),
        array(
            'value'  =>  'Kreon',
            'label'  =>  esc_html__('Kreon','1040nightclub'),
        ),
        array(
            'value'  =>  'Kristi',
            'label'  =>  esc_html__('Kristi','1040nightclub'),
        ),
        array(
            'value'  =>  'Krona One',
            'label'  =>  esc_html__('Krona One','1040nightclub'),
        ),
        array(
            'value'  =>  'La Belle Aurore',
            'label'  =>  esc_html__('La Belle Aurore','1040nightclub'),
        ),
        array(
            'value'  =>  'Laila',
            'label'  =>  esc_html__('Laila','1040nightclub'),
        ),
        array(
            'value'  =>  'Lancelot',
            'label'  =>  esc_html__('Lancelot','1040nightclub'),
        ),
        array(
            'value'  =>  'Lato',
            'label'  =>  esc_html__('Lato','1040nightclub'),
        ),
        array(
            'value'  =>  'League Script',
            'label'  =>  esc_html__('League Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Leckerli One',
            'label'  =>  esc_html__('Leckerli One','1040nightclub'),
        ),
        array(
            'value'  =>  'Ledger',
            'label'  =>  esc_html__('Ledger','1040nightclub'),
        ),
        array(
            'value'  =>  'Lekton',
            'label'  =>  esc_html__('Lekton','1040nightclub'),
        ),
        array(
            'value'  =>  'Lemon',
            'label'  =>  esc_html__('Lemon','1040nightclub'),
        ),
        array(
            'value'  =>  'Libre Baskerville',
            'label'  =>  esc_html__('Libre Baskerville','1040nightclub'),
        ),
        array(
            'value'  =>  'Life Savers',
            'label'  =>  esc_html__('Life Savers','1040nightclub'),
        ),
        array(
            'value'  =>  'Lilita One',
            'label'  =>  esc_html__('Lilita One','1040nightclub'),
        ),
        array(
            'value'  =>  'Lily Script One',
            'label'  =>  esc_html__('Lily Script One','1040nightclub'),
        ),
        array(
            'value'  =>  'Limelight',
            'label'  =>  esc_html__('Limelight','1040nightclub'),
        ),
        array(
            'value'  =>  'Linden Hill',
            'label'  =>  esc_html__('Linden Hill','1040nightclub'),
        ),
        array(
            'value'  =>  'Lobster',
            'label'  =>  esc_html__('Lobster','1040nightclub'),
        ),
        array(
            'value'  =>  'Lobster Two',
            'label'  =>  esc_html__('Lobster Two','1040nightclub'),
        ),
        array(
            'value'  =>  'Londrina Outline',
            'label'  =>  esc_html__('Londrina Outline','1040nightclub'),
        ),
        array(
            'value'  =>  'Londrina Shadow',
            'label'  =>  esc_html__('Londrina Shadow','1040nightclub'),
        ),
        array(
            'value'  =>  'Londrina Sketch',
            'label'  =>  esc_html__('Londrina Sketch','1040nightclub'),
        ),
        array(
            'value'  =>  'Londrina Solid',
            'label'  =>  esc_html__('Londrina Solid','1040nightclub'),
        ),
        array(
            'value'  =>  'Lora',
            'label'  =>  esc_html__('Lora','1040nightclub'),
        ),
        array(
            'value'  =>  'Love Ya Like A Sister',
            'label'  =>  esc_html__('Love Ya Like A Sister','1040nightclub'),
        ),
        array(
            'value'  =>  'Loved by the King',
            'label'  =>  esc_html__('Loved by the King','1040nightclub'),
        ),
        array(
            'value'  =>  'Lovers Quarrel',
            'label'  =>  esc_html__('Lovers Quarrel','1040nightclub'),
        ),
        array(
            'value'  =>  'Luckiest Guy',
            'label'  =>  esc_html__('Luckiest Guy','1040nightclub'),
        ),
        array(
            'value'  =>  'Lusitana',
            'label'  =>  esc_html__('Lusitana','1040nightclub'),
        ),
        array(
            'value'  =>  'Lustria',
            'label'  =>  esc_html__('Lustria','1040nightclub'),
        ),
        array(
            'value'  =>  'Macondo',
            'label'  =>  esc_html__('Macondo','1040nightclub'),
        ),
        array(
            'value'  =>  'Macondo Swash Caps',
            'label'  =>  esc_html__('Macondo Swash Caps','1040nightclub'),
        ),
        array(
            'value'  =>  'Magra',
            'label'  =>  esc_html__('Magra','1040nightclub'),
        ),
        array(
            'value'  =>  'Maiden Orange',
            'label'  =>  esc_html__('Maiden Orange','1040nightclub'),
        ),
        array(
            'value'  =>  'Marcellus',
            'label'  =>  esc_html__('Marcellus','1040nightclub'),
        ),
        array(
            'value'  =>  'Marcellus SC',
            'label'  =>  esc_html__('Marcellus SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Marck Script',
            'label'  =>  esc_html__('Marck Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Margarine',
            'label'  =>  esc_html__('Margarine','1040nightclub'),
        ),
        array(
            'value'  =>  'Marko One',
            'label'  =>  esc_html__('Marko One','1040nightclub'),
        ),
        array(
            'value'  =>  'Marmelad',
            'label'  =>  esc_html__('Marmelad','1040nightclub'),
        ),
        array(
            'value'  =>  'Marvel',
            'label'  =>  esc_html__('Marvel','1040nightclub'),
        ),
        array(
            'value'  =>  'Mate',
            'label'  =>  esc_html__('Mate','1040nightclub'),
        ),
        array(
            'value'  =>  'Mate SC',
            'label'  =>  esc_html__('Mate SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Maven Pro',
            'label'  =>  esc_html__('Maven Pro','1040nightclub'),
        ),
        array(
            'value'  =>  'McLaren',
            'label'  =>  esc_html__('McLaren','1040nightclub'),
        ),
        array(
            'value'  =>  'Meddon',
            'label'  =>  esc_html__('Meddon','1040nightclub'),
        ),
        array(
            'value'  =>  'MedievalSharp',
            'label'  =>  esc_html__('MedievalSharp','1040nightclub'),
        ),
        array(
            'value'  =>  'Medula One',
            'label'  =>  esc_html__('Medula One','1040nightclub'),
        ),
        array(
            'value'  =>  'Megrim',
            'label'  =>  esc_html__('Megrim','1040nightclub'),
        ),
        array(
            'value'  =>  'Meie Script',
            'label'  =>  esc_html__('Meie Script','1040nightclub'),
        ),
        array(
            'value'  =>  'League Script',
            'label'  =>  esc_html__('League Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Merienda',
            'label'  =>  esc_html__('Merienda','1040nightclub'),
        ),
        array(
            'value'  =>  'Merienda One',
            'label'  =>  esc_html__('Merienda One','1040nightclub'),
        ),
        array(
            'value'  =>  'Merriweather',
            'label'  =>  esc_html__('Merriweather','1040nightclub'),
        ),
        array(
            'value'  =>  'Merriweather Sans',
            'label'  =>  esc_html__('Merriweather Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Metal',
            'label'  =>  esc_html__('Metal','1040nightclub'),
        ),
        array(
            'value'  =>  'Metal Mania',
            'label'  =>  esc_html__('Metal Mania','1040nightclub'),
        ),
        array(
            'value'  =>  'Metamorphous',
            'label'  =>  esc_html__('Metamorphous','1040nightclub'),
        ),
        array(
            'value'  =>  'Metrophobic',
            'label'  =>  esc_html__('Metrophobic','1040nightclub'),
        ),
        array(
            'value'  =>  'Michroma',
            'label'  =>  esc_html__('Michroma','1040nightclub'),
        ),
        array(
            'value'  =>  'Milonga',
            'label'  =>  esc_html__('Milonga','1040nightclub'),
        ),
        array(
            'value'  =>  'Miltonian',
            'label'  =>  esc_html__('Miltonian','1040nightclub'),
        ),
        array(
            'value'  =>  'Miltonian Tattoo',
            'label'  =>  esc_html__('Miltonian Tattoo','1040nightclub'),
        ),
        array(
            'value'  =>  'Miniver',
            'label'  =>  esc_html__('Miniver','1040nightclub'),
        ),
        array(
            'value'  =>  'Miss Fajardose',
            'label'  =>  esc_html__('Miss Fajardose','1040nightclub'),
        ),
        array(
            'value'  =>  'Modern Antiqua',
            'label'  =>  esc_html__('Modern Antiqua','1040nightclub'),
        ),
        array(
            'value'  =>  'Molengo',
            'label'  =>  esc_html__('Molengo','1040nightclub'),
        ),
        array(
            'value'  =>  'Molle',
            'label'  =>  esc_html__('Molle','1040nightclub'),
        ),
        array(
            'value'  =>  'Monda',
            'label'  =>  esc_html__('Monda','1040nightclub'),
        ),
        array(
            'value'  =>  'Monofett',
            'label'  =>  esc_html__('Monofett','1040nightclub'),
        ),
        array(
            'value'  =>  'Monoton',
            'label'  =>  esc_html__('Monoton','1040nightclub'),
        ),
        array(
            'value'  =>  'Monsieur La Doulaise',
            'label'  =>  esc_html__('Monsieur La Doulaise','1040nightclub'),
        ),
        array(
            'value'  =>  'Montaga',
            'label'  =>  esc_html__('Montaga','1040nightclub'),
        ),
        array(
            'value'  =>  'Montez',
            'label'  =>  esc_html__('Montez','1040nightclub'),
        ),
        array(
            'value'  =>  'Montserrat',
            'label'  =>  esc_html__('Montserrat','1040nightclub'),
        ),
        array(
            'value'  =>  'Montserrat Alternates',
            'label'  =>  esc_html__('Montserrat Alternates','1040nightclub'),
        ),
        array(
            'value'  =>  'Montserrat Subrayada',
            'label'  =>  esc_html__('Montserrat Subrayada','1040nightclub'),
        ),
        array(
            'value'  =>  'Moul',
            'label'  =>  esc_html__('Moul','1040nightclub'),
        ),
        array(
            'value'  =>  'Moulpali',
            'label'  =>  esc_html__('Moulpali','1040nightclub'),
        ),
        array(
            'value'  =>  'Mountains of Christmas',
            'label'  =>  esc_html__('Mountains of Christmas','1040nightclub'),
        ),
        array(
            'value'  =>  'Mouse Memoirs',
            'label'  =>  esc_html__('Mouse Memoirs','1040nightclub'),
        ),
        array(
            'value'  =>  'Mr Bedfort',
            'label'  =>  esc_html__('Mr Bedfort','1040nightclub'),
        ),
        array(
            'value'  =>  'Mr Dafoe',
            'label'  =>  esc_html__('Mr Dafoe','1040nightclub'),
        ),
        array(
            'value'  =>  'Mr De Haviland',
            'label'  =>  esc_html__('Mr De Haviland','1040nightclub'),
        ),
        array(
            'value'  =>  'Mrs Saint Delafield',
            'label'  =>  esc_html__('Mrs Saint Delafield','1040nightclub'),
        ),
        array(
            'value'  =>  'Mrs Sheppards',
            'label'  =>  esc_html__('Mrs Sheppards','1040nightclub'),
        ),
        array(
            'value'  =>  'Muli',
            'label'  =>  esc_html__('Muli','1040nightclub'),
        ),
        array(
            'value'  =>  'Mystery Quest',
            'label'  =>  esc_html__('Mystery Quest','1040nightclub'),
        ),
        array(
            'value'  =>  'Neucha',
            'label'  =>  esc_html__('Neucha','1040nightclub'),
        ),
        array(
            'value'  =>  'Neuton',
            'label'  =>  esc_html__('Neuton','1040nightclub'),
        ),
        array(
            'value'  =>  'New Rocker',
            'label'  =>  esc_html__('New Rocker','1040nightclub'),
        ),
        array(
            'value'  =>  'News Cycle',
            'label'  =>  esc_html__('News Cycle','1040nightclub'),
        ),
        array(
            'value'  =>  'Niconne',
            'label'  =>  esc_html__('Niconne','1040nightclub'),
        ),
        array(
            'value'  =>  'Nixie One',
            'label'  =>  esc_html__('Nixie One','1040nightclub'),
        ),
        array(
            'value'  =>  'Nobile',
            'label'  =>  esc_html__('Nobile','1040nightclub'),
        ),
        array(
            'value'  =>  'Nokora',
            'label'  =>  esc_html__('Nokora','1040nightclub'),
        ),
        array(
            'value'  =>  'Norican',
            'label'  =>  esc_html__('Norican','1040nightclub'),
        ),
        array(
            'value'  =>  'Nosifer',
            'label'  =>  esc_html__('Nosifer','1040nightclub'),
        ),
        array(
            'value'  =>  'Nothing You Could Do',
            'label'  =>  esc_html__('Nothing You Could Do','1040nightclub'),
        ),
        array(
            'value'  =>  'Noticia Text',
            'label'  =>  esc_html__('Noticia Text','1040nightclub'),
        ),
        array(
            'value'  =>  'Noto Sans',
            'label'  =>  esc_html__('Noto Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Noto Serif',
            'label'  =>  esc_html__('Noto Serif','1040nightclub'),
        ),
        array(
            'value'  =>  'Nova Cut',
            'label'  =>  esc_html__('Nova Cut','1040nightclub'),
        ),
        array(
            'value'  =>  'Nova Flat',
            'label'  =>  esc_html__('Nova Flat','1040nightclub'),
        ),
        array(
            'value'  =>  'Nova Mono',
            'label'  =>  esc_html__('Nova Mono','1040nightclub'),
        ),
        array(
            'value'  =>  'Nova Oval',
            'label'  =>  esc_html__('Nova Oval','1040nightclub'),
        ),
        array(
            'value'  =>  'Nova Round',
            'label'  =>  esc_html__('Nova Round','1040nightclub'),
        ),
        array(
            'value'  =>  'Nova Script',
            'label'  =>  esc_html__('Nova Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Nova Slim',
            'label'  =>  esc_html__('Nova Slim','1040nightclub'),
        ),
        array(
            'value'  =>  'Nova Square',
            'label'  =>  esc_html__('Nova Square','1040nightclub'),
        ),
        array(
            'value'  =>  'Numans',
            'label'  =>  esc_html__('Numans','1040nightclub'),
        ),
        array(
            'value'  =>  'Nunito',
            'label'  =>  esc_html__('Nunito','1040nightclub'),
        ),
        array(
            'value'  =>  'Odor Mean Chey',
            'label'  =>  esc_html__('Odor Mean Chey','1040nightclub'),
        ),
        array(
            'value'  =>  'Offside',
            'label'  =>  esc_html__('Offside','1040nightclub'),
        ),
        array(
            'value'  =>  'Old Standard TT',
            'label'  =>  esc_html__('Old Standard TT','1040nightclub'),
        ),
        array(
            'value'  =>  'Oldenburg',
            'label'  =>  esc_html__('Oldenburg','1040nightclub'),
        ),
        array(
            'value'  =>  'Oleo Script',
            'label'  =>  esc_html__('Oleo Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Oleo Script Swash Caps',
            'label'  =>  esc_html__('Oleo Script Swash Caps','1040nightclub'),
        ),
        array(
            'value'  =>  'Open Sans',
            'label'  =>  esc_html__('Open Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Open Sans Condensed',
            'label'  =>  esc_html__('Open Sans Condensed','1040nightclub'),
        ),
        array(
            'value'  =>  'Oranienbaum',
            'label'  =>  esc_html__('Oranienbaum','1040nightclub'),
        ),
        array(
            'value'  =>  'Orbitron',
            'label'  =>  esc_html__('Orbitron','1040nightclub'),
        ),
        array(
            'value'  =>  'Oregano',
            'label'  =>  esc_html__('Oregano','1040nightclub'),
        ),
        array(
            'value'  =>  'Orienta',
            'label'  =>  esc_html__('Orienta','1040nightclub'),
        ),
        array(
            'value'  =>  'Original Surfer',
            'label'  =>  esc_html__('Original Surfer','1040nightclub'),
        ),
        array(
            'value'  =>  'Oswald',
            'label'  =>  esc_html__('Oswald','1040nightclub'),
        ),
        array(
            'value'  =>  'Over the Rainbow',
            'label'  =>  esc_html__('Over the Rainbow','1040nightclub'),
        ),
        array(
            'value'  =>  'Overlock',
            'label'  =>  esc_html__('Overlock','1040nightclub'),
        ),
        array(
            'value'  =>  'Overlock SC',
            'label'  =>  esc_html__('Overlock SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Ovo',
            'label'  =>  esc_html__('Ovo','1040nightclub'),
        ),
        array(
            'value'  =>  'Oxygen',
            'label'  =>  esc_html__('Oxygen','1040nightclub'),
        ),
        array(
            'value'  =>  'Oxygen Mono',
            'label'  =>  esc_html__('Oxygen Mono','1040nightclub'),
        ),
        array(
            'value'  =>  'PT Mono',
            'label'  =>  esc_html__('PT Mono','1040nightclub'),
        ),
        array(
            'value'  =>  'PT Sans',
            'label'  =>  esc_html__('PT Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'PT Sans Caption',
            'label'  =>  esc_html__('PT Sans Caption','1040nightclub'),
        ),
        array(
            'value'  =>  'PT Sans Narrow',
            'label'  =>  esc_html__('PT Sans Narrow','1040nightclub'),
        ),
        array(
            'value'  =>  'PT Serif',
            'label'  =>  esc_html__('PT Serif','1040nightclub'),
        ),
        array(
            'value'  =>  'PT Serif Caption',
            'label'  =>  esc_html__('PT Serif Caption','1040nightclub'),
        ),
        array(
            'value'  =>  'Pacifico',
            'label'  =>  esc_html__('Pacifico','1040nightclub'),
        ),
        array(
            'value'  =>  'Paprika',
            'label'  =>  esc_html__('Paprika','1040nightclub'),
        ),
        array(
            'value'  =>  'Parisienne',
            'label'  =>  esc_html__('Parisienne','1040nightclub'),
        ),
        array(
            'value'  =>  'Passero One',
            'label'  =>  esc_html__('Passero One','1040nightclub'),
        ),
        array(
            'value'  =>  'Passion One',
            'label'  =>  esc_html__('Passion One','1040nightclub'),
        ),
        array(
            'value'  =>  'Pathway Gothic One',
            'label'  =>  esc_html__('Pathway Gothic One','1040nightclub'),
        ),
        array(
            'value'  =>  'Patrick Hand',
            'label'  =>  esc_html__('Patrick Hand','1040nightclub'),
        ),
        array(
            'value'  =>  'Patrick Hand SC',
            'label'  =>  esc_html__('Patrick Hand SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Patua One',
            'label'  =>  esc_html__('Patua One','1040nightclub'),
        ),
        array(
            'value'  =>  'Paytone One',
            'label'  =>  esc_html__('Paytone One','1040nightclub'),
        ),
        array(
            'value'  =>  'Peralta',
            'label'  =>  esc_html__('Peralta','1040nightclub'),
        ),
        array(
            'value'  =>  'Permanent Marker',
            'label'  =>  esc_html__('Permanent Marker','1040nightclub'),
        ),
        array(
            'value'  =>  'Petit Formal Script',
            'label'  =>  esc_html__('Petit Formal Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Petrona',
            'label'  =>  esc_html__('Petrona','1040nightclub'),
        ),
        array(
            'value'  =>  'Philosopher',
            'label'  =>  esc_html__('Philosopher','1040nightclub'),
        ),
        array(
            'value'  =>  'Piedra',
            'label'  =>  esc_html__('Piedra','1040nightclub'),
        ),
        array(
            'value'  =>  'Pinyon Script',
            'label'  =>  esc_html__('Pinyon Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Pirata One',
            'label'  =>  esc_html__('Pirata One','1040nightclub'),
        ),
        array(
            'value'  =>  'Plaster',
            'label'  =>  esc_html__('Plaster','1040nightclub'),
        ),
        array(
            'value'  =>  'Play',
            'label'  =>  esc_html__('Play','1040nightclub'),
        ),
        array(
            'value'  =>  'Playball',
            'label'  =>  esc_html__('Playball','1040nightclub'),
        ),
        array(
            'value'  =>  'Playfair Display',
            'label'  =>  esc_html__('Playfair Display','1040nightclub'),
        ),
        array(
            'value'  =>  'Playfair Display SC',
            'label'  =>  esc_html__('Playfair Display SC','1040nightclub'),
        ),
        array(
            'value'  =>  'Podkova',
            'label'  =>  esc_html__('Podkova','1040nightclub'),
        ),
        array(
            'value'  =>  'Poiret One',
            'label'  =>  esc_html__('Poiret One','1040nightclub'),
        ),
        array(
            'value'  =>  'Poller One',
            'label'  =>  esc_html__('Poller One','1040nightclub'),
        ),
        array(
            'value'  =>  'Poly',
            'label'  =>  esc_html__('Poly','1040nightclub'),
        ),
        array(
            'value'  =>  'Pompiere',
            'label'  =>  esc_html__('Pompiere','1040nightclub'),
        ),
        array(
            'value'  =>  'Pontano Sans',
            'label'  =>  esc_html__('Pontano Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Poppins',
            'label'  =>  esc_html__('Poppins','1040nightclub'),
        ),
        array(
            'value'  =>  'Port Lligat Sans',
            'label'  =>  esc_html__('Port Lligat Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Port Lligat Slab',
            'label'  =>  esc_html__('Port Lligat Slab','1040nightclub'),
        ),
        array(
            'value'  =>  'Prata',
            'label'  =>  esc_html__('Prata','1040nightclub'),
        ),
        array(
            'value'  =>  'Preahvihear',
            'label'  =>  esc_html__('Preahvihear','1040nightclub'),
        ),
        array(
            'value'  =>  'Press Start 2P',
            'label'  =>  esc_html__('Press Start 2P','1040nightclub'),
        ),
        array(
            'value'  =>  'Princess Sofia',
            'label'  =>  esc_html__('Princess Sofia','1040nightclub'),
        ),
        array(
            'value'  =>  'Prociono',
            'label'  =>  esc_html__('Prociono','1040nightclub'),
        ),
        array(
            'value'  =>  'Prosto One',
            'label'  =>  esc_html__('Prosto One','1040nightclub'),
        ),
        array(
            'value'  =>  'Puritan',
            'label'  =>  esc_html__('Puritan','1040nightclub'),
        ),
        array(
            'value'  =>  'Purple Purse',
            'label'  =>  esc_html__('Purple Purse','1040nightclub'),
        ),
        array(
            'value'  =>  'Quando',
            'label'  =>  esc_html__('Quando','1040nightclub'),
        ),
        array(
            'value'  =>  'Quantico',
            'label'  =>  esc_html__('Quantico','1040nightclub'),
        ),
        array(
            'value'  =>  'Quattrocento',
            'label'  =>  esc_html__('Quattrocento','1040nightclub'),
        ),
        array(
            'value'  =>  'Quattrocento Sans',
            'label'  =>  esc_html__('Quattrocento Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Questrial',
            'label'  =>  esc_html__('Questrial','1040nightclub'),
        ),
        array(
            'value'  =>  'Quicksand',
            'label'  =>  esc_html__('Quicksand','1040nightclub'),
        ),
        array(
            'value'  =>  'Quintessential',
            'label'  =>  esc_html__('Quintessential','1040nightclub'),
        ),
        array(
            'value'  =>  'Qwigley',
            'label'  =>  esc_html__('Qwigley','1040nightclub'),
        ),
        array(
            'value'  =>  'Racing Sans One',
            'label'  =>  esc_html__('Racing Sans One','1040nightclub'),
        ),
        array(
            'value'  =>  'Radley',
            'label'  =>  esc_html__('Radley','1040nightclub'),
        ),
        array(
            'value'  =>  'Rajdhani',
            'label'  =>  esc_html__('Rajdhani','1040nightclub'),
        ),
        array(
            'value'  =>  'Raleway',
            'label'  =>  esc_html__('Raleway','1040nightclub'),
        ),
        array(
            'value'  =>  'Raleway Dots',
            'label'  =>  esc_html__('Raleway Dots','1040nightclub'),
        ),
        array(
            'value'  =>  'Rambla',
            'label'  =>  esc_html__('Rambla','1040nightclub'),
        ),
        array(
            'value'  =>  'Rammetto One',
            'label'  =>  esc_html__('Rammetto One','1040nightclub'),
        ),
        array(
            'value'  =>  'Ranchers',
            'label'  =>  esc_html__('Ranchers','1040nightclub'),
        ),
        array(
            'value'  =>  'Rancho',
            'label'  =>  esc_html__('Rancho','1040nightclub'),
        ),
        array(
            'value'  =>  'Rationale',
            'label'  =>  esc_html__('Rationale','1040nightclub'),
        ),
        array(
            'value'  =>  'Redressed',
            'label'  =>  esc_html__('Redressed','1040nightclub'),
        ),
        array(
            'value'  =>  'Reenie Beanie',
            'label'  =>  esc_html__('Reenie Beanie','1040nightclub'),
        ),
        array(
            'value'  =>  'Revalia',
            'label'  =>  esc_html__('Revalia','1040nightclub'),
        ),
        array(
            'value'  =>  'Ribeye',
            'label'  =>  esc_html__('Ribeye','1040nightclub'),
        ),
        array(
            'value'  =>  'Ribeye Marrow',
            'label'  =>  esc_html__('Ribeye Marrow','1040nightclub'),
        ),
        array(
            'value'  =>  'Righteous',
            'label'  =>  esc_html__('Righteous','1040nightclub'),
        ),
        array(
            'value'  =>  'Risque',
            'label'  =>  esc_html__('Risque','1040nightclub'),
        ),
        array(
            'value'  =>  'Roboto',
            'label'  =>  esc_html__('Roboto','1040nightclub'),
        ),
        array(
            'value'  =>  'Roboto Condensed',
            'label'  =>  esc_html__('Roboto Condensed','1040nightclub'),
        ),
        array(
            'value'  =>  'Roboto Slab',
            'label'  =>  esc_html__('Roboto Slab','1040nightclub'),
        ),
        array(
            'value'  =>  'Rochester',
            'label'  =>  esc_html__('Rochester','1040nightclub'),
        ),
        array(
            'value'  =>  'Rock Salt',
            'label'  =>  esc_html__('Rock Salt','1040nightclub'),
        ),
        array(
            'value'  =>  'Rokkitt',
            'label'  =>  esc_html__('Rokkitt','1040nightclub'),
        ),
        array(
            'value'  =>  'Romanesco',
            'label'  =>  esc_html__('Romanesco','1040nightclub'),
        ),
        array(
            'value'  =>  'Ropa Sans',
            'label'  =>  esc_html__('Ropa Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Rosario',
            'label'  =>  esc_html__('Rosario','1040nightclub'),
        ),
        array(
            'value'  =>  'Rosarivo',
            'label'  =>  esc_html__('Rosarivo','1040nightclub'),
        ),
        array(
            'value'  =>  'Rouge Script',
            'label'  =>  esc_html__('Rouge Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Rozha One',
            'label'  =>  esc_html__('Rozha One','1040nightclub'),
        ),
        array(
            'value'  =>  'Rubik Mono One',
            'label'  =>  esc_html__('Rubik Mono One','1040nightclub'),
        ),
        array(
            'value'  =>  'Rubik One',
            'label'  =>  esc_html__('Rubik One','1040nightclub'),
        ),
        array(
            'value'  =>  'Ruda',
            'label'  =>  esc_html__('Ruda','1040nightclub'),
        ),
        array(
            'value'  =>  'Rufina',
            'label'  =>  esc_html__('Rufina','1040nightclub'),
        ),
        array(
            'value'  =>  'Ruge Boogie',
            'label'  =>  esc_html__('Ruge Boogie','1040nightclub'),
        ),
        array(
            'value'  =>  'Ruluko',
            'label'  =>  esc_html__('Ruluko','1040nightclub'),
        ),
        array(
            'value'  =>  'Rum Raisin',
            'label'  =>  esc_html__('Rum Raisin','1040nightclub'),
        ),
        array(
            'value'  =>  'Ruslan Display',
            'label'  =>  esc_html__('Ruslan Display','1040nightclub'),
        ),
        array(
            'value'  =>  'Russo One',
            'label'  =>  esc_html__('Russo One','1040nightclub'),
        ),
        array(
            'value'  =>  'Ruthie',
            'label'  =>  esc_html__('Ruthie','1040nightclub'),
        ),
        array(
            'value'  =>  'Rye',
            'label'  =>  esc_html__('Rye','1040nightclub'),
        ),
        array(
            'value'  =>  'Sacramento',
            'label'  =>  esc_html__('Sacramento','1040nightclub'),
        ),
        array(
            'value'  =>  'Sail',
            'label'  =>  esc_html__('Sail','1040nightclub'),
        ),
        array(
            'value'  =>  'Salsa',
            'label'  =>  esc_html__('Salsa','1040nightclub'),
        ),
        array(
            'value'  =>  'Sanchez',
            'label'  =>  esc_html__('Sanchez','1040nightclub'),
        ),
        array(
            'value'  =>  'Sancreek',
            'label'  =>  esc_html__('Sancreek','1040nightclub'),
        ),
        array(
            'value'  =>  'Sansita One',
            'label'  =>  esc_html__('Sansita One','1040nightclub'),
        ),
        array(
            'value'  =>  'Sarina',
            'label'  =>  esc_html__('Sarina','1040nightclub'),
        ),
        array(
            'value'  =>  'Sarpanch',
            'label'  =>  esc_html__('Sarpanch','1040nightclub'),
        ),
        array(
            'value'  =>  'Satisfy',
            'label'  =>  esc_html__('Satisfy','1040nightclub'),
        ),
        array(
            'value'  =>  'Scada',
            'label'  =>  esc_html__('Scada','1040nightclub'),
        ),
        array(
            'value'  =>  'Schoolbell',
            'label'  =>  esc_html__('Schoolbell','1040nightclub'),
        ),
        array(
            'value'  =>  'Seaweed Script',
            'label'  =>  esc_html__('Seaweed Script','1040nightclub'),
        ),
        array(
            'value'  =>  'Sevillana',
            'label'  =>  esc_html__('Sevillana','1040nightclub'),
        ),
        array(
            'value'  =>  'Seymour One',
            'label'  =>  esc_html__('Seymour One','1040nightclub'),
        ),
        array(
            'value'  =>  'Shadows Into Light',
            'label'  =>  esc_html__('Shadows Into Light','1040nightclub'),
        ),
        array(
            'value'  =>  'Shadows Into Light Two',
            'label'  =>  esc_html__('Shadows Into Light Two','1040nightclub'),
        ),
        array(
            'value'  =>  'Shanti',
            'label'  =>  esc_html__('Shanti','1040nightclub'),
        ),
        array(
            'value'  =>  'Share',
            'label'  =>  esc_html__('Share','1040nightclub'),
        ),
        array(
            'value'  =>  'Share Tech',
            'label'  =>  esc_html__('Share Tech','1040nightclub'),
        ),
        array(
            'value'  =>  'Share Tech Mono',
            'label'  =>  esc_html__('Share Tech Mono','1040nightclub'),
        ),
        array(
            'value'  =>  'Shojumaru',
            'label'  =>  esc_html__('Shojumaru','1040nightclub'),
        ),
        array(
            'value'  =>  'Short Stack',
            'label'  =>  esc_html__('Short Stack','1040nightclub'),
        ),
        array(
            'value'  =>  'Siemreap',
            'label'  =>  esc_html__('Siemreap','1040nightclub'),
        ),
        array(
            'value'  =>  'Sigmar One',
            'label'  =>  esc_html__('Sigmar One','1040nightclub'),
        ),
        array(
            'value'  =>  'Signika',
            'label'  =>  esc_html__('Signika','1040nightclub'),
        ),
        array(
            'value'  =>  'Signika Negative',
            'label'  =>  esc_html__('Signika Negative','1040nightclub'),
        ),
        array(
            'value'  =>  'Simonetta',
            'label'  =>  esc_html__('Simonetta','1040nightclub'),
        ),
        array(
            'value'  =>  'Sintony',
            'label'  =>  esc_html__('Sintony','1040nightclub'),
        ),
        array(
            'value'  =>  'Sirin Stencil',
            'label'  =>  esc_html__('Sirin Stencil','1040nightclub'),
        ),
        array(
            'value'  =>  'Six Caps',
            'label'  =>  esc_html__('Six Caps','1040nightclub'),
        ),
        array(
            'value'  =>  'Skranji',
            'label'  =>  esc_html__('Skranji','1040nightclub'),
        ),
        array(
            'value'  =>  'Slabo 13px',
            'label'  =>  esc_html__('Slabo 13px','1040nightclub'),
        ),
        array(
            'value'  =>  'Slabo 27px',
            'label'  =>  esc_html__('Slabo 27px','1040nightclub'),
        ),
        array(
            'value'  =>  'Slackey',
            'label'  =>  esc_html__('Slackey','1040nightclub'),
        ),
        array(
            'value'  =>  'Smokum',
            'label'  =>  esc_html__('Smokum','1040nightclub'),
        ),
        array(
            'value'  =>  'Smythe',
            'label'  =>  esc_html__('Smythe','1040nightclub'),
        ),
        array(
            'value'  =>  'Sniglet',
            'label'  =>  esc_html__('Sniglet','1040nightclub'),
        ),
        array(
            'value'  =>  'Snippet',
            'label'  =>  esc_html__('Snippet','1040nightclub'),
        ),
        array(
            'value'  =>  'Snowburst One',
            'label'  =>  esc_html__('Snowburst One','1040nightclub'),
        ),
        array(
            'value'  =>  'Sofadi One',
            'label'  =>  esc_html__('Sofadi One','1040nightclub'),
        ),
        array(
            'value'  =>  'Sofia',
            'label'  =>  esc_html__('Sofia','1040nightclub'),
        ),
        array(
            'value'  =>  'Sonsie One',
            'label'  =>  esc_html__('Sonsie One','1040nightclub'),
        ),
        array(
            'value'  =>  'Sorts Mill Goudy',
            'label'  =>  esc_html__('Sorts Mill Goudy','1040nightclub'),
        ),
        array(
            'value'  =>  'Source Code Pro',
            'label'  =>  esc_html__('Source Code Pro','1040nightclub'),
        ),
        array(
            'value'  =>  'Source Sans Pro',
            'label'  =>  esc_html__('Source Sans Pro','1040nightclub'),
        ),
        array(
            'value'  =>  'Source Serif Pro',
            'label'  =>  esc_html__('Source Serif Pro','1040nightclub'),
        ),
        array(
            'value'  =>  'Special Elite',
            'label'  =>  esc_html__('Special Elite','1040nightclub'),
        ),
        array(
            'value'  =>  'Spicy Rice',
            'label'  =>  esc_html__('Spicy Rice','1040nightclub'),
        ),
        array(
            'value'  =>  'Spinnaker',
            'label'  =>  esc_html__('Spinnaker','1040nightclub'),
        ),
        array(
            'value'  =>  'Spirax',
            'label'  =>  esc_html__('Spirax','1040nightclub'),
        ),
        array(
            'value'  =>  'Squada One',
            'label'  =>  esc_html__('Squada One','1040nightclub'),
        ),
        array(
            'value'  =>  'Stalemate',
            'label'  =>  esc_html__('Stalemate','1040nightclub'),
        ),
        array(
            'value'  =>  'Stalinist One',
            'label'  =>  esc_html__('Stalinist One','1040nightclub'),
        ),
        array(
            'value'  =>  'Stardos Stencil',
            'label'  =>  esc_html__('Stardos Stencil','1040nightclub'),
        ),
        array(
            'value'  =>  'Stint Ultra Condensed',
            'label'  =>  esc_html__('Stint Ultra Condensed','1040nightclub'),
        ),
        array(
            'value'  =>  'Stint Ultra Expanded',
            'label'  =>  esc_html__('Stint Ultra Expanded','1040nightclub'),
        ),
        array(
            'value'  =>  'Stoke',
            'label'  =>  esc_html__('Stoke','1040nightclub'),
        ),
        array(
            'value'  =>  'Strait',
            'label'  =>  esc_html__('Strait','1040nightclub'),
        ),
        array(
            'value'  =>  'Sue Ellen Francisco',
            'label'  =>  esc_html__('Sue Ellen Francisco','1040nightclub'),
        ),
        array(
            'value'  =>  'Sunshiney',
            'label'  =>  esc_html__('Sunshiney','1040nightclub'),
        ),
        array(
            'value'  =>  'Supermercado One',
            'label'  =>  esc_html__('Supermercado One','1040nightclub'),
        ),
        array(
            'value'  =>  'Suwannaphum',
            'label'  =>  esc_html__('Suwannaphum','1040nightclub'),
        ),
        array(
            'value'  =>  'Swanky and Moo Moo',
            'label'  =>  esc_html__('Swanky and Moo Moo','1040nightclub'),
        ),
        array(
            'value'  =>  'Syncopate',
            'label'  =>  esc_html__('Syncopate','1040nightclub'),
        ),
        array(
            'value'  =>  'Tangerine',
            'label'  =>  esc_html__('Tangerine','1040nightclub'),
        ),
        array(
            'value'  =>  'Taprom',
            'label'  =>  esc_html__('Taprom','1040nightclub'),
        ),
        array(
            'value'  =>  'Tauri',
            'label'  =>  esc_html__('Tauri','1040nightclub'),
        ),
        array(
            'value'  =>  'Teko',
            'label'  =>  esc_html__('Teko','1040nightclub'),
        ),
        array(
            'value'  =>  'Telex',
            'label'  =>  esc_html__('Telex','1040nightclub'),
        ),
        array(
            'value'  =>  'Tenor Sans',
            'label'  =>  esc_html__('Tenor Sans','1040nightclub'),
        ),
        array(
            'value'  =>  'Text Me One',
            'label'  =>  esc_html__('Text Me One','1040nightclub'),
        ),
        array(
            'value'  =>  'The Girl Next Door',
            'label'  =>  esc_html__('The Girl Next Door','1040nightclub'),
        ),
        array(
            'value'  =>  'Tienne',
            'label'  =>  esc_html__('Tienne','1040nightclub'),
        ),
        array(
            'value'  =>  'Tinos',
            'label'  =>  esc_html__('Tinos','1040nightclub'),
        ),
        array(
            'value'  =>  'Titan One',
            'label'  =>  esc_html__('Titan One','1040nightclub'),
        ),
        array(
            'value'  =>  'Titillium Web',
            'label'  =>  esc_html__('Titillium Web','1040nightclub'),
        ),
        array(
            'value'  =>  'Trade Winds',
            'label'  =>  esc_html__('Trade Winds','1040nightclub'),
        ),
        array(
            'value'  =>  'Trocchi',
            'label'  =>  esc_html__('Trocchi','1040nightclub'),
        ),
        array(
            'value'  =>  'Trochut',
            'label'  =>  esc_html__('Trochut','1040nightclub'),
        ),
        array(
            'value'  =>  'Trykker',
            'label'  =>  esc_html__('Trykker','1040nightclub'),
        ),
        array(
            'value'  =>  'Tulpen One',
            'label'  =>  esc_html__('Tulpen One','1040nightclub'),
        ),
        array(
            'value'  =>  'Ubuntu',
            'label'  =>  esc_html__('Ubuntu','1040nightclub'),
        ),
        array(
            'value'  =>  'Ubuntu Condensed',
            'label'  =>  esc_html__('Ubuntu Condensed','1040nightclub'),
        ),
        array(
            'value'  =>  'Ubuntu Mono',
            'label'  =>  esc_html__('Ubuntu Mono','1040nightclub'),
        ),
        array(
            'value'  =>  'Ultra',
            'label'  =>  esc_html__('Ultra','1040nightclub'),
        ),
        array(
            'value'  =>  'Uncial Antiqua',
            'label'  =>  esc_html__('Uncial Antiqua','1040nightclub'),
        ),
        array(
            'value'  =>  'Underdog',
            'label'  =>  esc_html__('Underdog','1040nightclub'),
        ),
        array(
            'value'  =>  'Unica One',
            'label'  =>  esc_html__('Unica One','1040nightclub'),
        ),
        array(
            'value'  =>  'UnifrakturCook',
            'label'  =>  esc_html__('UnifrakturCook','1040nightclub'),
        ),
        array(
            'value'  =>  'UnifrakturMaguntia',
            'label'  =>  esc_html__('UnifrakturMaguntia','1040nightclub'),
        ),
        array(
            'value'  =>  'Unkempt',
            'label'  =>  esc_html__('Unkempt','1040nightclub'),
        ),
        array(
            'value'  =>  'Unlock',
            'label'  =>  esc_html__('Unlock','1040nightclub'),
        ),
        array(
            'value'  =>  'Unna',
            'label'  =>  esc_html__('Unna','1040nightclub'),
        ),
        array(
            'value'  =>  'VT323',
            'label'  =>  esc_html__('VT323','1040nightclub'),
        ),
        array(
            'value'  =>  'Vampiro One',
            'label'  =>  esc_html__('Vampiro One','1040nightclub'),
        ),
        array(
            'value'  =>  'Varela',
            'label'  =>  esc_html__('Varela','1040nightclub'),
        ),
        array(
            'value'  =>  'Varela Round',
            'label'  =>  esc_html__('Varela Round','1040nightclub'),
        ),
        array(
            'value'  =>  'Vast Shadow',
            'label'  =>  esc_html__('Vast Shadow','1040nightclub'),
        ),
        array(
            'value'  =>  'Vesper Libre',
            'label'  =>  esc_html__('Vesper Libre','1040nightclub'),
        ),
        array(
            'value'  =>  'Vibur',
            'label'  =>  esc_html__('Vibur','1040nightclub'),
        ),
        array(
            'value'  =>  'Vidaloka',
            'label'  =>  esc_html__('Vidaloka','1040nightclub'),
        ),
        array(
            'value'  =>  'Viga',
            'label'  =>  esc_html__('Viga','1040nightclub'),
        ),
        array(
            'value'  =>  'Voces',
            'label'  =>  esc_html__('Voces','1040nightclub'),
        ),
        array(
            'value'  =>  'Volkhov',
            'label'  =>  esc_html__('Volkhov','1040nightclub'),
        ),
        array(
            'value'  =>  'Vollkorn',
            'label'  =>  esc_html__('Vollkorn','1040nightclub'),
        ),
        array(
            'value'  =>  'Voltaire',
            'label'  =>  esc_html__('Voltaire','1040nightclub'),
        ),
        array(
            'value'  =>  'Waiting for the Sunrise',
            'label'  =>  esc_html__('Waiting for the Sunrise','1040nightclub'),
        ),
        array(
            'value'  =>  'Wallpoet',
            'label'  =>  esc_html__('Wallpoet','1040nightclub'),
        ),
        array(
            'value'  =>  'Walter Turncoat',
            'label'  =>  esc_html__('Walter Turncoat','1040nightclub'),
        ),
        array(
            'value'  =>  'Warnes',
            'label'  =>  esc_html__('Warnes','1040nightclub'),
        ),
        array(
            'value'  =>  'Wellfleet',
            'label'  =>  esc_html__('Wellfleet','1040nightclub'),
        ),
        array(
            'value'  =>  'Wendy One',
            'label'  =>  esc_html__('Wendy One','1040nightclub'),
        ),
        array(
            'value'  =>  'Wire One',
            'label'  =>  esc_html__('Wire One','1040nightclub'),
        ),
        array(
            'value'  =>  'Yanone Kaffeesatz',
            'label'  =>  esc_html__('Yanone Kaffeesatz','1040nightclub'),
        ),
        array(
            'value'  =>  'Yellowtail',
            'label'  =>  esc_html__('Yellowtail','1040nightclub'),
        ),
        array(
            'value'  =>  'Yeseva One',
            'label'  =>  esc_html__('Yeseva One','1040nightclub'),
        ),
        array(
            'value'  =>  'Yesteryear',
            'label'  =>  esc_html__('Yesteryear','1040nightclub'),
        ),
        array(
            'value'  =>  'Zeyada',
            'label'  =>  esc_html__('Zeyada','1040nightclub'),
        ),

    );
    $tz_1040nightclub_font_option_style =   array(
        array(
            'value'       => '100',
            'label'       => '100 Thin',
            'src'         => ''
        ),
        array(
            'value'       => '100italic',
            'label'       => '100 Thin Italic',
            'src'         => ''
        ),
        array(
            'value'       => '200',
            'label'       => '200 Extra-Light',
            'src'         => ''
        ),
        array(
            'value'       => '200italic',
            'label'       => '200 Extra-Light Italic',
            'src'         => ''
        ),
        array(
            'value'       => '300',
            'label'       => '300 Light',
            'src'         => ''
        ),
        array(
            'value'       => '300italic',
            'label'       => '300 Light Italic',
            'src'         => ''
        ),
        array(
            'value'       => '400',
            'label'       => '400 Regular',
            'src'         => ''
        ),
        array(
            'value'       => '400italic',
            'label'       => '400 Regular Italic',
            'src'         => ''
        ),
        array(
            'value'       => '500',
            'label'       => '500 Medium',
            'src'         => ''
        ),
        array(
            'value'       => '500italic',
            'label'       => '500 Medium Italic',
            'src'         => ''
        ),
        array(
            'value'       => '600',
            'label'       => '600 Semi-Bold',
            'src'         => ''
        ),
        array(
            'value'       => '600italic',
            'label'       => '600 Semi-Bold Italic',
            'src'         => ''
        ),
        array(
            'value'       => '700',
            'label'       => '700 Bold',
            'src'         => ''
        ),
        array(
            'value'       => '700italic',
            'label'       => '700 Bold Italic',
            'src'         => ''
        ),
        array(
            'value'       => '800',
            'label'       => '800 Extra-Bold',
            'src'         => ''
        ),
        array(
            'value'       => '800italic',
            'label'       => '800 Extra-Bold Italic',
            'src'         => ''
        ),
        array(
            'value'       => '900',
            'label'       => '900 Black',
            'src'         => ''
        ),
        array(
            'value'       => '900italic',
            'label'       => '900 Black Italic',
            'src'         => ''
        ),
    );


    /**
     * Custom settings array that will eventually be
     * passes to the OptionTree Settings API Class.
     */
    $tz_1040nightclub_custom_settings = array(
        'contextual_help' => array(
            'content' => array(
                array(
                    'id'      => 'general_help',
                    'title'   => 'General',
                    'content' => esc_html__( '<p>Help content goes here!</p>', '1040nightclub' )
                ),
            ),
            'sidebar' => esc_html__('<p>Sidebar content goes here!</p>', '1040nightclub' )
        ),
        'sections'        => array(
            array(
                'id'    =>  'TzGlobalOption',
                'title' =>  esc_html__('General Options','1040nightclub'),
            ),
            array(
                'id'    => 'logo',
                'title' => esc_html__('Logo & Favicon', '1040nightclub'),
            ),
            array(
                'id'    => 'menu',
                'title' => esc_html__('Menu', '1040nightclub'),
            ),
            array(
                'id'    => 'social_network',
                'title' => esc_html__('Social Network','1040nightclub'),
            ),
            array(
                'id'    => 'color_social_network',
                'title' => esc_html__('Color','1040nightclub'),
            ),
            array(
                'id'    =>  'TZBlog',
                'title' =>  esc_html__('Blog Option', '1040nightclub'),
            ),
            array(
                'id'    =>  'TZSingle',
                'title' =>  esc_html__('Single Option','1040nightclub'),
            ),
            array(
                'id'    =>  'TZEventsCalendar',
                'title' =>  esc_html__('Events Calendar', '1040nightclub'),
            ),
            array(
                'id'    =>  'TzSyle',
                'title' =>  esc_html__('Font Option', '1040nightclub'),
            ),
            array(
                'id'    =>  'TZFamily',
                'title' =>  esc_html__('Family','1040nightclub'),
            ),
            array(
                'id'    =>  'TzCustomCss',
                'title' =>  esc_html__('Custom CSS', '1040nightclub'),
            ),
            array(
                'id'    => '404',
                'title' => esc_html__('404 Page', '1040nightclub'),
            ),
            array(
                'id'    => 'copyright',
                'title' => esc_html__('Copyright', '1040nightclub'),
            ),
            array(
                'id'    => 'music_press',
                'title' => esc_html__('Music Press', '1040nightclub'),
            ),
        ),

        'settings'        => array(

            /* General options */
            array(
                'id'        =>  'nightclub_TzGlobalOptionLoading',
                'label'     =>  esc_html__('Show loading','1040nightclub'),
                'desc'      =>  esc_html__('Show or hide loading','1040nightclub'),
                'std'       =>  '1',
                'type'      =>  'select',
                'section'   =>  'TzGlobalOption',
                'choices'   =>  array(
                    array(
                        'value' =>  1,
                        'label' =>  esc_html__('Show','1040nightclub'),
                    ),
                    array(
                        'value' =>  0,
                        'label' =>  esc_html__('Hide','1040nightclub'),
                    ),

                ),
            ),
            array(
                'id'        => 'nightclub_TzGlobalOptionUploadLoading',
                'label'     => 'Upload images loading',
                'desc'      => 'Upload <a target="_blank" href="http://preloaders.net/">images</a> .gif',
                'std'       => '',
                'type'      => 'upload',
                'section'   => 'TzGlobalOption',
            ),

            array(
                'id'        =>  'nightclub_TzGlobalOption_pagination_home_page',
                'label'     =>  esc_html__('Show or hide pagination template homepage','1040nightclub'),
                'std'       =>  '1',
                'type'      =>  'select',
                'section'   =>  'TzGlobalOption',
                'choices'   =>  array(
                    array(
                        'value' =>  1,
                        'label' =>  esc_html__('Show','1040nightclub'),
                    ),
                    array(
                        'value' =>  0,
                        'label' =>  esc_html__('Hide','1040nightclub'),
                    ),

                ),
            ),

            array(
                'id'        =>  'nightclub_TzGlobalOption_pagination_home_page_check',
                'label'     =>  esc_html__('Pagination template homepage data','1040nightclub'),
                'std'       =>  '1',
                'type'      =>  'select',
                'section'   =>  'TzGlobalOption',
                'choices'   =>  array(
                    array(
                        'value' =>  1,
                        'label' =>  esc_html__('Page checkbox','1040nightclub'),
                    ),
                    array(
                        'value' =>  0,
                        'label' =>  esc_html__('Enter Page ID','1040nightclub'),
                    ),

                ),
            ),

            array(
                'id'        =>  'nightclub_TzGlobalOption_pagination_home_page_data_input',
                'label'     =>  esc_html__('Enter Page ID(separator by comma)','1040nightclub'),
                'desc'     =>  esc_html__('Eg: 1,2,3,4,5','1040nightclub'),
                'std'       =>  '',
                'type'      =>  'text',
                'section'   =>  'TzGlobalOption',
            ),

            array(
                'id'        =>  'nightclub_page_check_menu',
                'label'     => esc_html__('Choose Page Pagination', '1040nightclub'),
                'std'       => '',
                'type'      => 'page-checkbox',
                'section'   => 'TzGlobalOption',
            ),
            /* End General options */

            /* Logo */
            array(
                'id'        => 'nightclub_logo',
                'label'     => esc_html__('Upload Logo', '1040nightclub'),
                'std'       => '',
                'type'      => 'upload',
                'section'   => 'logo',
            ),
            array(
                'id'        => 'nightclub_width_height_logo',
                'label'     => esc_html__('Width & Height Logo','1040nightclub'),
                'std'       => '',
                'type'      => 'dimension',
                'section'   => 'logo',
            ),
            array(
                'id'        => 'nightclub_favicon_onoff',
                'label'     => esc_html__('Enable Favicon', '1040nightclub'),
                'desc'      => esc_html__('Show or hide Favicon', '1040nightclub'),
                'std'       => 'no',
                'type'      => 'select',
                'section'   => 'logo',
                'choices'   => array(
                    array(
                        'value' => 'yes',
                        'label' => esc_html__('Yes', '1040nightclub'),
                        'src'   => ''
                    ),
                    array(
                        'value' => 'no',
                        'label' => esc_html__('No', '1040nightclub'),
                        'src'   => ''
                    )
                ),
            ),
            array(
                'id'        => 'nightclub_favicon',
                'label'     => esc_html__('Upload Favicon Icon','1040nightclub'),
                'desc'      => esc_html__('Please choose an image  to use for favicon.','1040nightclub'),
                'std'       => '',
                'type'      => 'upload',
                'section'   => 'logo',
            ),
            /* End Logo */

            /* Menu */
            array(
                'id'        =>  'nightclub_menu_color_item',
                'label'     => esc_html__('Menu item color', '1040nightclub'),
                'std'       => '',
                'type'      => 'colorpicker',
                'section'   => 'menu',
            ),
            array(
                'id'        =>  'nightclub_menu_color_item_hover',
                'label'     => esc_html__('Menu item color hover', '1040nightclub'),
                'std'       => '',
                'type'      => 'colorpicker',
                'section'   => 'menu',
            ),
            /* End Menu */

            /* Social network */
            array(
                'id'        => 'nightclub_social_network_facebook',
                'label'     => esc_html__('Facebook','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Facebook icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_twitter',
                'label'     => esc_html__('Twitter','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Twitter icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_flickr',
                'label'     => esc_html__('Flickr','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Flickr icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_behance',
                'label'     => esc_html__('Behance','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Behance icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_instagram',
                'label'     => esc_html__('Instagram','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Instagram icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_digg',
                'label'     => esc_html__('Digg','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Digg icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_dribbble',
                'label'     => esc_html__('Dribbble','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Dribbble icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_linkedin',
                'label'     => esc_html__('Linkedin','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Linkedin icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',

            ),
            array(
                'id'        => 'nightclub_social_network_dropbox',
                'label'     => esc_html__('Dropbox','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Dropbox icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_google-plus',
                'label'     => esc_html__('Google Plus','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Google Plus icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_foursquare',
                'label'     => esc_html__('Foursquare','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Foursquare icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_pinterest',
                'label'     => esc_html__('Pinterest','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Pinterest icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_skype',
                'label'     => esc_html__('Skype','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Skype icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_tumblr',
                'label'     => esc_html__('Tumblr','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Tumblr icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network'
            ),
            array(
                'id'        => 'nightclub_social_network_vimeo',
                'label'     => esc_html__('Vimeo','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Vimeo icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        => 'nightclub_social_network_youtube',
                'label'     => esc_html__('Youtube','1040nightclub'),
                'desc'      => esc_html__('Place the link you want and Youtube icon will appear. To remove it, just leave it blank.','1040nightclub'),
                'std'       => '',
                'type'      => 'text',
                'section'   => 'social_network',
            ),
            array(
                'id'        =>  'nightclub_color_social_network',
                'label'     => esc_html__('Social Network Color', '1040nightclub'),
                'std'       => '',
                'type'      => 'colorpicker',
                'section'   => 'color_social_network',
            ),
            array(
                'id'        =>  'nightclub_color_social_network_hover',
                'label'     => esc_html__('Social Network Color Hover', '1040nightclub'),
                'std'       => '',
                'type'      => 'colorpicker',
                'section'   => 'color_social_network',
            ),
            /* End Social network */

            /* Option Blog and Tag and Serach and Author */
            array(
                'id'        => 'TZBlog',
                'label'     => esc_html__('Option', '1040nightclub'),
                'desc'      => '<p>'.esc_html__('Option for page blog and tag and search and author','1040nightclub').'</p>',
                'std'       => '',
                'type'      => 'textblock-titled',
                'section'   => 'TZBlog',
                'rows'      => '',
                'post_type' => '',
                'taxonomy'  => '',
                'class'     => ''
            ),
            array(
                'id'        => 'nightclub_TZBlogSidebar',
                'label'     => esc_html__('Show Sidebar', '1040nightclub'),
                'type'      => 'select',
                'desc'      => '',
                'std'       => 1,
                'section'   => 'TZBlog',
                'choices'   =>  array(
                    array(
                        'value' =>  1,
                        'label' =>  esc_html__('Sidebar Right', '1040nightclub'),
                    ),
                    array(
                        'value' =>  2,
                        'label' =>  esc_html__('Sidebar Left', '1040nightclub'),
                    ),
                    array(
                        'value' =>  3,
                        'label' =>  esc_html__('Sidebar Left And Right', '1040nightclub'),
                    ),
                    array(
                        'value' =>  0,
                        'label' =>  esc_html__('Hide Sidebar', '1040nightclub'),
                    ),
                ),

            ),
            /* End Option Blog and Tag and Serach and Author */

            /* Single page */
            array(
                'id'        => 'TZSingle',
                'label'     => esc_html__('Option', '1040nightclub'),
                'desc'      => '<p>'.esc_html__('Option for page single post.','1040nightclub').'</p>',
                'std'       => '',
                'type'      => 'textblock-titled',
                'section'   => 'TZSingle',
            ),
            array(
                'id'        => 'nightclub_text_single',
                'label'     => esc_html__('Text Single','1040nightclub'),
                'std'       => esc_html__( 'Blog Details', '1040nightclub' ),
                'type'      => 'text',
                'section'   => 'TZSingle',
            ),
            array(
                'id'        =>  'nightclub_single_time_comment',
                'label'     => esc_html__('Time Comment', '1040nightclub'),
                'desc'      =>  '',
                'sdt'       =>  0,
                'type'      =>  'select',
                'section'   => 'TZSingle',
                'choices'   =>  array(
                    array(
                        'value' =>  0,
                        'label' =>  esc_html__('Time Ago', '1040nightclub'),
                    ),
                    array(
                        'value' =>  1,
                        'label' =>  esc_html__('Time format ', '1040nightclub'),
                    )
                )
            ),
            array(
                'id'        =>  'nightclub_singlesidebar',
                'label'     => esc_html__('Show or hide sidebar', '1040nightclub'),
                'desc'      =>  '',
                'sdt'       =>  0,
                'type'      =>  'select',
                'section'   => 'TZSingle',
                'choices'   =>  array(
                    array(
                        'value' =>  0,
                        'label' =>  esc_html__('Hide', '1040nightclub'),
                    ),
                    array(
                        'value' =>  1,
                        'label' =>  esc_html__('Show', '1040nightclub'),
                    )
                )
            ),
            array(
                'id'        =>'nightclub_tzshowdate',
                'label'     =>esc_html__('Show Date','1040nightclub'),
                'type'      =>'select',
                'std'=>'',
                'section'=>'TZSingle',
                'choices'=>array(
                    array(
                        'value'=>   1,
                        'label'=>   esc_html__('Yes','1040nightclub'),
                    ),
                    array(
                        'value'=>   0,
                        'label'=>   esc_html__('No','1040nightclub'),
                    ),

                )

            ),
            array(
                'id'        =>'nightclub_tzshowtag',
                'label'     =>esc_html__('Show Tag','1040nightclub'),
                'type'      =>'select',
                'std'       =>'',
                'section'   =>'TZSingle',
                'choices'   =>array(
                    array(
                        'value'=>   1,
                        'label'=>   esc_html__('Yes','1040nightclub'),
                    ),
                    array(
                        'value'=>   0,
                        'label'=>   esc_html__('No','1040nightclub'),
                    ),
                )
            ),
            array(
                'id'        =>'nightclub_tzshowshare',
                'label'     =>esc_html__('Show Share','1040nightclub'),
                'type'      =>'select',
                'std'=>'',
                'section'=>'TZSingle',
                'choices'=>array(
                    array(
                        'value'=>   1,
                        'label'=>   esc_html__('Yes','1040nightclub'),
                    ),
                    array(
                        'value'=>   0,
                        'label'=>   esc_html__('No','1040nightclub'),
                    ),

                )

            ),
            array(
                'id'        =>  'nightclub_tzshowcomment',
                'label'     =>  esc_html__('Show Comment','1040nightclub'),
                'type'      =>  'select',
                'std'       =>  '',
                'section'   =>  'TZSingle',
                'choices'   =>  array(
                    array(
                        'value' =>  1,
                        'label' =>  esc_html__('Yes','1040nightclub'),
                    ),
                    array(
                        'value' =>  0,
                        'label' =>  esc_html__('No','1040nightclub'),
                    ),

                )

            ),
            /* End Single page */

            /* Events Calendar */
            array(
                'id'        => 'nightclub_event_calendar_text_single',
                'label'     => esc_html__('Text Single Event','1040nightclub'),
                'desc'      => esc_html__('Using for Single Event','1040nightclub'),
                'std'       => 'Event Details',
                'type'      => 'text',
                'section'   => 'TZEventsCalendar',
            ),
            array(
                'id'        => 'nightclub_event_calendar_bk',
                'label'     => esc_html__('Upload Background Image', '1040nightclub'),
                'desc'      => esc_html__('Background Image using for Month View Page', '1040nightclub'),
                'std'       => '',
                'type'      => 'upload',
                'class'     => 'ot-upload-attachment-id',
                'section'   => 'TZEventsCalendar',
            ),
            array(
                'id'        => 'nightclub_event_calendar_title',
                'label'     => esc_html__('Title Event','1040nightclub'),
                'desc'      => esc_html__('Using for Month View Page','1040nightclub'),
                'std'       => 'Next Event',
                'type'      => 'text',
                'section'   => 'TZEventsCalendar',
            ),
            array(
                'id'        => 'nightclub_event_calendar_sub_title',
                'label'     => esc_html__('Sub Title Event','1040nightclub'),
                'desc'      => esc_html__('Using for Month View Page','1040nightclub'),
                'std'       => 'Upcoming Event',
                'type'      => 'text',
                'section'   => 'TZEventsCalendar',
            ),
            /* End Events Calendar */

            /* style option */
            array(
                'id'        =>  'nightclub_TzSyle',
                'label'     => esc_html__('StyleConfig', '1040nightclub'),
                'desc'      => esc_html__('Config for Content, Main Menu, Big Headings, Small Headings, custom style','1040nightclub'),
                'std'       => '',
                'type'      => 'textblock-titled',
                'section'   => 'TzSyle',
            ),

            array(
                'id'        =>  'nightclub_select_font_theme',
                'label'     =>  esc_html__('Choose Use Fonts','1040nightclub'),
                'desc'      =>  esc_html__('option font type','1040nightclub'),
                'std'       =>  1,
                'type'      =>  'select',
                'section'   =>  'TZFamily',
                'choices'   =>  array(
                    array(
                        'value' =>  1,
                        'label' =>  'Default',
                    ),
                    array(
                        'value' =>  2,
                        'label' =>  'Font Family',
                    ),
                    array(
                        'value' =>  3,
                        'label' =>  'Custom Fonts vs CSS',
                    ),

                ),
            ),
            array(
                'id'        =>  'nightclub_font_text_family',
                'label'     => esc_html__('Font Family','1040nightclub'),
                'std'       => '',
                'type'      => 'textblock-titled',
                'section'   => 'TZFamily',
                'rows'      => '',
                'post_type' => '',
                'taxonomy'  => '',
                'class'     => 'tz-text-font-family'
            ),
            array(
                'id'       =>   'nightclub_font_content',
                'label'    =>   esc_html__('Content','1040nightclub'),
                'desc'     =>   esc_html__('All theme texts except headings and menu','1040nightclub'),
                'type'     =>   'select',
                'std'       =>  'Raleway',
                'section'  =>   'TZFamily',
                'class'    =>   'TzFontStylet',
                'choices'  =>   $tz_1040nightclub_font_option,
            ),
            array(
                'id'       =>   'nightclub_font_menu',
                'label'    =>   esc_html__('Main Menu','1040nightclub'),
                'desc'     =>   esc_html__('Header menu','1040nightclub'),
                'type'     =>   'select',
                'std'       =>  'Raleway',
                'section'  =>   'TZFamily',
                'class'    =>   'TzFontStylet',
                'choices'  =>   $tz_1040nightclub_font_option,
            ),
            array(
                'id'       =>   'nightclub_font_headings',
                'label'    =>   esc_html__('Big Headings','1040nightclub'),
                'desc'     =>   esc_html__('H1, H2, H3 & H4 headings','1040nightclub'),
                'type'     =>   'select',
                'std'       =>  'Raleway',
                'section'  =>   'TZFamily',
                'class'    =>   'TzFontStylet',
                'choices'  =>   $tz_1040nightclub_font_option,
            ),
            array(
                'id'       =>   'nightclub_font_headings_small',
                'label'    =>   esc_html__('Small Headings','1040nightclub'),
                'desc'     =>   esc_html__('H5 & H6 headings','1040nightclub'),
                'type'     =>   'select',
                'std'       =>  'Raleway',
                'section'  =>   'TZFamily',
                'class'    =>   'TzFontStylet',
                'choices'  =>   $tz_1040nightclub_font_option,
            ),
            array(
                'id'        =>  'nightclub_font_info_google',
                'label'     => esc_html__('Google Fonts','1040nightclub'),
                'std'       => '',
                'type'      => 'textblock-titled',
                'section'   => 'TZFamily',
                'rows'      => '',
                'post_type' => '',
                'taxonomy'  => '',
                'class'     => 'tz-text-font-family'
            ),
            array(
                'id'          => 'nightclub_font_weight',
                'label'       => esc_html__( 'Google Fonts Style & Weight', '1040nightclub' ),
                'std'         => '',
                'type'        => 'checkbox',
                'section'     => 'TZFamily',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => 'tz-check-font-weight',
                'operator'    => 'and',
                'choices'     => $tz_1040nightclub_font_option_style,
            ),
            array(
                'label'     =>  esc_html__('Google Fonts Subset','1040nightclub'),
                'id'        => 'nightclub_font_subset',
                'type'      => 'text',
                'desc'      =>   esc_html__('Specify which subsets should be downloaded. Multiple subsets should be separated with coma (,)','1040nightclub'),
                'class'     => '',
                'section'   => 'TZFamily',
            ),
            /* End style option */

            /* custom css */
            array(
                 'id'        =>  'nightclub_TzCustomCss',
                 'label'     =>  esc_html__('Code CSS', '1040nightclub'),
                 'desc'      =>  esc_html__('Paste your CSS code, do not include any tags or HTML in thie field. Any custom CSS entered here will override the theme CSS. In some cases, the !important tag may be needed.', '1040nightclub'),
                 'std'       =>  '',
                 'type'      => 'css',
                 'section'   => 'TzCustomCss',
            ),
            /* End custom css */

            /* 404 */
            array(
                'id'        => 'nightclub_404_bk',
                'label'     => esc_html__('404 Background', '1040nightclub'),
                'desc'      => '',
                'std'       => '',
                'type'      => 'upload',
                'section'   => '404',
                'rows'      => '',
                'post_type' => '',
                'taxonomy'  => '',
                'class'     => ''
            ),
            array(
                'id'    =>  'nightclub_title_page_404',
                'label' =>  esc_html__('Title 404', '1040nightclub'),
                'std'   =>  esc_html__( 'Page not found', '1040nightclub' ),
                'type'  =>  'text',
                'section'=> '404',
            ),
            array(
                'id'        => 'nightclub_404_page_content',
                'label'     => esc_html__('404 Page Content','1040nightclub'),
                'desc'      => '',
                'std'       => '',
                'type'      => 'textarea',
                'section'   => '404',
                'rows'      => '15',
                'post_type' => '',
                'taxonomy'  => '',
                'class'     => ''
            ),
            array(
                'id'    =>  'nightclub_text_go_home',
                'label' =>  esc_html__('Text Button Go Home', '1040nightclub'),
                'std'   =>  esc_html__( 'Come back to home', '1040nightclub' ),
                'type'  =>  'text',
                'section'=> '404',
            ),
            /* End 404 */

            /* Copyright Settings */
            array(
                'id'        => 'nightclub_copyright',
                'label'     => esc_html__('Copyright','1040nightclub'),
                'desc'      => '',
                'std'       => 'Copyright &copy; Templaza',
                'type'      => 'textarea',
                'section'   => 'copyright',
                'rows'      => '15',
                'post_type' => '',
                'taxonomy'  => '',
                'class'     => ''
            ),
            /* End Copyright Settings */

            /* Music Press */
            array(
                'id'        => 'nightclub_music_press',
                'label'     => esc_html__('Music Press','1040nightclub'),
                'std'       => '',
                'type'      => 'textarea',
                'section'   => 'music_press',
            ),
            /* End Music Press */

        ) // end setting
    );

    /* allow settings to be filtered before saving */

    $tz_1040nightclub_custom_settings = apply_filters('option_tree_settings_args', $tz_1040nightclub_custom_settings);

    /* settings are not the same update the DB */
    if ($tz_1040nightclub_saved_settings !== $tz_1040nightclub_custom_settings) {
        update_option('option_tree_settings', $tz_1040nightclub_custom_settings);
    }

}


?>
