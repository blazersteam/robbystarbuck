<?php
    /*
     * Method process option
     * # option 1: config font
     * # option 2: process config theme
    */
    if(!is_admin()):


        add_action('wp_enqueue_scripts','tz_1040nightclub_config_theme',99);

        function tz_1040nightclub_config_theme(){

            $styles ='';
            $styles.='/**
            * 1.0 - Reset
            * -----------------------------------------------------------------------------
            */
            ';

            /* Custom menu */
            $tz_1040nightclub_menu_color_item       =   ot_get_option( 'nightclub_menu_color_item', '' );
            $nightclub_menu_color_item_hover        =   ot_get_option( 'nightclub_menu_color_item_hover', '' );

            if ( $tz_1040nightclub_menu_color_item != '' ) :
                $styles.='.tz_nav_menu li a, .menu__breadcrumbs a { color: '. esc_attr( $tz_1040nightclub_menu_color_item ) .'; }';
            endif;

            if ( $nightclub_menu_color_item_hover != '' ) :
                $styles.='.tz_nav_menu li a:hover { color: '. esc_attr( $nightclub_menu_color_item_hover ) .'; }';
            endif;
            /* End Custom menu */

            /* width & height logo */
            $tz_1040nightclub_width_height_logo =   ot_get_option( 'nightclub_width_height_logo','' );

            if ( $tz_1040nightclub_width_height_logo !='' ) {
                $styles.='.tz_logo img { width: '. esc_attr( $tz_1040nightclub_width_height_logo['width'] .$tz_1040nightclub_width_height_logo['unit']) .'; height: '. esc_attr( $tz_1040nightclub_width_height_logo['height'].$tz_1040nightclub_width_height_logo['unit'] ) .'; }';
            }
            /* End width & height logo */

            /* Custom social network */
            $tz_1040nightclub_color_social_network          =   ot_get_option( 'nightclub_color_social_network','' );
            $tz_1040nightclub_color_social_network_hover    =   ot_get_option( 'nightclub_color_social_network_hover','' );

            if ( $tz_1040nightclub_color_social_network != '' ) :
                $styles.='.tz_social_network_item_footer { color: '. esc_attr( $tz_1040nightclub_color_social_network ) .'; }';
            endif;

            if ( $tz_1040nightclub_color_social_network_hover != '' ) :
                $styles.='.tz_social_network_item_footer:hover { color: '. esc_attr( $tz_1040nightclub_color_social_network_hover ) .'; }';
            endif;
            /* End Custom social network */

            /*   Fonts-option   */
            $tz_1040nightclub_on_off_google_font   =  ot_get_option( 'nightclub_select_font_theme', 1 ) ;

            if ( $tz_1040nightclub_on_off_google_font == 2 ) :

                $tz_1040nightclub_font_content         = str_replace( '#', '', ot_get_option( 'nightclub_font_content','Raleway' ) );
                $tz_1040nightclub_font_menu            = str_replace( '#', '', ot_get_option( 'nightclub_font_menu','Raleway' ) );
                $tz_1040nightclub_font_headings        = str_replace( '#', '', ot_get_option( 'nightclub_font_headings','Raleway' ) );
                $tz_1040nightclub_font_headings_small  = str_replace( '#', '', ot_get_option( 'nightclub_font_headings_small','Raleway' ) );

                $styles.='body#bd, button, input[type="submit"], input[type="reset"], input[type="button"], input[type="text"], input[type="password"], input[type="tel"], input[type="email"], textarea, select, .tz-form-search input.Tzsearchform, .tz_page_pagination a.tz_pagination_id span.tz_pagination_title, .tz_button_menu span.tz_button_menu_text, .tz_content_slider_box_post .tz_date_post, .tz_reservation_form .tz_btn_submit input, .vc_general.vc_tta-shape-rounded .vc_tta-tabs-list .vc_tta-tab a, .tz_footer_music_press .jp-audio .jp-type-playlist .jp-title, .tz_title_more_video a.playlist-btn, span.tz_partner_prev_next, span.tz_partner_prev_next { font-family: '. esc_attr( $tz_1040nightclub_font_content ) .'; }';

                $styles.='.tz_nav_menu li a, .menu__breadcrumbs a { font-family: '. esc_attr( $tz_1040nightclub_font_menu ) .'; }';

                $styles.='h1, h2, h3, h4, .tz_element_title .tz_title { font-family: '. esc_attr( $tz_1040nightclub_font_headings ) .'; font-weight: 400; }';

                $styles.='.side-newsfeed .side-item-text h4, h3.module-title, .tz_post_related h3 { font-weight: 400; }';

                $styles.='h5, h6 { font-family: '. esc_attr( $tz_1040nightclub_font_headings_small ) .'; font-weight: 400; }';

            endif;
            /*   End-Fonts-option   */

            /* bk 404 */
            $tz_1040nightclub_bk_img_404 = ot_get_option('nightclub_404_bk','');

            if ( $tz_1040nightclub_bk_img_404 !='' ) :
                $styles.='.tz_error {  background-image: url("'.esc_url( $tz_1040nightclub_bk_img_404 ).'"); }';
            endif;

            /* add Custom css */
            $tz_1040nightclub_customcss = ot_get_option('nightclub_TzCustomCss','');
            if( isset( $tz_1040nightclub_customcss ) && !empty( $tz_1040nightclub_customcss ) ){
                $styles.= balanceTags( $tz_1040nightclub_customcss );
            }
            /* End Custom css */

        ?>

        <?php

            if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
                if(ot_get_option( 'nightclub_favicon_onoff','no') == 'yes'){
                    $tz_1040nightclub_favicon = ot_get_option('nightclub_favicon','');
                    if( $tz_1040nightclub_favicon ){
                        echo '<link rel="shortcut icon" href="' . esc_url($tz_1040nightclub_favicon) . '" type="image/x-icon" />';
                    }
                }
            }

        ?>

    <?php

            if (!empty($styles))
                wp_add_inline_style( 'tz-1040nightclub-custom-style', $styles);
    }

    endif;

?>