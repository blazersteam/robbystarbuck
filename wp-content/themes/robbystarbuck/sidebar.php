
<div class="tz_nightclub_sidebar">

    <?php

        if ( is_active_sidebar( 'nightclub-sidebar-main' ) ) :

            dynamic_sidebar('nightclub-sidebar-main');

        endif;

    ?>

</div>
