<?php

get_header();
get_template_part('template_inc/inc','menu');

if ( class_exists('Tribe__Events__Main') && class_exists( 'Tribe__Events__Pro__Main' ) ):

    if (  is_singular( 'tribe_events' ) || ( tribe_is_month() && !is_tax() ) || ( tribe_is_month() && is_tax() ) ) {

        $tz_1040nightclub_event =   'tz_no_container';

    }elseif ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || ( tribe_is_week() && !is_tax() ) || ( tribe_is_day() && !is_tax() ) || ( tribe_is_map() && !is_tax() ) || ( tribe_is_photo() && !is_tax() ) || ( tribe_is_past() || tribe_is_upcoming() && is_tax() ) || ( tribe_is_week() && is_tax() ) || ( tribe_is_day() && is_tax() ) || ( tribe_is_map() && is_tax() ) || ( tribe_is_photo() && is_tax() ) ) {

        $tz_1040nightclub_event =   'tz_no_container tz_no_container_event_list';

    } else{

        $tz_1040nightclub_event =   'tz_cat_padding';
    }

elseif ( class_exists( 'Tribe__Events__Main' ) && ! class_exists( 'Tribe__Events__Pro__Main' ) ) :

    if (  is_singular( 'tribe_events' ) || ( tribe_is_month() && !is_tax() ) || ( tribe_is_month() && is_tax() ) ) {

        $tz_1040nightclub_event =   'tz_no_container';

    }elseif ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || ( tribe_is_day() && !is_tax() ) || ( tribe_is_past() || tribe_is_upcoming() && is_tax() ) || ( tribe_is_day() && is_tax() ) ) {

        $tz_1040nightclub_event =   'tz_no_container tz_no_container_event_list';

    } else{

        $tz_1040nightclub_event =   'tz_cat_padding';
    }

else:
    $tz_1040nightclub_event =   'tz_cat_padding';
endif;

?>

<?php

if ( class_exists('Tribe__Events__Main') && class_exists( 'Tribe__Events__Pro__Main' ) ):

    if ( ( tribe_is_month() && !is_tax() ) || ( tribe_is_past() || tribe_is_upcoming() && !is_tax() ) || ( tribe_is_week() && !is_tax() ) || ( tribe_is_day() && !is_tax() ) || ( tribe_is_map() && !is_tax() ) || ( tribe_is_photo() && !is_tax() ) || ( tribe_is_month() && is_tax() ) || ( tribe_is_past() || tribe_is_upcoming() && is_tax() ) || ( tribe_is_week() && is_tax() ) || ( tribe_is_day() && is_tax() ) || ( tribe_is_map() && is_tax() ) || ( tribe_is_photo() && is_tax() ) ) :

        $tz_1040nightclub_bk_event          =   ot_get_option( 'nightclub_event_calendar_bk','' );
        $tz_1040nightclub_title_event       =   ot_get_option( 'nightclub_event_calendar_title','Next Event' );
        $tz_1040nightclub_sub_title_event   =   ot_get_option( 'nightclub_event_calendar_sub_title','Upcoming Event' );
?>

        <section class="tz_tribe_events_image">

            <?php

                if ( $tz_1040nightclub_bk_event !== '' ) {
                    echo wp_get_attachment_image( $tz_1040nightclub_bk_event, 'full' );
                }else {
                    echo '<img src="'.get_template_directory_uri().'/images/bk-single-event.png" alt="'.get_bloginfo('title').'" />';
                }

            ?>

            <div class="tz_box_tile_event">
                <div class="ds-table">
                    <div class="ds-table-cell-left-bottom">
                        <h5 class="tz_tribe_events_month_title">
                            <?php echo esc_attr( $tz_1040nightclub_title_event ); ?>
                        </h5>
                        <h3 class="tz_tribe_events_month_sub_title">
                            <?php echo esc_attr( $tz_1040nightclub_sub_title_event ); ?>
                        </h3>
                    </div>
                </div>
            </div>
        </section>

<?php
    endif;

elseif ( class_exists( 'Tribe__Events__Main' ) && ! class_exists( 'Tribe__Events__Pro__Main' ) ):

    if ( ( tribe_is_month() && !is_tax() ) || ( tribe_is_past() || tribe_is_upcoming() && !is_tax() )  || ( tribe_is_day() && !is_tax() )  || ( tribe_is_month() && is_tax() ) || ( tribe_is_past() || tribe_is_upcoming() && is_tax() ) || ( tribe_is_day() && is_tax() ) ) :

        $tz_1040nightclub_bk_event          =   ot_get_option( 'nightclub_event_calendar_bk','' );
        $tz_1040nightclub_title_event       =   ot_get_option( 'nightclub_event_calendar_title','Next Event' );
        $tz_1040nightclub_sub_title_event   =   ot_get_option( 'nightclub_event_calendar_sub_title','Upcoming Event' );


?>

        <section class="tz_tribe_events_image">

            <?php

                if ( $tz_1040nightclub_bk_event !== '' ) {
                    echo wp_get_attachment_image( $tz_1040nightclub_bk_event, 'full' );
                }else {
                    echo '<img src="'.get_template_directory_uri().'/images/bk-single-event.png" alt="'.get_bloginfo('title').'" />';
                }

            ?>

            <div class="tz_box_tile_event">
                <div class="ds-table">
                    <div class="ds-table-cell-left-bottom">
                        <h5 class="tz_tribe_events_month_title">
                            <?php echo esc_attr( $tz_1040nightclub_title_event ); ?>
                        </h5>
                        <h3 class="tz_tribe_events_month_sub_title">
                            <?php echo esc_attr( $tz_1040nightclub_sub_title_event ); ?>
                        </h3>
                    </div>
                </div>
            </div>
        </section>

<?php

    endif;

endif;
?>

<section class="<?php echo esc_attr( $tz_1040nightclub_event ); ?>">

    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

        <?php while (have_posts()) : the_post() ; ?>

            <div <?php post_class('tz_page_content') ?>>

                <?php
                the_content();
                wp_link_pages( array(
                    'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', '1040nightclub' ) . '</span>',
                    'after'       => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                ) );
                ?>

            </div>

        <?php
             comments_template();
        endwhile;
        ?>
</section>

<?php

get_template_part('template_inc/inc','footer-home-page');
get_footer();

?>

