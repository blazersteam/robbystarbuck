<?php

get_header();

$tz_1040nightclub_title_page_404    =   ot_get_option('nightclub_title_page_404','Page not found');
$tz_1040nightclub_content           =   ot_get_option('nightclub_404_page_content');
$tz_1040nightclub_text_go_home      =   ot_get_option( 'nightclub_text_go_home', 'Come back to home' );

?>

<section class="tz_error">
    <div class="ds-table-cell">
        <div class="tz_content_404">

            <?php if ( $tz_1040nightclub_title_page_404 ) : ?>
                <h1 class="tz_title_page_404">
                    <?php echo balanceTags( $tz_1040nightclub_title_page_404 ); ?>
                </h1>
            <?php endif; ?>

            <?php if ( $tz_1040nightclub_content !='' ) : ?>

                <?php echo balanceTags( $tz_1040nightclub_content ); ?>

                <a class="tz_come_back_home" href="<?php echo esc_url(get_home_url('/')); ?>">
                    <?php echo esc_attr( $tz_1040nightclub_text_go_home ); ?>
                </a>

            <?php else: ?>

                <div class="tz_bk_404">
                    <img src="<?php echo get_template_directory_uri() ?>/images/bk-title-404.png" alt="<?php get_bloginfo('title'); ?>">
                    <p>
                        <?php esc_html_e('Oops! Sorry, we\'re not able to find what you looking were for you.', '1040nightclub'); ?>
                    </p>
                    <a class="tz_come_back_home" href="<?php echo esc_url(get_home_url('/')); ?>">
                        <?php echo esc_attr( $tz_1040nightclub_text_go_home ); ?>
                    </a>
                </div>

            <?php endif; ?>

        </div>
    </div>
</section>

<?php

get_footer();

?>