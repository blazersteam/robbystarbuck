<?php
/**
 * The template for displaying search forms in Twenty Eleven
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
	<form method="get" id="searchform" action="<?php echo esc_url( get_home_url( '/' ) ); ?>">
		<label for="s" class="assistive-text assistive-tzsearch"><?php esc_attr_e( 'Search', '1040nightclub' ); ?></label>
		<input type="text" class="field Tzsearchform inputbox search-query" name="s"  placeholder="<?php esc_attr_e( 'Search...', '1040nightclub' ); ?>" />
		<input type="submit" class="submit searchsubmit" name="submit" value="<?php esc_attr_e( 'Search', '1040nightclub' ); ?>" />
	</form>
