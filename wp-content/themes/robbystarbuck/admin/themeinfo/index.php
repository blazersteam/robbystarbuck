<?php

global $wpdb;

/*
|--------------------------------------------------------------------------
| Add to Appearance Menu
|--------------------------------------------------------------------------
*/ 
add_action('admin_menu', 'nightclub_theme_info_page');

function nightclub_theme_info_page() {
    
	global $theme_path;
    add_theme_page('1040nightclub'.' Nightclub Info', 'Nightclub Info','install_themes', 'view_info' , 'nightclub_view_info');
}

/*
|--------------------------------------------------------------------------
| Output
|--------------------------------------------------------------------------
*/ 
function nightclub_view_info() { ?>

<div class="wrap">
	
    <div class="icon32" id="icon-options-general"><br></div>
    <h2><?php  esc_html_e( 'Nightclub Info' , '1040nightclub' ); ?></h2>
	<h3 class="title"><?php  esc_html_e( 'Please paste down these information when starting a support inquiry in our supportforum' , '1040nightclub' ); ?></h3>
	
    <table class="form-table">
    <tbody>
    
    <tr valign="top">
        <th scope="row">WordPress Version:</th>
        <td> <?php echo esc_attr(get_bloginfo('version')); ?> </td>
    </tr>
    
    <tr valign="top">
        <th scope="row">URL:</th>
        <td> <?php echo esc_url(site_url()); ?> </td>
    </tr>
    
    <tr valign="top">
        <th scope="row">Installed Theme:</th>
        <td> <?php echo esc_attr('1040nightclub'); ?> </td>
    </tr>

    <?php $nightclub_theme = wp_get_theme( ); ?>
    <tr valign="top">
        <th scope="row">Theme Version:</th>
        <td> <?php echo esc_html($nightclub_theme->version); ?></td>
    </tr>
   
   	<tr valign="top">
        <th scope="row">PHP Version:</th>
        <td> <?php echo esc_attr(phpversion()); ?> </td>
    </tr>
    
    <?php if( is_array(get_option( 'active_plugins' ))) : ?>
    <tr valign="top">
        <th scope="row">Installed Plugins:</th>
        <td>
        
        <ul>
        <?php foreach(get_option( 'active_plugins' ) as $plugin) {
                echo '<li>'.esc_attr($plugin).'</li>';
        } ?>
        </ul>
        
        </td>
    </tr>
    <?php endif; ?>
        
    </tbody></table>
        
</div>

<?php } ?>