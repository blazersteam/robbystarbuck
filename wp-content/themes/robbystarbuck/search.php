<?php

get_header();

//  Blog options show hide

$tz_1040nightclub_blog_sidebar      =   ot_get_option( 'nightclub_TZBlogSidebar',1 );

$tz_night_club_column   =   'col-lg-9 col-md-9';
if ( $tz_1040nightclub_blog_sidebar == 0 ) :
    $tz_night_club_column = 'col-lg-12 col-md-12';
endif;

get_template_part('template_inc/inc','menu');

?>

<section class="home-post tz_cat_padding">
    <div class="row">

        <?php if ( $tz_1040nightclub_blog_sidebar == 2 ) : ?>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <?php get_sidebar(); ?>
            </div>

        <?php endif; ?>

        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

            <h1 class="tzarchive tz_nightclub_title_cat">
                <?php echo  esc_html(get_search_query()) ; ?>
            </h1>

            <?php

            if ( have_posts() ) : while (have_posts()) : the_post() ;

                $tz_1040nightclub_post_type = get_post_type( $post -> ID );

                if ( $tz_1040nightclub_post_type !='page' ) :

            ?>

                    <article id='post-<?php the_ID(); ?>' class="post-item">

                        <?php the_post_thumbnail( 'full' ); ?>

                        <h3>
                            <a href="<?php the_permalink() ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>

                        <?php if ( has_category() ): ?>

                            <div class="tz_single_tag">
                                <span class="tz_icon_single">
                                    <i class="fa fa-folder" aria-hidden="true"></i>
                                    <?php the_category( ', ' ); ?>
                                </span>
                            </div>

                        <?php endif; ?>

                        <?php if ( get_the_tags () !=  false ): ?>

                            <div class="tz_single_tag">
                            <span class="tz_icon_single">
                                <i class="fa fa-tags fa-rotate-90" aria-hidden="true"></i><?php the_tags('',' '); ?>
                            </span>
                            </div>

                        <?php endif; ?>

                        <div class="description">
                            <?php the_excerpt(); ?>
                        </div>

                    </article>

                <?php else: ?>

                    <h1 class="tz_title_post_type_page">
                        <a href="<?php the_permalink() ?>">
                            <?php  the_title(); ?>
                        </a>
                    </h1>

            <?php

                    endif;
                endwhile; // end while ( have_posts )
                else:

            ?>
                <div class="tz_nightclub_search_not_data">
                    <h5>
                        <?php esc_html_e('No Data', '1040nightclub') ; ?>
                    </h5>
                    <?php get_search_form(); ?>
                    <a href="<?php echo esc_url(get_home_url('/')); ?>">
                        <?php esc_html_e('Go to Home','1040nightclub'); ?>
                    </a>
                </div>
            <?php

            endif; // end if ( have_posts )
            tz_1040nightclub_paging_nav();

            ?>

        </div>

        <?php if ( $tz_1040nightclub_blog_sidebar == 1 ) : ?>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <?php get_sidebar(); ?>
            </div>

        <?php endif; ?>

    </div>
</section>

<?php

get_template_part('template_inc/inc','footer-home-page');
get_footer();

?>

