<?php
/**
 * Template Name: Template Homepage
 */

get_header();

$tz_1040nightclub_pagination_home_page  =   ot_get_option( 'nightclub_TzGlobalOption_pagination_home_page', 1 );

$tz_1040nightclub_hide_show_bk_image    =   get_post_meta( get_the_ID(), 'nightclub_hide_show_bk_image', true );
$tz_1040nightclub_bk_home_page          =   get_post_meta( get_the_ID(), 'nightclub_bk_image_page_home', true );

?>

<div class="tz_template_homepage" data-page-id="<?php the_ID(); ?>">

    <?php if ( $tz_1040nightclub_bk_home_page !='' && $tz_1040nightclub_hide_show_bk_image == 1 ) : ?>

        <img class="tz_bk_image_home_page" src="<?php echo esc_attr( $tz_1040nightclub_bk_home_page ); ?>" alt="<?php the_title(); ?>">

    <?php endif; ?>

    <?php
        if( have_posts() ):
            // Start the Loop.
            while( have_posts() ): the_post();

                the_content();
                wp_link_pages( array(
                    'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', '1040nightclub' ) . '</span>',
                    'after'       => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                ) );

            endwhile;

            if ( $tz_1040nightclub_pagination_home_page == 1 ) :

                tz_1040nightclub_getPrevNext();

            endif;

        endif;
    ?>
</div>

<?php
get_template_part('template_inc/inc','footer-home-page');
get_footer();
?>