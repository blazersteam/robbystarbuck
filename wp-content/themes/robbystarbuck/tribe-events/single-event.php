<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.2.4
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

global $post;

$tz_1040nightclub_event_image					=	get_post_meta( $post -> ID, 'nightclub_tribe_events_image', true );
$tz_1040nightclub_event_calendar_text_single	=	ot_get_option( 'nightclub_event_calendar_text_single', 'Event Details' );

?>

<div class="tz_nightclub_event_image">
	<div class="tz_nightclub_event_bk"></div>

	<?php if ( $tz_1040nightclub_event_image !='' ) : ?>

		<img src="<?php echo esc_url( $tz_1040nightclub_event_image ); ?>" alt="<?php the_title(); ?>">

	<?php else: ?>

		<img src="<?php echo get_template_directory_uri(); ?>/images/bk-single-event.png" alt="<?php the_title(); ?>">

	<?php endif; ?>

	<div class="tz_box_tile_event">
		<div class="ds-table">
			<div class="ds-table-cell-left-bottom">
				<div class="tz_nightclub_event_box_title">
					<?php the_title( '<h1 class="tribe-events-single-event-title tz_title_single_event">', '</h1>' ); ?>
					<h1 class="tz_nightclub_event_sub_title">
						<?php echo balanceTags( $tz_1040nightclub_event_calendar_text_single ); ?>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="tz_tribe_events_content">
	<div class="tz_tribe_events_date">

		<?php
		$tz_1040nightclub_date = tribe_get_start_date( null, false, 'M d, Y' );
		$tz_1040nightclub_start_datetime = tribe_get_start_date(null, false, 'g:i a');
		$tz_1040nightclub_end_datetime = tribe_get_end_date(null, false, 'g:i a');
		?>
		<span>
			<i class="lnr lnr-calendar-full"></i>
			<?php echo esc_attr( $tz_1040nightclub_date ); ?>
		</span>
		<span class="tz_tribe_events_address">
			<i class="lnr linea-basic-basic-geolocalize-01"></i>
			<?php echo tribe_get_full_address(); ?>
		</span>
		<span class="tz_tribe_events_clock">
			<i class="lnr lnr-clock"></i>
			<?php echo esc_attr( $tz_1040nightclub_start_datetime ) . ' - ' . esc_attr( $tz_1040nightclub_end_datetime ) ; ?>
		</span>

	</div>
	<div class="tz_tribe_events_box_content">
		<div id="tribe-events-content" class="tribe-events-single">

			<p class="tribe-events-back">
				<a href="<?php echo esc_url( tribe_get_events_link() ); ?>"> <?php printf( '&laquo; ' . esc_html__( 'All %s', '1040nightclub' ), $events_label_plural ); ?></a>
			</p>

			<!-- Notices -->
			<?php tribe_the_notices() ?>

			<!-- Event header -->
			<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>
				<!-- Navigation -->
				<h3 class="tribe-events-visuallyhidden"><?php printf( esc_html__( '%s Navigation', '1040nightclub' ), $events_label_singular ); ?></h3>
				<ul class="tribe-events-sub-nav">
					<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
					<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
				</ul>
				<!-- .tribe-events-sub-nav -->
			</div>
			<!-- #tribe-events-header -->

			<?php while ( have_posts() ) :  the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<!-- Event featured image, but exclude link -->
					<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>

					<!-- Event content -->
					<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
					<div class="tribe-events-single-event-description tribe-events-content">
						<?php the_content(); ?>
					</div>
					<!-- .tribe-events-single-event-description -->
					<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

					<div class="clearfix"></div>

					<!-- Event meta -->
					<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
					<?php tribe_get_template_part( 'modules/meta' ); ?>
					<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
				</div> <!-- #post-x -->
				<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
			<?php endwhile; ?>

			<!-- Event footer -->
			<div id="tribe-events-footer">
				<!-- Navigation -->
				<h3 class="tribe-events-visuallyhidden"><?php printf( esc_html__( '%s Navigation', '1040nightclub' ), $events_label_singular ); ?></h3>
				<ul class="tribe-events-sub-nav">
					<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
					<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
				</ul>
				<!-- .tribe-events-sub-nav -->
			</div>
			<!-- #tribe-events-footer -->

		</div><!-- #tribe-events-content -->
	</div>
</div>
