<?php

get_header();

//  Single options show hide
$tz_1040nightclub_text_single       =   ot_get_option( 'nightclub_text_single','Blog Details' );
$tz_1040nightclub_single_sidebar    =   ot_get_option( 'nightclub_singlesidebar',1 );
$tz_1040nightclub_single_date       =   ot_get_option( 'nightclub_tzshowdate',1 );
$tz_1040nightclub_single_tag        =   ot_get_option( 'nightclub_tzshowtag',1 );
$tz_1040nightclub_single_share      =   ot_get_option( 'nightclub_tzshowshare',1 );
$tz_1040nightclub_single_comment    =   ot_get_option( 'nightclub_tzshowcomment',1 );

get_template_part('template_inc/inc','menu');

$tz_post_item_content_box_no_sidebar    =   '';

if ( $tz_1040nightclub_single_sidebar == 0 ) :

    $tz_post_item_content_box_no_sidebar    =   ' tz_post_item_content_box_no_sidebar';

endif

?>

<section class="home-post tz_page_single">
    <div class="tz_post_single">

        <?php

        if ( have_posts() ) : while (have_posts()) : the_post() ;

            $tz_1040nightclub_post_image_single =   get_post_meta( get_the_ID(), 'nightclub_post_image_single', true );
        ?>

            <div id='post-<?php the_ID(); ?>' class="tz_post_item">
                <div class="tz_post_item_image">
                    <div class="tz_post_item_image_overlay"></div>

                    <?php

                        if ( $tz_1040nightclub_post_image_single != '' ) :
                            echo '<img src="'.esc_url( $tz_1040nightclub_post_image_single ).'" alt="'.get_bloginfo('title').'" />';
                        elseif ( has_post_thumbnail() ) :
                            the_post_thumbnail( 'full' );
                        else:
                            echo '<img src="'.get_template_directory_uri().'/images/bk-single-event.png" alt="'.get_bloginfo('title').'" />';
                        endif;

                    ?>

                    <div class="tz_box_tile">
                        <div class="ds-table">
                            <div class="ds-table-cell-left-bottom">
                                <div class="tz_box_tile_content">
                                    <h1 class="tz_title_post">
                                        <?php the_title(); ?>
                                    </h1>
                                    <h2 class="tz_single_text">
                                        <?php echo esc_attr( $tz_1040nightclub_text_single ); ?>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tz_post_item_content_date<?php echo esc_attr( $tz_post_item_content_box_no_sidebar ); ?>">

                    <?php if ( $tz_1040nightclub_single_date == 1 ) : ?>

                        <h3 class="tz_post_date">
                            <?php echo get_the_date(); ?>
                        </h3>

                    <?php endif; ?>

                    <div class="tz_post_item_content_box">
                        <div class="tz_post_item_content">

                            <!-- Content -->
                            <div class="tz_single_post_content">
                                <?php

                                the_content();
                                wp_link_pages( array(
                                    'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', '1040nightclub' ) . '</span>',
                                    'after'       => '</div>',
                                    'link_before' => '<span>',
                                    'link_after'  => '</span>',
                                ) );

                                ?>
                            </div>
                            <!-- End Content -->

                            <!-- Tag -->
                            <?php if ( $tz_1040nightclub_single_tag == 1 && has_tag() ) : ?>

                                <div class="tz_single_tag">
                                    <span class="tz_icon_single">
                                        <i class="fa fa-tags fa-rotate-90" aria-hidden="true"></i><?php the_tags('',' '); ?>
                                    </span>
                                </div>

                            <?php endif; ?>
                            <!-- End Tag -->

                            <!-- Share -->
                            <?php if ( $tz_1040nightclub_single_share == 1 ) : ?>

                                <div class="tz_share_post">
                                    <span class="tz_icon_single">
                                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    </span>

                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
                                        <i class="fa fa-facebook-square"></i>
                                    </a>

                                    <a target="_blank" href="https://twitter.com/home?status=Check%20out%20this%20article:%20<?php print tz_1040nightclub_social_title( get_the_title() ); ?>%20-%20<?php the_permalink(); ?>">
                                        <i class="fa fa-twitter-square"></i>
                                    </a>

                                    <a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>">
                                        <i class="fa fa-google-plus-square"></i>
                                    </a>

                                    <?php $tz_1040nightclub_pin_image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>

                                    <a data-pin-do="skipLink" target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo esc_attr( $tz_1040nightclub_pin_image ); ?>&description=<?php print tz_1040nightclub_social_title( get_the_title() ); ?>">
                                        <i class="fa fa-pinterest-square"></i>
                                    </a>
                                </div>

                            <?php endif; ?>
                            <!-- End Share -->

                            <!-- Comment -->
                            <?php if ( $tz_1040nightclub_single_comment == 1 ) : ?>

                                <div class="tz_single_comment">
                                    <?php comments_template( '', true ); ?>
                                </div>

                            <?php endif; ?>
                            <!-- End Comment -->

                        </div>

                        <?php

                        if ( $tz_1040nightclub_single_sidebar == 1 ) :
                            get_sidebar();
                        endif;

                        ?>

                    </div>
                </div>
            </div>

        <?php

        endwhile; // end while ( have_posts )
        endif; // end if ( have_posts )

        ?>

    </div>
</section>

<?php

get_template_part('template_inc/inc','footer-home-page');
get_footer();

?>

